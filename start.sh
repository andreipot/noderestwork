forever stopall
#forever start -w -l /var/log/workado_hosts.log -a host-server.js
forever start -w -l /var/log/workado_api.log -a api-server.js
forever start -w -l /var/log/workado_web.log -a web-server.js
forever start -w -l /var/log/workado_jobs.log -a job-server.js
forever start -w -l /var/log/workado_ws.log -a socket-server.js

