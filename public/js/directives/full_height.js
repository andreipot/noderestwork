app.directive('fullheight', function ($rootScope) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            $rootScope.$on('event_height_target_change', function(event, theight) {
                element[0].style.height = theight + 'px';
            });
        }
    };
});

app.directive('heighttarget', function ($rootScope) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            scope.$watch(function() {
                var viewport_height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
                var topnav_height = $('#topnav').height();

                $rootScope.$broadcast('event_height_target_change', viewport_height - topnav_height);
            });
        }
    };
})