app.directive('wdActivity', function () {
    return {
        restrict: 'E',
        scope: {
            campaign: '='
        },

        templateUrl: '/js/views/activity.html',

        link: function (scope, element, attrs) {

        },
        controller: function($scope, Tasks, Users) {
            $scope.users_ready = false;
            $scope.tasks_ready = false;

            $scope.activity = [];
            $scope.activity_indexes = [];
            $scope.user_indexes = {};

            $scope.build_activity = function() {
                $scope.activity = [];
                $scope.activity_indexes = [];

                Tasks.FindAll().forEach(function(task, index) {
                    if (task.status == 3 && task.campaign_id == $scope.campaign) {
                        $scope.activity_indexes[task.id] = task;
                        $scope.activity.push( task );
                    }
                });
            }

            $scope.init_check = function() {
                if ($scope.tasks_ready && $scope.users_ready && $scope.campaign) {
                    $scope.build_activity();
                }
            }

            $scope.process_task = function(task) {
                if ($scope.activity_indexes[task.id]) {
                    if (task.status == 3) {
                        // If the task exists and is completed, update it's properties
                        for (var i in task) {
                            $scope.activity_indexes[task.id][i] = task[i];
                        }
                    } else {
                        // Task exists, but isn't completed, remove it
                        for (var i in $scope.activity) {
                            if ($scope.activity[i].id == task.id) {
                                $scope.activity.splice(i, 1);
                                delete $scope.activity_indexes[task.id];
                            }
                        }
                    }
                } else {
                    // Task wasn't here already, add it if it's completed
                    if (task.status == 3) {
                        $scope.activity_indexes[task.id] = task;
                        $scope.activity.push( $scope.activity_indexes[task.id] );
                    }
                }
            }

            // --
            // Init
            // --

            var tasks_ready = Tasks.Ready();
            tasks_ready.then(function() {
                $scope.tasks_ready = true;
                $scope.init_check();
            });

            var users_ready = Users.Ready();
            users_ready.then(function() {
                $scope.users_ready = true;
                $scope.user_indexes = Users.FindAll(true);
                $scope.init_check();
            });

            $scope.$on('event_task_toggle', function (event, data) {
                $scope.process_task( data );
            });

            $scope.$on('task_activity_' + account_id, function(event, task) {
                $scope.process_task( JSON.parse(task) );
            });

            $scope.$watch('campaign', function() {
                $scope.init_check();
            });
        }
    };
});