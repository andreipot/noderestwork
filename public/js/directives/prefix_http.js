app.directive('prefixHttp', function () {
    return {
        restrict: 'A',
        scope: '@',
        require: 'ngModel',
        link: function (scope, element, attrs, controller) {
            function ensurePrefix(value) {
                if (!value) { value = '' }
                if (value.length > 0 && value.indexOf('http://') < 0 && value.indexOf('https://') < 0) {
                    controller.$setViewValue('http://' + value);
                    controller.$render();
                    return 'http://' + value;
                } else {
                    return value;
                }
            }
            controller.$formatters.push(ensurePrefix);
            controller.$parsers.push(ensurePrefix);
        }
    };
});