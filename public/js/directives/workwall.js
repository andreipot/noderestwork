app.directive('wdWorkwall', function () {
    return {
        restrict: 'E',
        scope: {},
        transclude: true,

        templateUrl: '/js/views/workwall.html' ,

        link: function (scope, element, attrs) {

        },
        controller: function($scope, Tasks, Users, Campaigns) {
            $scope.users_ready = false;
            $scope.tasks_ready = false;
            $scope.campaigns_ready = false;

            $scope.campaign_index = {};

            $scope.entries = [];
            $scope.entry_indexes = [];
            $scope.users = [];

            $scope.target_height = 1024;

            $scope.build_workwall = function() {
                $scope.entries = [];
                $scope.entry_indexes = [];

                Tasks.FindAll().forEach(function(task, index) {
                    if (task.status == 3) {
                        $scope.entry_indexes[task.id] = task;
                        $scope.entries.push( task );
                    }
                });

                $scope.campaign_index = Campaigns.FindAll(true);
                console.log($scope.campaign_index);
                console.log($scope.campaign_index['f5922450-78a1-45a8-9c9f-9ce3ba237979']);
            }

            $scope.init_check = function() {
                if ($scope.tasks_ready && $scope.users_ready && $scope.campaigns_ready) {
                    $scope.build_workwall();
                }
            }

            $scope.process_task = function(task) {
                if ($scope.entry_indexes[task.id]) {
                    if (task.status == 3) {
                        // If the task exists and is completed, update it's properties
                        for (var i in task) {
                            $scope.entry_indexes[task.id][i] = task[i];
                        }
                    } else {
                        // Task exists, but isn't completed, remove it
                        for (var i in $scope.entries) {
                            if ($scope.entries[i].id == task.id) {
                                $scope.entries.splice(i, 1);
                                delete $scope.entry_indexes[task.id];
                            }
                        }
                    }
                } else {
                    // Task wasn't here already, add it if it's completed
                    if (task.status == 3) {
                        $scope.entry_indexes[task.id] = task;
                        $scope.entries.push( $scope.entry_indexes[task.id] );
                    }
                }
            }
            
            // --
            // Init
            // --

            var campaigns_ready = Campaigns.Ready();
            campaigns_ready.then(function() {
                $scope.campaigns_ready = true;
                $scope.init_check();
            });

            var tasks_ready = Tasks.Ready();
            tasks_ready.then(function() {
                $scope.tasks_ready = true;
                $scope.init_check();
            });

            var users_ready = Users.Ready();
            users_ready.then(function() {
                $scope.users_ready = true;
                $scope.users = Users.FindAll(true);
                $scope.init_check();
            });

            $scope.$on('event_task_toggle', function (event, data) {
                $scope.process_task( data );
            });

            $scope.$on('task_activity_' + account_id, function(event, task) {
                $scope.process_task( JSON.parse(task) );
            });

            $scope.$on('campaign_activity_' + account_id, function(event, campaign) {
                //$scope.process_task( JSON.parse(campaign) );
            });

            $scope.$on('event_height_target_change', function(event, theight) {
                $scope.target_height = theight - $('#left-cont > .el-head').height();
            });

            $('#right-scroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        }
    };
});