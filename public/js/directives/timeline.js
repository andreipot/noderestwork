app.directive('wdTimeline', function () {
    return {
        restrict: 'E',
        scope: {
            campaign: '=',
            cmonth: '='
        },

        templateUrl: '/js/views/timeline.html',

        link: function (scope, element, attrs) {

        },
        controller: function($scope, Tasks, Users) {
            $scope.offset = 0;

            $scope.change_month = function(month) {
                $scope.cmonth = month;
                console.log('Month changed', month);
            }

            $scope.set_offset = function(plus) {
                if (plus && $scope.offset < ($scope.campaign.blocks.length - 1)) {
                    $scope.offset++;
                } else if (!plus && $scope.offset > 0) {
                    $scope.offset--;
                }
                for (var i in $scope.campaign.blocks) {
                    $scope.campaign.blocks[i]._hidden = ($scope.campaign.blocks[i].num > $scope.offset) ? false : true;
                }
            }
        }
    };
});