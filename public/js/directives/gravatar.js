app.directive('gravatar', function () {
    return {
        restrict: 'A',
        scope: '@',
        link: function (scope, element, attrs) {
            var size = scope.$eval(attrs.gravatarSize);
            attrs.$observe('gravatarSrc', function(value) {
                element[0].src = 'https://www.gravatar.com/avatar/' + md5(value) + '?d=mm&s=' + size;
            })
        }
    };
});