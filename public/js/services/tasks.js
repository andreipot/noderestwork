angular.module('WorkadoApp.Tasks', [])
    .value('version', '1.0')
    .service('Tasks', function ($rootScope, $q, Task) {

        // --
        // Properties
        // --

        var tasks = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in tasks) {
                index[ tasks[i].id ] = tasks[i];
            }
        }

        // --
        // Init
        // --

        console.log('Tasks Service initializing...');

        var tasks_promise = Task.list()
        tasks_promise.then(function(rows) {
            for (var i in rows) {
                tasks.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the tasks list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return tasks;
                }
            },

            /**
             * Returns a count of tasks assigned to a user
             *
             * @param   user_id             string      The desired user id
             * @param   incomplete_only     bool        Return only incomplete tasks?
             */
            CountAssigned: function(user_id, incomplete_only) {
                var count = 0;
                for (var i in tasks) {
                    if (incomplete_only) {
                        if (tasks[i].assigned_to == user_id && tasks[i].status < 3 && tasks[i].status > 0) {
                            count++;
                        }
                    } else {
                        if (tasks[i].assigned_to == user_id && tasks[i].status > 0) {
                            count++;
                        }
                    }
                }

                return count;
            },

            /**
             * Returns a count of tasks assigned to a user
             *
             * @param   user_id             string      The desired user id
             * @param   incomplete_only     bool        Return only incomplete tasks?
             */
            FetchAssigned: function(user_id, incomplete_only) {
                var res = [];
                for (var i in tasks) {
                    if (incomplete_only) {
                        if (tasks[i].assigned_to == user_id && tasks[i].status < 3 && tasks[i].status > 0) {
                            res.push( tasks[i] );
                        }
                    } else {
                        if (tasks[i].assigned_to == user_id && tasks[i].status > 0) {
                            res.push( tasks[i] );
                        }
                    }
                }

                return res;
            },

            /**
             * Update a Task
             */
            Update: function(task) {
                if (index[task.id]) {
                    index[task.id] = task;
                    for (var i in tasks) {
                        if (tasks[i].id == task.id) {
                            tasks[i] = task;
                        }
                    }
                } else {
                    index[task.id] = task;
                    tasks.push( index[task.id] );
                }
            },

            /**
             * Add a Task
             */
            Add: function(task) {
                index[task.id] = task;
                tasks.push(index[task.id]);
            },

            /**
             * Add a Task
             */
            Delete: function(task) {
                for (var i in tasks) {
                    if (tasks[i].id == task.id) {
                        tasks.splice(i, 1);
                    }
                }
                delete index[task.id];
            },

            /**
             * Delete Tasks related to a specific Campaign
             */
            DeleteCampaign: function(campaign_id) {
                for (var i in tasks) {
                    if (tasks[i].campaign_id == campaign_id) {
                        delete index[tasks[i].id]
                        tasks.splice(i, 1);
                    }
                }
            }
        }
    });