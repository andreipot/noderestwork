angular.module('WorkadoApp.Socket', [])
    .value('version', '1.0')
    .service('Socket', function ($rootScope) {
        if (typeof token != "undefined" && token != '') {
            var socket = io.connect(socket_host, { query: "auth_param=" + token });

            return {

                on:function (eventName, callback) {
                    socket.on(eventName, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            callback.apply(socket, args);
                        });
                    });
                },

                emit:function (eventName, data, callback) {
                    socket.emit(eventName, data, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    })
                }
            };
        } else {
            return false;
        }
    });
