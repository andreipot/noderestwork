angular.module('WorkadoApp.Campaigns', [])
    .value('version', '1.0')
    .service('Campaigns', function ($rootScope, $q, Campaign) {

        // --
        // Properties
        // --

        var campaigns = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in campaigns) {
                index[ campaigns[i].id ] = campaigns[i];
            }
        }

        // --
        // Init
        // --

        console.log('Campaigns Service initializing...');

        var campaigns_promise = Campaign.list()
        campaigns_promise.then(function(rows) {
            for (var i in rows) {
                campaigns.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return campaigns;
                }
            },

            /**
             * Add a Task
             */
            Delete: function(camp) {
                for (var i in campaigns) {
                    if (campaigns[i].id == camp.id) {
                        campaigns.splice(i, 1);
                    }
                }
                delete index[camp.id];
            }

        }
    });