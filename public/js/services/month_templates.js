angular.module('WorkadoApp.MonthTemplates', [])
    .value('version', '1.0')
    .service('MonthTemplates', function ($rootScope, $q, MonthTemplate) {

        // --
        // Properties
        // --

        var month_templates = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in month_templates) {
                index[ month_templates[i].id ] = month_templates[i];
            }
        }

        // --
        // Init
        // --

        console.log('MonthTemplates Service initializing...');

        var month_templates_promise = MonthTemplate.list()
        month_templates_promise.then(function(rows) {
            for (var i in rows) {
                month_templates.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return month_templates;
                }
            }

        }
    });