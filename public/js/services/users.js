angular.module('WorkadoApp.Users', [])
    .value('version', '1.0')
    .service('Users', function ($rootScope, $q, User) {

        // --
        // Properties
        // --

        var users = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in users) {
                users[i].onboarding = JSON.parse(users[i].onboarding || '{}') || {};
                index[ users[i].id ] = users[i];
            }
        }

        // --
        // Init
        // --

        console.log('Users Service initializing...');

        var users_promise = User.list()
        users_promise.then(function(rows) {
            for (var i in rows) {
                users.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the users list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return users;
                }
            },

            Get: function(user_id) {
                return index[user_id];
            }
        }
    });
