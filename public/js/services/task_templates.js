angular.module('WorkadoApp.TaskTemplates', [])
    .value('version', '1.0')
    .service('TaskTemplates', function ($rootScope, $q, TaskTemplate) {

        // --
        // Properties
        // --

        var task_templates = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in task_templates) {
                index[ task_templates[i].id ] = task_templates[i];
            }
        }

        // --
        // Init
        // --

        console.log('TaskTemplates Service initializing...');

        var task_templates_promise = TaskTemplate.list()
        task_templates_promise.then(function(rows) {
            for (var i in rows) {
                task_templates.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return task_templates;
                }
            }

        }
    });