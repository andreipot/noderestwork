angular.module('WorkadoApp.Accounts', [])
    .value('version', '1.0')
    .service('Accounts', function ($rootScope, $q, Account) {

        // --
        // Properties
        // --

        var accounts = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in accounts) {
                index[ accounts[i].id ] = accounts[i];
            }
        }

        // --
        // Init
        // --

        console.log('Accounts Service initializing...');

        var accounts_promise = Account.list()
        accounts_promise.then(function(rows) {
            for (var i in rows) {
                accounts.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return accounts;
                }
            },

            /**
             * Returns access to a single account
             *
             * @param   id     UUID        The desired account id
             */
            Get: function(id) {
                return index[id];
            }
            
        }
    });