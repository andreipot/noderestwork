angular.module('WorkadoApp.Companies', [])
    .value('version', '1.0')
    .service('Companies', function ($rootScope, $q, Company) {

        // --
        // Properties
        // --

        var companies = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in companies) {
                index[ companies[i].id ] = companies[i];
            }
        }

        // --
        // Init
        // --

        console.log('Companies Service initializing...');

        var companies_promise = Company.list()
        companies_promise.then(function(rows) {
            for (var i in rows) {
                companies.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the companies list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return companies;
                }
            }

        }
    });