app.factory('Billing', function($http) {

    var Billing = function(data) {
        angular.extend(this, data);
    };

    /**
     * Create a Billing Agreement
     *
     * @param   billing      obj     Billing Obj
     * @param   cb           func    Callback Function
     */
    Billing.create = function(billing, cb) {
        var url = api_host + '/billing';
        return $http.post(url + '?token=' + token, billing)
            .success(function(data, status, headers, config) {
                cb(null, data);
            })
            .error(function(data, status, headers, config) {
                cb(data, status);
            });
    };

    /**
     * Create a Billing Agreement
     *
     * @param   billing      obj     Billing Obj
     * @param   cb           func    Callback Function
     */
    Billing.update = function(billing, cb) {
        var url = api_host + '/billing';
        return $http.put(url + '?token=' + token, billing)
            .success(function(data, status, headers, config) {
                cb(null, data);
            })
            .error(function(data, status, headers, config) {
                cb(data, status);
            });
    };

    /**
     * Delete a Billing Agreement
     *
     * @param   cb      func     Callback Function
     */
    Billing.delete = function(cb) {
        var url = api_host + '/billing';
        return $http.delete(url + '?token=' + token)
            .success(function(data, status, headers, config) {
                cb(null, data);
            })
            .error(function(data, status, headers, config) {
                cb(data, status);
            });
    };

    return Billing;
});