app.factory('User', function($http) {

    var User = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific User
     *
     * @param   id      int     User Identifier
     */
    User.get = function(id) {
        var url = api_host + '/user/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Fetch a List of the User's Users
     */
    User.list = function() {
        var url = api_host + '/users';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Create a User
     *
     * @param   user      obj     User Obj
     */
    User.create = function(user) {
        var url = api_host + '/user';
        return $http.post(url + '?token=' + token, user).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Update a User
     *
     * @param   user      obj     User Obj
     */
    User.update = function(user) {
        var url = api_host + '/user/' + user.id;
        return $http.put(url + '?token=' + token, user).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Delete a User
     *
     * @param   id      int     User Identifier
     */
    User.delete = function(id) {
        var url = api_host + '/user/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new User(response.data);
        });
    };

    return User;
});