app.factory('Campaign', function($http) {

    var Campaign = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Campaign
     *
     * @param   id      int     Campaign Identifier
     */
    Campaign.get = function(id) {
        var url = api_host + '/campaign/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Fetch a List of the Campaign's Campaigns
     */
    Campaign.list = function() {
        var url = api_host + '/campaigns';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Create a Campaign
     *
     * @param   campaign      obj     Campaign Obj
     */
    Campaign.create = function(campaign) {
        var url = api_host + '/campaign';
        return $http.post(url + '?token=' + token, campaign).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Update a Campaign
     *
     * @param   campaign      obj     Campaign Obj
     */
    Campaign.update = function(campaign) {
        var url = api_host + '/campaign/' + campaign.id;
        return $http.put(url + '?token=' + token, campaign).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Delete a Campaign
     *
     * @param   id      int     Campaign Identifier
     */
    Campaign.delete = function(id) {
        var url = api_host + '/campaign/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Campaign(response.data);
        });
    };

    return Campaign;
});