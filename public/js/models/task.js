app.factory('Task', function($http) {

    var Task = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Task
     *
     * @param   id      int     Task Identifier
     */
    Task.get = function(id) {
        var url = api_host + '/task/' + id;
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Fetch a List of the Task's Tasks
     */
    Task.list = function() {
        var url = api_host + '/tasks';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Fetch a List of the Latest Activity
     */
    Task.activity = function() {
        var url = api_host + '/tasks/activity';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Fetch a List of the Latest Activity
     */
    Task.assigned = function() {
        var url = api_host + '/tasks/assigned';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Create a Task
     *
     * @param   task      obj     Task Obj
     */
    Task.create = function(task) {
        var url = api_host + '/task';
        return $http.post(url + '?token=' + token, task).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Update a Task
     *
     * @param   task      obj     Task Obj
     */
    Task.update = function(task) {
        var url = api_host + '/task/' + task.id;
        return $http.put(url + '?token=' + token, task).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Delete a Task
     *
     * @param   id      int     Task Identifier
     */
    Task.delete = function(id) {
        var url = api_host + '/task/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    return Task;
});