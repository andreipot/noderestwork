app.factory('MonthTemplate', function($http) {

    var MonthTemplate = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific MonthTemplate
     *
     * @param   id      int     MonthTemplate Identifier
     */
    MonthTemplate.get = function(id) {
        var url = api_host + '/template/set/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Fetch a List of the MonthTemplate's MonthTemplates
     */
    MonthTemplate.list = function() {
        var url = api_host + '/template/sets';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Create a MonthTemplate
     *
     * @param   task      obj     MonthTemplate Obj
     */
    MonthTemplate.create = function(task) {
        var url = api_host + '/template/set';
        return $http.post(url + '?token=' + token, task).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Update a MonthTemplate
     *
     * @param   task      obj     MonthTemplate Obj
     */
    MonthTemplate.update = function(task) {
        var url = api_host + '/template/set/' + task.id;
        return $http.put(url + '?token=' + token, task).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Delete a MonthTemplate
     *
     * @param   id      int     MonthTemplate Identifier
     */
    MonthTemplate.delete = function(id) {
        var url = api_host + '/template/set/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    return MonthTemplate;
});