app.factory('Company', function($http) {

    var Company = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Company
     *
     * @param   id      int     Company Identifier
     */
    Company.get = function(id) {
        var url = api_host + '/company/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Fetch a List of the Company's Companys
     */
    Company.list = function() {
        var url = api_host + '/companies';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Create a Company
     *
     * @param   company      obj     Company Obj
     */
    Company.create = function(company) {
        var url = api_host + '/company';
        return $http.post(url + '?token=' + token, company).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Update a Company
     *
     * @param   company      obj     Company Obj
     */
    Company.update = function(company) {
        var url = api_host + '/company/' + company.id;
        return $http.put(url + '?token=' + token, company).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Delete a Company
     *
     * @param   id      int     Company Identifier
     */
    Company.delete = function(id) {
        var url = api_host + '/company/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Company(response.data);
        });
    };

    return Company;
});