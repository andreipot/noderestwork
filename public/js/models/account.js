app.factory('Account', function($http) {

    var Account = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Account
     *
     * @param   id      int     Account Identifier
     */
    Account.get = function(id) {
        var url = api_host + '/account/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Fetch a List of the Account's Accounts
     */
    Account.list = function() {
        var url = api_host + '/accounts';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Create a Account
     *
     * @param   account      obj     Account Obj
     */
    Account.create = function(account) {
        var url = api_host + '/account';
        return $http.post(url + '?token=' + token, account).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Update a Account
     *
     * @param   account      obj     Account Obj
     */
    Account.update = function(account) {
        var url = api_host + '/account/' + account.id;
        return $http.put(url + '?token=' + token, account).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Delete a Account
     *
     * @param   id      int     Account Identifier
     */
    Account.delete = function(id) {
        var url = api_host + '/account/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Account(response.data);
        });
    };

    return Account;
});