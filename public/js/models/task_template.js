app.factory('TaskTemplate', function($http) {

    var TaskTemplate = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific TaskTemplate
     *
     * @param   id      int     TaskTemplate Identifier
     */
    TaskTemplate.get = function(id) {
        var url = api_host + '/template/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Fetch a List of the TaskTemplate's TaskTemplates
     */
    TaskTemplate.list = function() {
        var url = api_host + '/templates';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Create a TaskTemplate
     *
     * @param   task      obj     TaskTemplate Obj
     */
    TaskTemplate.create = function(task) {
        var url = api_host + '/template';
        return $http.post(url + '?token=' + token, task).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Update a TaskTemplate
     *
     * @param   task      obj     TaskTemplate Obj
     */
    TaskTemplate.update = function(task) {
        var url = api_host + '/template/' + task.id;
        return $http.put(url + '?token=' + token, task).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Delete a TaskTemplate
     *
     * @param   id      int     TaskTemplate Identifier
     */
    TaskTemplate.delete = function(id) {
        var url = api_host + '/template/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    return TaskTemplate;
});