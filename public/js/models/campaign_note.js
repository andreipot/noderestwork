app.factory('CampaignNote', function($http) {

    var CampaignNote = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific CampaignNote
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.get = function(campaign_id, id) {
        var url = api_host + '/campaign/' + campaign_id + '/note/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Fetch a List of the CampaignNote's CampaignNotes
     *
     * @param   campaign_id     string     Owning Campaign id
     */
    CampaignNote.list = function(campaign_id) {
        var url = api_host + '/campaign/' + campaign_id + '/notes';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Create a CampaignNote
     *
     * @param   note      obj     CampaignNote obj
     */
    CampaignNote.create = function(note) {
        var url = api_host + '/campaign/' + note.campaign_id + '/note';
        return $http.post(url + '?token=' + token, note).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Update a CampaignNote
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.update = function(campaign_id, id, props) {
        var url = api_host + '/campaign/' + campaign_id + '/note/' + id;
        return $http.put(url + '?token=' + token, props).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Delete a CampaignNote
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.delete = function(campaign_id, id) {
        var url = api_host + '/campaign/' + campaign_id + '/note/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Mark a set of CampaignNotes as viewed.
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.view = function(campaign_id, ids) {
        var url = api_host + '/campaign/' + campaign_id + '/notes/view';
        return $http.put(url + '?token=' + token, { note_ids: ids }).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    return CampaignNote;
});