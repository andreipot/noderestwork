app.controller('ManageCompaniesController', function ManageCompaniesController($scope, Users, User, $cookies, Companies, Company) {

    $scope.companies = [];
    $scope.selected = '';
    $scope.users = [];
    $scope.user_indexes = {};
    $scope.target_height = 1024;

    $scope.init = 0;
    $scope.ob_active = 0;
    $scope.showing_modal = false;
    $scope.ob = {
        step: 0
    }

    $scope.new = {
        company: {
            id: '',
            account_id: '',
            name: '',
            website: '',
            contact: '',
            email: '',
            phone: '',
            do_reporting: '',
            status: ''
        }
    }

    $scope.editing = angular.copy($scope.new.company);
    $scope.creating = angular.copy($scope.new.company);
    $scope.mode = 'new';
    $scope.modal_title_edit_company = 'Add New Company';

    $scope.select_company = function(company) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_company = 'Edit Company';
        $scope.editing = angular.copy(company);
    }

    $scope.new_company = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_company = 'New Company';
        $scope.creating = angular.copy($scope.new.company);
        $scope.showing_modal = true;
    }

    $scope.save_company = function(company) {
        if ($scope.mode == 'new') {
            company.do_reporting = true;
            company.phone = company.phone.replace(/-/g, '');
            company.phone = company.phone.replace(/\(/g, '');
            company.phone = company.phone.replace(/\)/g, '');
            company.phone = company.phone.replace(/ /g, '');
            var company_promise = Company.create(company);
            company_promise.then(function(company) {
                $scope.companies.push(company);
                $scope.sort_companies();
                $scope.select_company( company );
                $('#modal_edit_company').modal('hide');
            });
        } else {
            var company_promise = Company.update(company);
            company_promise.then(function(company) {
                for (var i in $scope.companies) {
                    if ($scope.companies[i].id == company.id) {
                        $scope.companies[i] = company;
                    }
                }
                $('#modal_edit_company').modal('hide');
            });
        }

    }

    $scope.delete_company = function(company) {
        var company_promise = Company.delete(company.id);
        company_promise.then(function(data) {
            for (var i in $scope.companies) {
                if ($scope.companies[i].id == company.id) {
                    $scope.companies.splice(i, 1);
                }
            }
            $scope.select_company($scope.companies[0]);
            $('#modal_delete_company').modal('hide');
        });
    }

    $scope.sort_companies = function() {
        $scope.companies.sort(function(a, b) {
            if (a.name.toUpperCase() < b.name.toUpperCase()) {
                return -1;
            } else if (a.name.toUpperCase() > b.name.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    $scope.onboarding_done = function() {
        if (typeof $scope.user_indexes[user_id].onboarding.companies == "undefined") {
            $scope.user_indexes[user_id].onboarding.companies = true;

            User.update($scope.user_indexes[user_id]).then(function() {
                console.log('Onboarding Done');
                console.log($scope.user_indexes[user_id].onboarding);
            });
        }
    }

    $scope.progress_ob = function(step) {
        $scope.ob.step = step;

        switch (step) {
            case 2:
                $scope.user_indexes[user_id].onboarding.clients = true;
                $scope.ob.step = 2;

                User.update($scope.user_indexes[user_id]);
                break;

            case 3:
                window.location = '/campaigns';
                $scope.ob_active = false;
                break;
        }


    }

    $scope.init_check = function() {
        $scope.init++;

        if ($scope.init > 1) {
            if (!$scope.user_indexes[user_id].onboarding.clients && !$scope.companies.length) {
                $scope.ob.step = 1;
                $scope.ob_active = true;
            } else {
                $scope.progress_ob(2);
            }
            $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        }

        $scope.select_company( $scope.companies[0] );
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    Users.Ready().then(function() {
        $scope.users = Users.FindAll();
        $scope.user_indexes = Users.FindAll(true);

        $scope.init_check();
    });

    Companies.Ready().then(function() {
        $scope.companies = Companies.FindAll();
        $scope.sort_companies();

        var preselect = window.location.hash.split('/')[1];
        if (preselect) {
            for (var i in $scope.companies) {
                if ($scope.companies[i].id == preselect) {
                    $scope.select_company( $scope.companies[i] );
                    break;
                }
            }
        }

        $scope.init_check();

        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    });


});