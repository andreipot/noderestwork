app.controller('BillingController', function BillingController($scope, Billing, Accounts, Users) {

    $scope.blank = {
        cardholder_name: '',
        card_number: '',
        card_exp_month: '',
        card_exp_year: '',
        card_cvv: ''
    }

    $scope.info = angular.copy($scope.blank);

    $scope.section = 'update';
    $scope.processing = false;
    $scope.users = [];
    $scope.total_costs = 0;

    $scope.did_success = false;
    $scope.did_failure = false;
    $scope.error_message = 'There was a problem updating your billing details.';

    $scope.update = function(info) {
        $scope.did_success = false;
        $scope.did_failure = false;
        $scope.processing = true;

        var account = Accounts.Get(account_id);

        if (account && account.stripe_card_id) {
            Billing.update(info, function(error, data) {
                $scope.processing = false;

                if (error) {
                    $scope.did_failure = true;
                    $scope.error_message = error.message.message;
                } else {
                    $scope.did_success = true;
                    $scope.info = angular.copy($scope.blank);
                }

                $scope.form_update_billing.$setPristine();
                $('#billing-alert').focus();
            });
        } else {
            Billing.create(info, function(error, data) {
                $scope.processing = false;

                if (error) {
                    $scope.did_failure = true;
                    $scope.error_message = error.message.message;
                } else {
                    $scope.did_success = true;
                    $scope.info = angular.copy($scope.blank);
                }

                $scope.form_update_billing.$setPristine();
                $('#billing-alert').focus();
            });
        }
    }

    $scope.delete = function() {
        $scope.did_success = false;
        $scope.did_failure = false;
        $scope.processing = true;
        Billing.delete(function(error, data) {
            $scope.processing = false;

            if (error) {
                $scope.did_failure = true;
                $scope.error_message = error.message.message;
            } else {
                $scope.did_success = true;
            }

            $('#modal_confirm_cancel').modal('hide');
            $('#billing-alert-d').focus();
        });
    }

    $scope.select_section = function(section) {
        $scope.did_success = false;
        $scope.did_failure = false;
        $scope.section = section;
    }

    // --
    // Init
    // --

    Accounts.Ready().then(function() {
        $scope.account = Accounts.Get(account_id);
    });

    Users.Ready().then(function() {
        $scope.users = Users.FindAll();

        if ($scope.users.length > 2) {
            $scope.total_costs = (($scope.users.length - 2) * 10) + 30;
        } else {
            $scope.total_costs = 30;
        }
    });
});