app.controller('TopNavController', function TopNavController($scope, $rootScope,  Tasks) {

    $scope.assigned = 0;
    $scope.notification_width = 283;
    $scope.notifications_on = false;
    $scope.tasks = [];

    // --
    // Functions
    // --

    $scope.show_notifications = function(show) {
        $scope.notification_width = $('#member-notifications').width() - 40;
        if (show) {
            $scope.notifications_on = true;
        } else {
            $scope.notifications_on = false;
        }
    }

    // --
    // Init
    // --

    Tasks.Ready().then(function() {
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $rootScope.$on('event_task_toggle', function (event, data) {
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $rootScope.$on('event_campaign_deleted', function (event, data) {
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $rootScope.$on('task_activity_' + account_id, function(event, task) {
        task = JSON.parse(task);
        Tasks.Update(task);
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $('#notifications-container').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});

});