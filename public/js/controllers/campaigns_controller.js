app.controller('CampaignsController', function CampaignsController($scope, $rootScope, $route, $routeParams, $location, $cookies, Company, Companies, Campaign, Campaigns, CampaignNote, Task, Tasks, User, Users, TaskTemplate, TaskTemplates, MonthTemplate, MonthTemplates, Socket) {
    $scope.month_names = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

    $scope.campaigns = [];
    $scope.campaign_indexes = [];
    $scope.companies = [];
    $scope.company_indexes = [];
    $scope.users = [];
    $scope.user_indexes = [];
    $scope.templates = [];
    $scope.template_indexes = [];
    $scope.month_templates = [];
    $scope.month_template_indexes = [];
    $scope.show_task_typeahead = false;

    $scope.init_status = 0;
    $scope.ob_active = 0;
    $scope.showing_modal = false;
    $scope.doing_init = true;
    $scope.note_mode = 'new';

    $scope.current = false;
    $scope.actual_month = 1;
    $scope.current_month = 1;
    $scope.selected_template = '';
    $scope.selected_task = false;
    $scope.selected_note = false;
    $scope.selectedmt = '';
    $scope.current_notes = [];
    $scope.target_height = 1024;
    $scope.notes_to_clear = false;
    $scope.months_to_add = 1;
    $scope.month_load_status = 0
    $scope.month_load_mode = 0;
    $scope.label_modal_edit_task = 'Add Task to Timeline';
    $scope.label_bt_edit_task = 'Add New Task';
    $scope.label_modal_edit_campaign = 'New Campaign';
    $scope.label_bt_edit_campaign = 'Add New Campaign';

    $scope.new = {
        task: {
            title: '',
            description: '',
            selected_template: ''
        },
        campaign: {
            company_id: '',
            start_at: '',
            stop_at: ''
        },
        note: {
            note: ''
        },
        set: {
            name: ''
        }
    }

    $scope.editing_task = angular.copy($scope.new.task);
    $scope.new_campaign = angular.copy($scope.new.campaign);
    $scope.note         = angular.copy($scope.new.note);
    $scope.set          = angular.copy($scope.new.set);

    $scope.ob = {
        step: 0
    }


    $scope.init_check = function() {
        $scope.init_status++;

        if ($scope.init_status == 3) {
            $scope.prepare_data();
            //$scope.refresh_workwall();
        }
    }

    $scope.prepare_data = function() {
        if ($scope.campaigns.length) {

            for (var i in $scope.campaigns) {
                $scope.campaigns[i].total_months = month_span($scope.campaigns[i].start_at, $scope.campaigns[i].stop_at) + 1;
                $scope.campaigns[i].months_done = month_span($scope.campaigns[i].start_at, new Date().getTime() / 1000) + 1;
                $scope.campaigns[i].current_month = month_span($scope.campaigns[i].start_at, new Date().getTime() / 1000) + 1;
                $scope.campaigns[i].unviewed = false;
                $scope.campaigns[i].company_name = $scope.company_indexes[$scope.campaigns[i].company_id].name;

                for (var k in $scope.campaigns[i].notes) {
                    if (!$scope.campaigns[i].notes[k].viewed) {
                        $scope.campaigns[i].unviewed = true;
                        break;
                    }
                }

                // Sort Tasks into month blocks and tag overdues
                $scope.process_tasks( $scope.campaigns[i] );
            }

            // Sort Campaigns
            $scope.campaigns.sort(function(a, b) {
                // Step 1 - Compare Status
                if (a.status != b.status) {
                    return a.status - b.status;
                }

                // Step 2 - Compare Client Name
                if (a.company_name != b.company_name) {
                    if (a.company_name < b.company_name) {
                        return -1;
                    }
                    if (a.company_name > b.company_name) {
                        return 1;
                    }
                    return 0;
                }

                // Step 3 - Compare Campaign Name
                if (a.name != b.name) {
                    if (a.name < b.name) {
                        return -1;
                    }
                    if (a.name > b.name) {
                        return 1;
                    }
                    return 0;
                }
            });

            if ($scope.campaigns.length && !$scope.user_indexes[user_id].onboarding.campaigns) {
                $scope.user_indexes[user_id].onboarding.campaigns = true;
                User.update($scope.user_indexes[user_id]);
            }

            if ($scope.campaigns.length && !$scope.user_indexes[user_id].onboarding.tasks) {
                if ($scope.user_indexes[user_id].type > 0) {
                    $scope.user_indexes[user_id].onboarding.tasks = true;
                    User.update($scope.user_indexes[user_id]);
                    $scope.ob.step = 3;
                } else {
                    $scope.ob.step = 2;
                }
                $scope.ob_active = true;

            }

            if ($scope.campaigns.length && $scope.user_indexes[user_id].onboarding.tasks && !$scope.user_indexes[user_id].onboarding.toggle) {
                $scope.ob_active = true;
                $scope.ob.step = 3;
            }

            if ($scope.campaigns.length && $scope.user_indexes[user_id].onboarding.tasks && $scope.user_indexes[user_id].onboarding.toggle && !$scope.user_indexes[user_id].onboarding.notes) {
                $scope.ob_active = true;
                $scope.ob.step = 4;
            }

            if ($scope.campaigns.length && $scope.user_indexes[user_id].onboarding.tasks && $scope.user_indexes[user_id].onboarding.toggle && $scope.user_indexes[user_id].onboarding.notes && !$scope.user_indexes[user_id].onboarding.templates) {
                $scope.ob_active = true;
                $scope.ob.step = 5;
            }

            var hash = location.hash.split('/');

            if (hash[1] && $scope.campaign_indexes[hash[1]]) {
                $scope.select_campaign($scope.campaign_indexes[hash[1]]);

                // Find the Month the task is in
                if (hash[2]) {
                    $scope.campaign_indexes[hash[1]].blocks.forEach(function(item, i) {
                        for (var i in item.tasks) {
                            if (item.tasks[i].id == hash[2]) {
                                $scope.current_month = item.num;
                            }
                        }
                    });
                }
            } else {
                $scope.select_campaign($scope.campaigns[0]);
            }
        } else if (!$scope.campaigns.length && !$scope.user_indexes[user_id].onboarding.campaigns) {
            $scope.ob.step = 1;
            $scope.ob_active = true;
        }

        $scope.doing_init = false;
    }

    $scope.select_campaign = function(campaign) {
        if (campaign && campaign.id) {
            $scope.mark_notes_viewed(campaign);
            $scope.current = campaign;
            $scope.current_month = month_span(campaign.start_at, new Date().getTime() / 1000) + 1;
            $scope.current.actual_month = $scope.current_month;
            var exists = false;
            for (var i in $scope.current.blocks) {
                if ($scope.current.blocks[i].num == $scope.current_month) {
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                for (var i in $scope.current.blocks) {
                    $scope.current_month = $scope.current.blocks[i].num;
                    break;
                }
            }


        } else {
            console.log('Selected invalid campaign entry');
        }
    }

    $scope.toggle_reporting = function() {
        var body = {};
        body.do_reporting = !$scope.current.do_reporting;
        body.id = $scope.current.company_id;
        var company_promise = Company.update(body);
        company_promise.then(function(company) {
            $scope.companies[company.id] = company;
            $scope.current.do_reporting = company.do_reporting;
        });
    }

    $scope.toggle_campaign_status = function() {
        var body = {};
        body.status = ($scope.current.status == 1) ? 2 : 1;
        body.id = $scope.current.id;
        var camp_promise = Campaign.update(body);
        camp_promise.then(function(company) {
            $scope.current.status = company.status;
        });
    }

    $scope.sort_users = function() {
        $scope.users.sort(function(a, b) {
            if (a.fname.toUpperCase() < b.fname.toUpperCase()) {
                return -1;
            } else if (a.fname.toUpperCase() > b.fname.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    $scope.select_template = function() {
        if ($scope.selected_template == '' || !$scope.selected_template) {
            $scope.editing_task.title = '';
            $scope.editing_task.description = '';
        } else {
            $scope.editing_task.title = $scope.template_indexes[$scope.selected_template].title;
            $scope.editing_task.description = $scope.template_indexes[$scope.selected_template].description;
        }
    }

    $scope.select_task = function(task) {
        $scope.selected_task = task;
    }

    $scope.save_task = function(task) {
        task.due_on = parseInt(task.due_on_js.getTime() / 1000);
        if (task.id) {
            // Editing a Task
            Task.update(task).then(function(task) {
                $scope.editing_task = angular.copy($scope.new.task);
                $rootScope.$broadcast('event_task_toggle', task);
                $scope.process_tasks($scope.current);
                $('#modal_edit_task').modal('hide');
            });
        } else {
            // New Task - Add supporting task data
            task.campaign_id = $scope.current.id;
            task.company_id = $scope.current.company_id;

            Task.create(task).then(function(task) {
                $scope.editing_task = angular.copy($scope.new.task);
                //Tasks.Add(task);
                $rootScope.$broadcast('event_task_toggle', task);
                $scope.showing_modal = false;

                // Onboarding Check
                if (!$scope.user_indexes[user_id].onboarding.tasks) {
                    $scope.ob.step = 3;
                    $scope.user_indexes[user_id].onboarding.tasks = true;
                    User.update($scope.user_indexes[user_id]);
                }

                $scope.showing_modal = false;
                $('#modal_edit_task').modal('hide');
            });
        }
    }

    $scope.delete_task = function(task) {
        task.status = 0;
        var task_promise = Task.delete(task.id);
        task_promise.then(function(data) {
            Tasks.Delete(task);
            $rootScope.$broadcast('event_task_toggle', task);
            $('#modal_delete_task').modal('hide');
        });
    }

    $scope.toggle_task = function(task) {
        if (task.status != 3) {
            task.status = 3;

            // Onboarding Check
            if (!$scope.user_indexes[user_id].onboarding.toggle) {
                $scope.ob.step = 4;
                $scope.user_indexes[user_id].onboarding.toggle = true;
                User.update($scope.user_indexes[user_id]);
            }
        } else {
            task.status = 1;
        }
        var task_promise = Task.update(task);
        task_promise.then(function(data) {
            Tasks.Update(data);
            $rootScope.$broadcast('event_task_toggle', data);

            // Onboarding Check
            if (!$scope.user_indexes[user_id].onboarding.toggle) {
                $scope.ob.step = 4;
                $scope.user_indexes[user_id].onboarding.toggle = true;
                User.update($scope.user_indexes[user_id]);
            }

            $('#modal_complete_task').modal('hide');
        });


    }
    
    $scope.process_tasks = function(campaign) {
        var now_obj = new Date();
        var now = parseInt(now_obj.getTime() / 1000);
        var start = new Date(campaign.start_at * 1000);
        var cur_month = start.getMonth() + 1;
        var cur_year = start.getFullYear();
        campaign.total_completed = 0;
        campaign.has_overdue = false;
        campaign.active_month = now_obj.getFullYear() + '/' + (now_obj.getMonth() + 1);

        campaign.blocks = [];
        for (var j = 1; j <= campaign.total_months; j++) {
            var new_block = {
                num: j,
                tasks: [],
                completed: 0,
                _hidden: false,
                name: $scope.month_names[ cur_month - 1 ] + ' ' + start.getFullYear()
            };
            campaign.blocks[cur_year + '/' + cur_month] = new_block;
            campaign.blocks.push(new_block);
            if ((cur_month + 1) <= 12) {
                cur_month++;
            } else {
                cur_month = 1;
                cur_year++;
            }
        }
        
        for (var j in campaign.tasks) {
            campaign.tasks[j].is_overdue = (now >= campaign.tasks[j].due_on) ? true : false;

            if (campaign.tasks[j].is_overdue && campaign.tasks[j].status != 3) {
                campaign.has_overdue = true;
            }

            var due = new Date(campaign.tasks[j].due_on * 1000);
            if (campaign.blocks[(due.getFullYear()) + '/' + (due.getMonth() + 1)]) {
                campaign.blocks[(due.getFullYear()) + '/' + (due.getMonth() + 1)].tasks.push(campaign.tasks[j]);
                if (campaign.tasks[j].status == 3) {
                    campaign.total_completed++;
                    campaign.blocks[(due.getFullYear()) + '/' + (due.getMonth() + 1)].completed++;
                }
            } else {
                campaign.blocks[(start.getFullYear()) + '/' + (start.getMonth() + 1)].tasks.push(campaign.tasks[j]);
                if (campaign.tasks[j].status == 3) {
                    campaign.total_completed++;
                    campaign.blocks[(start.getFullYear()) + '/' + (start.getMonth() + 1)].completed++;
                }
            }
        }

        campaign.blocks_with_tasks = 0;
        for (var i in campaign.blocks) {
            if (campaign.blocks[i].tasks.length) {
                campaign.blocks_with_tasks++;
            }
            campaign.blocks[i].percent_done = campaign.blocks[i].completed / campaign.blocks[i].tasks.length * 100;
        }

        if (campaign.blocks_with_tasks >= 2) {
            campaign.blocks_with_tasks = campaign.blocks_with_tasks / 2;
        }
    }

    $scope.add_campaign = function() {
        $scope.new_campaign = angular.copy($scope.new.campaign);
        $scope.show_modal();
    };

    $scope.edit_campaign = function(campaign) {
        $scope.new_campaign = campaign;

        $scope.new_campaign.start_at_js = new Date($scope.new_campaign.start_at * 1000);

        $scope.label_modal_edit_campaign = 'Edit Campaign';
        $scope.label_bt_edit_campaign = 'Save Campaign';
    }

    $scope.save_campaign = function() {
        var start = $scope.new_campaign.start_at_js;
        start.setDate(1);
        start.setHours(0);
        start.setMinutes(0);
        start.setSeconds(0);
        $scope.new_campaign.start_at = parseInt(start.getTime() / 1000);

        if ($scope.new_campaign.id) { // campaign exists, update it
            console.log('Updating Campaign');
            var task_promise = Campaign.update($scope.new_campaign);
            task_promise.then(function(camp) {
                camp = $scope.new_campaign;
                camp.total_months = month_span(camp.start_at, camp.stop_at) + 1;
                camp.months_done = month_span(camp.start_at, new Date().getTime() / 1000);
                camp.current_month = month_span(camp.start_at, new Date().getTime() / 1000) + 1;

                // Sort Tasks into month blocks and tag overdues
                $scope.process_tasks( camp );

                $scope.showing_modal = false;
                $('#modal_edit_campaign').modal('hide');
            });
        } else { // new campaign
            console.log('Creating Campaign');

            // Set the stop date to the last second of the start month (i.e. start + 1 month - 1 sec)
            var stop = new Date($scope.new_campaign.start_at_js.getTime());
            stop.setDate(1);
            stop.setHours(0);
            stop.setMinutes(0);
            stop.setSeconds(0);
            stop.setMonth(start.getMonth() + 1);
            $scope.new_campaign.stop_at = parseInt(stop.getTime() / 1000) - 1;

            var task_promise = Campaign.create($scope.new_campaign);
            task_promise.then(function(camp) {
                camp.tasks = [];
                camp.notes = [];

                camp.total_months = month_span(camp.start_at, camp.stop_at) + 1;
                camp.months_done = month_span(camp.start_at, new Date().getTime() / 1000);
                camp.current_month = month_span(camp.start_at, new Date().getTime() / 1000) + 1;

                // Sort Tasks into month blocks and tag overdues
                $scope.process_tasks( camp );

                $scope.campaign_indexes[camp.id] = camp;
                $scope.campaigns.push( $scope.campaign_indexes[camp.id] );
                $scope.new_campaign = angular.copy($scope.new.campaign);
                $scope.select_campaign( $scope.campaign_indexes[camp.id] );

                // Onboarding Check
                if ($scope.campaigns.length >= 1 && $scope.ob.step < 2 && !$scope.user_indexes[user_id].onboarding.campaigns) {
                    $scope.ob.step = 2;
                    $scope.user_indexes[user_id].onboarding.campaigns = true;
                    User.update($scope.user_indexes[user_id]);
                }

                $scope.showing_modal = false;
                $('#modal_edit_campaign').modal('hide');
            });
        }
    }

    $scope.delete_campaign = function(campaign) {
        campaign.status = 0;
        Campaign.delete(campaign.id).then(function(data) {
            Campaigns.Delete(campaign);
            Tasks.DeleteCampaign(campaign.id);
            $rootScope.$broadcast('event_campaign_deleted', campaign);
            $scope.select_campaign( $scope.campaigns[0] );
            $('#modal_delete_campaign').modal('hide');
        });
    }

    $scope.complete_task = function(task, $event) {
        var checkbox = $event.target;

        if (task.status != 3) {
            checkbox.checked = false;
            task.status = 1;
            $scope.editing_task = task;
            $('#modal_complete_task').modal('show');
        } else {
            checkbox.checked = true;
            $scope.toggle_task(task);
        }
    }

    $scope.add_note = function(note) {
        $scope.note = angular.copy($scope.new.note);
        $scope.note_mode = 'new';
        $('#modal_add_note').modal('show');
    };

    $scope.save_note = function(note) {
        note.campaign_id = $scope.current.id;

        if ($scope.note_mode == 'new') {
            CampaignNote.create(note).then(function(note) {
                $scope.note = angular.copy($scope.new.note);
                $('#modal_add_note').modal('hide');
            });
        } else {
            CampaignNote.update(note.campaign_id, note.id, { note: note.note }).then(function(note) {
                $scope.note = angular.copy($scope.new.note);
                $('#modal_add_note').modal('hide');
            });
        }
    }

    $scope.select_note = function(note) {
        $scope.selected_note = note;
    }

    $scope.edit_note = function(note) {
        $scope.note = note;
        $scope.note_mode = 'edit';
        $('#modal_add_note').modal('show');
    }

    $scope.delete_note = function(note) {
        CampaignNote.delete(note.campaign_id, note.id).then(function() {
            for (var i in $scope.current.notes) {
                if ($scope.current.notes[i].id == note.id) {
                    $scope.current.notes.splice(i, 1);
                }
            }

            $('#modal_delete_note').modal('hide');
        });
    }

    $scope.mark_notes_viewed = function(campaign) {
        var ids = [];

        for (var i in campaign.notes) {
            if (!campaign.notes[i].viewed) {
                ids.push( campaign.notes[i].id );
            }
        }

        if ($scope.notes_to_clear) {
            $scope.campaign_indexes[$scope.notes_to_clear].unviewed = false;
            for (var i in $scope.campaign_indexes[$scope.notes_to_clear].notes) {
                $scope.campaign_indexes[$scope.notes_to_clear].notes[i].viewed = true;
            }
            $scope.notes_to_clear = false;
        }

        if (ids.length) {
            CampaignNote.view(campaign.id, ids).then(function(res) {
                $scope.notes_to_clear = campaign.id;
            });
        }
    }

    $scope.filterDates = function(d) {
        var now = new Date();
        return now.getTime() <= d.getTime()
    }

    $scope.process_task_change = function(task) {
        $scope.campaign_indexes[ task.campaign_id ].has_overdue = false;
        var now = parseInt(new Date().getTime() / 1000);
        var due = new Date(task.due_on * 1000);
        var block_set = $scope.campaign_indexes[ task.campaign_id ].blocks[ (due.getFullYear()) + '/' + (due.getMonth() + 1) ];

        task.is_overdue = (now >= task.due_on) ? true : false;

        if ($scope.campaign_indexes[ task.campaign_id ]) {
            var task_exists = false;
            for (var i in $scope.campaign_indexes[ task.campaign_id ].tasks) {
                if ($scope.campaign_indexes[ task.campaign_id ].tasks[i].is_overdue && $scope.campaign_indexes[ task.campaign_id ].tasks[i].status == 1) {
                    $scope.campaign_indexes[ task.campaign_id ].has_overdue = true;
                }

                if ($scope.campaign_indexes[ task.campaign_id ].tasks[i].id == task.id) {
                    task_exists = true;

                    if (task.status == 0) {
                        // Task was deleted, remove it from primary task list
                        // as well as current block.
                        $scope.campaign_indexes[ task.campaign_id ].tasks.splice(i, 1);
                        for (var k in block_set.tasks) {
                            if (block_set.tasks[k].id == task.id) {
                                block_set.tasks.splice(k, 1);
                            }
                        }
                    } else {
                        // Update task's data
                        for (var k in task) {
                            $scope.campaign_indexes[ task.campaign_id ].tasks[i][k] = task[k];
                        }
                    }
                }
            }

            var completed = 0;

            if (!task_exists) {
                $scope.campaign_indexes[ task.campaign_id ].tasks.push(task);
                block_set.tasks.push(task);
            }

            for (var i in block_set.tasks) {
                if (block_set.tasks[i].status == 3) {
                    completed++;
                }
            }

            var diff = completed - block_set.completed;
            block_set.completed = completed;
            $scope.campaign_indexes[ task.campaign_id ].total_completed += diff;
            block_set.percent_done = block_set.completed / block_set.tasks.length * 100;
        }
    }

    $scope.onboarding_done = function() {
        if (typeof $scope.user_indexes[user_id].onboarding.campaigns == "undefined") {
            $scope.user_indexes[user_id].onboarding.campaigns = true;

            User.update($scope.user_indexes[user_id]).then(function() {
                console.log('Onboarding Done');
                console.log($scope.user_indexes[user_id].onboarding);
            });
        }
    }

    $scope.add_months = function() {
        if ($scope.months_to_add > 12) {
            $scope.months_to_add = 12;
        }

        var stop = new Date($scope.current.stop_at * 1000);
        stop.setDate(1);
        stop.setHours(0);
        stop.setMinutes(0);
        stop.setSeconds(0);
        stop.setMonth( stop.getMonth() + $scope.months_to_add + 1 );
        $scope.current.stop_at = parseInt(stop.getTime() / 1000) - 1;
        $scope.current.total_months = month_span($scope.current.start_at, $scope.current.stop_at) + 1;
        $scope.current.months_done = month_span($scope.current.start_at, new Date().getTime() / 1000);
        $scope.current.current_month = month_span($scope.current.start_at, new Date().getTime() / 1000) + 1;

        Campaign.update($scope.current).then(function(camp) {
            $scope.process_tasks( $scope.current );
            $('#modal_add_months').modal('hide');
        });
    }

    $scope.save_set = function() {
        $scope.set.tasks = [];

        for (var i in $scope.current.blocks[$scope.current_month - 1].tasks) {
            var task = {};

            for (var k in $scope.current.blocks[$scope.current_month - 1].tasks[i]) {
                task.title = $scope.current.blocks[$scope.current_month - 1].tasks[i].title;
                task.description = $scope.current.blocks[$scope.current_month - 1].tasks[i].description;
                task.assigned_to = $scope.current.blocks[$scope.current_month - 1].tasks[i].assigned_to;
            }

            $scope.set.tasks.push( task );
        }

        MonthTemplate.create($scope.set).then(function(template) {
            template.tasks = JSON.parse(template.tasks);
            console.log(template);
            $scope.month_template_indexes[template.id] = template;
            $scope.month_templates.push(template);
            $('#modal_create_set').modal('hide');
        });
    }

    $scope.load_month = function() {
        $scope.month_load_mode = 1;
        $scope.month_load_status = 0;

        var weight_per_task = 100 / $scope.month_template_indexes[$scope.selectedmt.id].tasks.length;



        for (var i in $scope.month_template_indexes[$scope.selectedmt.id].tasks) {
            var start = new Date($scope.current.start_at * 1000);
            start.setMonth( start.getMonth() + $scope.current_month - 1);
            var due_on = new Date(start.getTime());
            due_on.setDate(1);
            due_on.setHours(0);
            due_on.setMinutes(0);
            due_on.setSeconds(0);
            due_on.setMonth( start.getMonth() + 1 );
            due_on = parseInt(due_on.getTime() / 1000) - 1;

            var task = {
                title: $scope.month_template_indexes[$scope.selectedmt.id].tasks[i].title,
                description: $scope.month_template_indexes[$scope.selectedmt.id].tasks[i].description,
                assigned_to: $scope.month_template_indexes[$scope.selectedmt.id].tasks[i].assigned_to,
                campaign_id: $scope.current.id,
                company_id: $scope.current.company_id,
                due_on: due_on
            }

            Task.create(task).then(function(task) {
                $scope.month_load_status += weight_per_task;
                $scope.month_load_status = Math.ceil($scope.month_load_status);
                $rootScope.$broadcast('event_task_toggle', task);

                if ($scope.month_load_status >= 100) {
                    $('#modal_load_month').modal('hide');
                    $scope.month_load_mode = 0;
                }
            });
        }
    }

    $scope.edit_task = function(task) {
        $scope.editing_task = angular.copy(task);
        $scope.label_modal_edit_task = 'Edit Task';
        $scope.label_bt_edit_task = 'Save Task';
        $scope.editing_task.due_on_js = new Date($scope.editing_task.due_on * 1000);
    }

    $scope.new_task = function() {
        $scope.editing_task = angular.copy($scope.new.task);

        var start = new Date($scope.current.start_at * 1000);
        start.setMonth( start.getMonth() + $scope.current_month - 1);

        $scope.editing_task.due_on = new Date(start.getTime());
        $scope.editing_task.due_on.setDate(1);
        $scope.editing_task.due_on.setHours(0);
        $scope.editing_task.due_on.setMinutes(0);
        $scope.editing_task.due_on.setSeconds(0);
        $scope.editing_task.due_on.setMonth( start.getMonth() + 1 );

        $scope.editing_task.due_on = parseInt($scope.editing_task.due_on.getTime() / 1000) - 1;

        $scope.editing_task.due_on_js = new Date($scope.editing_task.due_on * 1000);

        $scope.label_modal_edit_task = 'Add Task to Timeline';
        $scope.label_bt_edit_task = 'Add New Task';
        $scope.show_modal();
    }

    $scope.show_modal = function() {
        $scope.showing_modal = true;
    }

    $scope.progress_ob = function(step) {
        $scope.ob.step = step;

        switch (step) {
            case 3:
                $scope.user_indexes[user_id].onboarding.tasks = true;
                $scope.ob.step = 3;

                User.update($scope.user_indexes[user_id]);
                break;
            case 4:
                $scope.user_indexes[user_id].onboarding.toggle = true;
                $scope.ob.step = 4;

                User.update($scope.user_indexes[user_id]);
                break;

            case 5:
                $scope.user_indexes[user_id].onboarding.notes = true;

                if ($scope.user_indexes[user_id].type > 0) {
                    $scope.user_indexes[user_id].onboarding.templates = true;
                    $scope.ob.step = 6;
                }

                User.update($scope.user_indexes[user_id]);
                break;
            case 6:
                $scope.user_indexes[user_id].onboarding.templates = true;
                User.update($scope.user_indexes[user_id]);
                break;

            case 7:
                $scope.ob_active = false;
                break;
        }


    }

    $scope.hide_task_typeahead = function() {
        setTimeout(function() {
            $scope.show_task_typeahead = false;
            $scope.$apply();
        }, 130);
    }

    $scope.choose_task_template = function(template) {
        $scope.editing_task.title = $scope.template_indexes[template.id].title;
        $scope.editing_task.description = $scope.template_indexes[template.id].description;
        $scope.show_task_typeahead = false;
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('socket_available', function(event, data) {
        Socket.emit('subscribe', { channel: 'task_activity_' + account_id });
        Socket.emit('subscribe', { channel: 'note_activity_' + account_id });
    });

    $scope.$on('task_activity_' + account_id, function(event, task) {
        task = JSON.parse(task);
        $scope.process_task_change(task);
    });

    $scope.$on('note_activity_' + account_id, function(event, note) {
        note = JSON.parse(note);
        if (note.user_id != user_id) {
            note.unviewed = true;
        }

        var exists = false;
        for (var i in $scope.campaign_indexes[note.campaign_id].notes) {
            if ($scope.campaign_indexes[note.campaign_id].notes[i].id == note.id) {
                exists = true;
                break;
            }
        }

        if (!exists) {
            $scope.campaign_indexes[note.campaign_id].notes.push( note );
        }
    });

    $scope.$on('event_task_toggle', function (event, data) {
        $scope.process_task_change( data );
    });

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    Campaigns.Ready().then(function() {
        $scope.campaigns = Campaigns.FindAll();
        $scope.campaign_indexes = Campaigns.FindAll(true);
        $scope.init_check();
    });

    Companies.Ready().then(function() {
        $scope.companies = Companies.FindAll();
        $scope.company_indexes = Companies.FindAll(true);

        if ($scope.companies.length < 1) {
            window.location = '/clients';
        }

        $scope.companies.sort(function(a, b) {
            if (a.name < b.name) {
                return -1;
            } else if (a.name > b.name) {
                return 1;
            } else {
                return 0;
            }
        });

        $scope.init_check();
    });

    Users.Ready().then(function() {
        $scope.users = Users.FindAll();
        $scope.user_indexes = Users.FindAll(true);
        $scope.sort_users();
        $scope.init_check();
    });

    TaskTemplates.Ready().then(function() {
        $scope.templates = TaskTemplates.FindAll();
        $scope.template_indexes = TaskTemplates.FindAll(true);

        $scope.templates.sort(function(a, b) {
            if (a.title.toLowerCase() < b.title.toLowerCase()) {
                return -1;
            } else if (a.title.toLowerCase() > b.title.toLowerCase()) {
                return 1;
            } else {
                return 0;
            }
        })
    });

    MonthTemplates.Ready().then(function() {
        $scope.month_templates = MonthTemplates.FindAll();
        $scope.month_template_indexes = MonthTemplates.FindAll(true);
    });

    $scope.$watch(function() {
        return location.hash
    }, function(old_value, new_value) {
        var hash = location.hash.split('/');

        if (hash[1] && !$scope.doing_init) {
            // Campaign changed
            if ($scope.campaign_indexes[hash[1]]) {
                $scope.select_campaign($scope.campaign_indexes[hash[1]]);

                // Find the Month the task is in
                if (hash[2]) {
                    $scope.campaign_indexes[hash[1]].blocks.forEach(function(item, i) {
                        for (var i in item.tasks) {
                            if (item.tasks[i].id == hash[2]) {
                                $scope.current_month = item.num;
                            }
                        }
                    });
                }
            }
        } else if (!hash[0]) {
            location.hash = '#/';
        }
    });

    $('#left-scroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});

});

app.config(function(ngQuickDateDefaultsProvider) {
    // Configure with icons from font-awesome
    return ngQuickDateDefaultsProvider.set({
        closeButtonHtml: "<i class='fa fa-times'></i>",
        buttonIconHtml: "<i class='fa fa-calendar'></i>",
        nextLinkHtml: "<i class='fa fa-chevron-right'></i>",
        prevLinkHtml: "<i class='fa fa-chevron-left'></i>"
    });
});