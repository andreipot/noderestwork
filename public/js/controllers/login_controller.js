app.controller('LoginController', function LoginController($scope, $cookies, User) {

    $scope.do_remember = false;

    $scope.creds = {};

    // -------------------------
    //   Functions
    // -------------------------

    $scope.remember_me = function() {
        $scope.do_remember = !$scope.do_remember;
        if (!$scope.do_remember) {
            window.localStorage.removeItem('remember_me');
        }
    }

    $scope.login = function() {
        if ($scope.creds.password && $scope.creds.email) {
            window.localStorage.setItem('remember_me', JSON.stringify({
                e: $scope.creds.email,
                p: $scope.creds.password
            }));
        }
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    if (window.localStorage.getItem('remember_me')) {
        $scope.do_remember = true;
        $scope.creds = JSON.parse(window.localStorage.getItem('remember_me'));
    }

});