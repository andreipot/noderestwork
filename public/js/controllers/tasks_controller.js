app.controller('TasksController', function TasksController($scope, $cookies, TaskTemplate) {

    $scope.tasks = [];
    $scope.current = false;
    $scope.mode = 'new';
    $scope.modal_title_edit_task = 'Add New Task';
    $scope.target_height = 1024;

    $scope.new = {
        task: {
            title: '',
            description: ''
        }
    }

    $scope.editing = angular.copy($scope.new.task);

    $scope.select_task = function(task) {
        $scope.current = task;
        $scope.mode = 'editing';
        $scope.modal_title_edit_task = 'Edit Task';
        $scope.editing = angular.copy(task);
    }

    $scope.new_task = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_task = 'Add New Task';
        $scope.editing = angular.copy($scope.new.task);
        $scope.form_edit_task.$setPristine();
    }

    $scope.edit_task = function(task) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_task = 'Edit Task';
        $scope.editing = angular.copy(task);
        $scope.form_edit_task.$setPristine();
    }

    $scope.delete_task = function(task) {
        var task_promise = TaskTemplate.delete(task.id);
        task_promise.then(function(data) {
            for (var i in $scope.tasks) {
                if ($scope.tasks[i].id == task.id) {
                    $scope.tasks.splice(i, 1);
                    $scope.current = $scope.tasks[0];
                }
            }
            $('#modal_delete_task').modal('hide');
        });
    }

    /**
     * Saves or Creates a new TaskTemplate
     */
    $scope.save_task = function(task) {
        if ($scope.mode == 'new') {
            var task_promise = TaskTemplate.create(task);
            task_promise.then(function(task) {
                $scope.tasks.push(task);
                $scope.select_task(task);
                $('#modal_edit_task').modal('hide');
            });
        } else {
            var task_promise = TaskTemplate.update(task);
            task_promise.then(function(task) {
                $('#modal_edit_task').modal('hide');
            });
        }

    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    var tasks_promise = TaskTemplate.list();
    tasks_promise.then(function(data) {
        for (var i in data) {
            $scope.tasks.push(data[i]);
        }
        $scope.select_task($scope.tasks[0]);
        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    });

});