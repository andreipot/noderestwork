app.controller('ChangePasswordController', function ChangePasswordController($scope, $cookies, User) {

    $scope.accounts = [];
    $scope.selected = '';
    $scope.target_height = 1024;
    $scope.processing = false;

    $scope.new = {
        pwd_set: {
            pwd_one: '',
            pwd_two: ''
        }
    }

    $scope.done = false;

    $scope.editing = angular.copy($scope.new.pwd_set);
    $scope.modal = angular.copy($scope.new.pwd_set);

    // -------------------------
    //   Functions
    // -------------------------

    $scope.save_pwd = function(editing) {
        if (editing.pwd_one == editing.pwd_two) {
            $scope.processing = true;
            User.update({
                id: user_id,
                password: editing.pwd_one
            }).then(function() {
                $scope.processing = false;
                $scope.editing = angular.copy($scope.new.pwd_set);
                $scope.done = true;
            });
        }
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    // ~~

});