app.controller('ManageUsersController', function ManageUsersController($scope, $cookies, User, Users) {

    $scope.users = [];
    $scope.selected = '';
    $scope.user_indexes = {};
    $scope.target_height = 1024;

    $scope.new = {
        user: {
            fname: '',
            lname: '',
            email: '',
            type: ''
        }
    }

    $scope.editing = angular.copy($scope.new.user);
    $scope.creating = angular.copy($scope.new.user);

    $scope.mode = 'new';
    $scope.modal_title_edit_user = 'Add New User';

    $scope.select_user = function(user) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_user = 'Edit User';
        $scope.editing = angular.copy(user);
        $scope.form_edit_entry.$setPristine();
    }

    $scope.new_user = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_user = 'New User';
        $scope.creating = angular.copy($scope.new.user);
        $scope.form_add_entry.$setPristine();
    }

    $scope.save_user = function(user) {
        if ($scope.mode == 'new') {
            var user_promise = User.create(user);
            user_promise.then(function(user) {
                $scope.users.push(user);
                $scope.sort_users();
                $scope.select_user(user);
                $('#modal_edit_user').modal('hide');
            });
        } else {
            var user_promise = User.update(user);
            user_promise.then(function(user) {
                for (var i in $scope.users) {
                    if (user.id == $scope.users[i].id) {
                        $scope.users[i] = user;
                        return;
                    }
                }
                $('#modal_edit_user').modal('hide');
            });
        }

    }

    $scope.delete_user = function(user) {
        var user_promise = User.delete(user.id);
        user_promise.then(function(data) {
            for (var i in $scope.users) {
                if ($scope.users[i].id == user.id) {
                    $scope.users.splice(i, 1);
                }
            }
            $scope.select_user($scope.users[0]);
            $('#modal_delete_user').modal('hide');
        });
    }

    $scope.sort_users = function() {
        $scope.users.sort(function(a, b) {
            if (a.fname.toUpperCase() < b.fname.toUpperCase()) {
                return -1;
            } else if (a.fname.toUpperCase() > b.fname.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    $scope.onboarding_done = function() {
        if (typeof $scope.user_indexes[user_id].onboarding.users == "undefined") {
            $scope.user_indexes[user_id].onboarding.users = true;

            User.update($scope.user_indexes[user_id]).then(function() {
                console.log('Onboarding Done');
                console.log($scope.user_indexes[user_id].onboarding);
            });
        }
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    Users.Ready().then(function(users) {
        $scope.users = Users.FindAll();
        $scope.user_indexes = Users.FindAll(true);

        $scope.select_user($scope.users[0]);

        if (!$scope.user_indexes[user_id].onboarding.users) {
            //$scope.do_onboarding = true;
        }

        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    })

});