app.controller('ManageAccountsController', function ManageAccountsController($scope, $cookies, Account, User) {

    $scope.accounts = [];
    $scope.selected = '';
    $scope.target_height = 1024;

    $scope.new = {
        account: {
            billing_fname: '',
            billing_lname: '',
            billing_email: '',
            billing_company: '',
            plan_id: ''
        },
        admin: {
            fname: '',
            lname: '',
            email: '',
            password: '',
            password2: ''
        }
    }

    $scope.editing = angular.copy($scope.new.account);
    $scope.modal = angular.copy($scope.new.account);
    $scope.admin = angular.copy($scope.new.admin);

    $scope.mode = 'new';

    $scope.modal_title_edit_account = 'Add New Account';

    $scope.select_account = function(account) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_account = 'Edit Account';
        $scope.editing = account;
        $scope.form_edit_entry.$setPristine();
    }

    $scope.new_account = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_account = 'New Account';
        $scope.modal = angular.copy($scope.new.account);
        $scope.admin = angular.copy($scope.new.admin);
        $scope.form_add_entry.$setPristine();
    }

    $scope.save_account = function(account) {
        if ($scope.mode == 'new') {
            Account.create(account).then(function(account) {
                $scope.accounts.push(account);
                $scope.sort_accounts();
                $scope.select_account( account );

                $scope.admin.account_id = account.id;
                $scope.admin.type = 0;

                User.create($scope.admin).then(function(user) {
                    account.user_count = 1;
                    account.cost = 30;
                    account.created_at = new Date(account.created_at).toString();

                    $('#modal_edit_account').modal('hide');
                    $scope.form_add_entry.$setPristine();
                });
            });
        } else {
            var account_promise = Account.update(account);
            account_promise.then(function(account) {
                $('#modal_edit_account').modal('hide');
                $scope.form_edit_entry.$setPristine();
            });
        }

    }

    $scope.delete_account = function(account) {
        var account_promise = Account.delete(account.id);
        account_promise.then(function(data) {
            for (var i in $scope.accounts) {
                if ($scope.accounts[i].id == account.id) {
                    $scope.accounts.splice(i, 1);
                }
            }
            $scope.select_account( $scope.accounts[0] );
            $('#modal_delete_account').modal('hide');
        });
    }

    $scope.sort_accounts = function() {
        $scope.accounts.sort(function(a, b) {
            if (a.billing_fname.toUpperCase() < b.billing_fname.toUpperCase()) {
                return -1;
            } else if (a.billing_fname.toUpperCase() > b.billing_fname.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    var accounts_promise = Account.list();
    accounts_promise.then(function(data) {
        for (var i in data) {
            if (data[i].user_count > 2) {
                data[i].cost = 30 + ((data[i].user_count - 2) * 10);
            } else {
                data[i].cost = 30;
            }
            data[i].created_at = new Date(data[i].created_at).toString();
            $scope.accounts.push(data[i]);
        }
        $scope.sort_accounts();
        $scope.select_account( $scope.accounts[0] );
        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    });

});