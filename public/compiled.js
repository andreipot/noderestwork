var app = angular.module('WorkadoApp', [
    'WorkadoApp.Socket', 'WorkadoApp.Users', 'WorkadoApp.Tasks', 'WorkadoApp.TaskTemplates',
    'WorkadoApp.Companies', 'WorkadoApp.Campaigns', 'WorkadoApp.Accounts', 'WorkadoApp.MonthTemplates',
    'ngResource', 'ngCookies', 'ngAnimate', 'ngQuickDate'
]);

app.run(function($rootScope, Socket) {

    /**
     * Receive emitted message and broadcast it.
     */
    $rootScope.$on('new_broadcast', function(event, args) {
        if (args && args.channel && args.data) {
            $rootScope.$broadcast(args.channel, args.data);
        } else {
            $rootScope.$broadcast('broadcast', args);
        }
    });

    /**
     * Check for a Socket.io connection; prepare it
     */
    if (Socket && Socket.on) {
        Socket.on('connect', function() {
            $rootScope.$broadcast('socket_available', true);
            console.log('Socket available');
        });

        Socket.on('subscription_event', function( event ) {
            if (event && event.channel && event.event) {
                $rootScope.$broadcast(event.channel, event.event);
                console.log('Socket Event', event.channel, event.event);
            }
        });

        Socket.on('subscription_ready', function( event ) {
            if (event && event.channel) {
                $rootScope.$broadcast('subscription_ready', event.channel);
                console.log('Subscription [' + event.channel + '] is ready!');
            }
        });

        Socket.on('disconnect', function() {
            console.log('Lost socket connection.');
        });
    }

});

app.filter('ts_to_date', function ts_to_date($filter){
    return function(text) {
        text = parseInt(text) || 0;
        var template = new Date(parseInt(text) * 1000);
        return $filter('date')(template, "MM/dd/yyyy");
    }
});

app.filter('ts_to_datetime', function ts_to_date($filter){
    return function(text) {
        text = parseInt(text) || 0;
        var template = new Date(parseInt(text) * 1000);
        return $filter('date')(template, "MM/dd/yyyy h:mm:ss a");
    }
});

app.filter('dt_to_date', function dt_to_date($filter){
    return function(text) {
        var template = new Date(text);
        return $filter('date')(template, "MM/dd/yyyy");
    }
});

app.filter('dt_to_datetime', function ts_to_datetime($filter){
    return function(text) {
        var template = new Date(text);
        return $filter('date')(template, "MM/dd/yyyy h:mm:ss a");
    }
});

app.filter('ts_sort',function() {
    function sort(a, b) {
        return new Date(b.updated_at).getTime() - new Date(a.updated_at).getTime();
    }

    return function(arrInput) {
        var arr = arrInput.sort(function(a, b) {
            return sort(a, b);
        });
        return arr;
    }
});

app.filter('campaign_sort',function() {
    function sort(a, b) {
        // Step 1 - Compare Status
        if (a.status != b.status) {
            return a.status - b.status;
        }

        // Step 2 - Compare Client Name
        if (a.company_name != b.company_name) {
            if (a.company_name < b.company_name) {
                return -1;
            }
            if (a.company_name > b.company_name) {
                return 1;
            }
            return 0;
        }

        // Step 3 - Compare Campaign Name
        if (a.name != b.name) {
            if (a.name < b.name) {
                return -1;
            }
            if (a.name > b.name) {
                return 1;
            }
            return 0;
        }
    }

    return function(arrInput) {
        var arr = arrInput.sort(function(a, b) {
            return sort(a, b);
        });
        return arr;
    }
});

app.filter('completion_sort',function() {
    function sort(a, b) {
        return a.completed_on - b.completed_on;
    }

    return function(arrInput) {
        var arr = arrInput.sort(function(a, b) {
            return sort(a, b);
        });
        return arr;
    }
})

app.filter('completed_last',function() {
    return function(arr) {
        if (arr && arr.sort) {
            arr.sort(function (a, b) {
                if (a.status >= 0 && b.status >= 0) {
                    return a.status - b.status;
                } else {
                    return 0;
                }
            });
            return arr;
        } else {
            return;
        }
    }
})

app.filter('created_sort',function() {
    function sort(a, b) {
        return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
    }

    return function(arrInput) {
        if (arrInput && arrInput.sort) {
            var arr = arrInput.sort(function(a, b) {
                return sort(a, b);
            });
        }
        return arr;
    }
})

/**
 * Calculates the difference between 2 dates in months (integer)
 */
var month_span = function(start, stop) {
    start = new Date(start * 1000);
    stop = new Date(stop * 1000);

    if (start.getYear() < stop.getYear()) {
        var result = stop.getMonth() - start.getMonth() + 12;
    } else {
        var result = stop.getMonth() - start.getMonth();
    }

    return result;
}

/**
 * Returns random CDN paths
 */
var cdn_asset = function(file) {
    return cdn_assets.options[ Math.floor(Math.random() * cdn_assets.options.length) ] + file + '?v=' + cdn_assets.version;
}

/**
 * MD5 Hasing via PHPJS Project
 */
function md5(str) {
    //  discuss at: http://phpjs.org/functions/md5/
    // original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // improved by: Michael White (http://getsprink.com)
    // improved by: Jack
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //    input by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //  depends on: utf8_encode
    //   example 1: md5('Kevin van Zonneveld');
    //   returns 1: '6e658d4bfcb59cc13f96c14450ac40b9'

    var xl;

    var rotateLeft = function(lValue, iShiftBits) {
        return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
    };

    var addUnsigned = function(lX, lY) {
        var lX4, lY4, lX8, lY8, lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
    };

    var _F = function(x, y, z) {
        return (x & y) | ((~x) & z);
    };
    var _G = function(x, y, z) {
        return (x & z) | (y & (~z));
    };
    var _H = function(x, y, z) {
        return (x ^ y ^ z);
    };
    var _I = function(x, y, z) {
        return (y ^ (x | (~z)));
    };

    var _FF = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var _GG = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var _HH = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var _II = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var convertToWordArray = function(str) {
        var lWordCount;
        var lMessageLength = str.length;
        var lNumberOfWords_temp1 = lMessageLength + 8;
        var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
        var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
        var lWordArray = new Array(lNumberOfWords - 1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while (lByteCount < lMessageLength) {
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount - (lByteCount % 4)) / 4;
        lBytePosition = (lByteCount % 4) * 8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
        lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
        lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
        return lWordArray;
    };

    var wordToHex = function(lValue) {
        var wordToHexValue = '',
            wordToHexValue_temp = '',
            lByte, lCount;
        for (lCount = 0; lCount <= 3; lCount++) {
            lByte = (lValue >>> (lCount * 8)) & 255;
            wordToHexValue_temp = '0' + lByte.toString(16);
            wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
        }
        return wordToHexValue;
    };

    var x = [],
        k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
        S12 = 12,
        S13 = 17,
        S14 = 22,
        S21 = 5,
        S22 = 9,
        S23 = 14,
        S24 = 20,
        S31 = 4,
        S32 = 11,
        S33 = 16,
        S34 = 23,
        S41 = 6,
        S42 = 10,
        S43 = 15,
        S44 = 21;

    str = this.utf8_encode(str);
    x = convertToWordArray(str);
    a = 0x67452301;
    b = 0xEFCDAB89;
    c = 0x98BADCFE;
    d = 0x10325476;

    xl = x.length;
    for (k = 0; k < xl; k += 16) {
        AA = a;
        BB = b;
        CC = c;
        DD = d;
        a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
        d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
        c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
        b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
        a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
        d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
        c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
        b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
        a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
        d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
        c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
        b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
        a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
        d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
        c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
        b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
        a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
        d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
        c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
        b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
        a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
        d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
        c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
        b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
        a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
        d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
        c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
        b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
        a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
        d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
        c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
        b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
        a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
        d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
        c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
        b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
        a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
        d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
        c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
        b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
        a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
        d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
        c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
        b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
        a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
        d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
        c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
        b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
        a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
        d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
        c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
        b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
        a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
        d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
        c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
        b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
        a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
        d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
        c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
        b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
        a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
        d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
        c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
        b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
        a = addUnsigned(a, AA);
        b = addUnsigned(b, BB);
        c = addUnsigned(c, CC);
        d = addUnsigned(d, DD);
    }

    var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

    return temp.toLowerCase();
}

function utf8_encode(argString) {
    //  discuss at: http://phpjs.org/functions/utf8_encode/
    // original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: sowberry
    // improved by: Jack
    // improved by: Yves Sucaet
    // improved by: kirilloid
    // bugfixed by: Onno Marsman
    // bugfixed by: Onno Marsman
    // bugfixed by: Ulrich
    // bugfixed by: Rafal Kukawski
    // bugfixed by: kirilloid
    //   example 1: utf8_encode('Kevin van Zonneveld');
    //   returns 1: 'Kevin van Zonneveld'

    if (argString === null || typeof argString === 'undefined') {
        return '';
    }

    var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var utftext = '',
        start, end, stringl = 0;

    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;

        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode(
                (c1 >> 6) | 192, (c1 & 63) | 128
            );
        } else if (c1 & 0xF800 != 0xD800) {
            enc = String.fromCharCode(
                (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
            );
        } else { // surrogate pairs
            if (c1 & 0xFC00 != 0xD800) {
                throw new RangeError('Unmatched trail surrogate at ' + n);
            }
            var c2 = string.charCodeAt(++n);
            if (c2 & 0xFC00 != 0xDC00) {
                throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
            }
            c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
            enc = String.fromCharCode(
                (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
            );
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }

    if (end > start) {
        utftext += string.slice(start, stringl);
    }

    return utftext;
}

(function(){var a;a=angular.module("ngQuickDate",[]),a.provider("ngQuickDateDefaults",function(){return{options:{dateFormat:"M/d/yyyy",timeFormat:"h:mm a",labelFormat:null,placeholder:"Click to Set Date",hoverText:null,buttonIconHtml:null,closeButtonHtml:"&times;",nextLinkHtml:"Next &rarr;",prevLinkHtml:"&larr; Prev",disableTimepicker:!1,disableClearButton:!1,defaultTime:null,dayAbbreviations:["Su","M","Tu","W","Th","F","Sa"],dateFilter:null,parseDateFunction:function(a){var b;return b=Date.parse(a),isNaN(b)?null:new Date(b)}},$get:function(){return this.options},set:function(a,b){var c,d,e;if("object"==typeof a){e=[];for(c in a)d=a[c],e.push(this.options[c]=d);return e}return this.options[a]=b}}}),a.directive("quickDatepicker",["ngQuickDateDefaults","$filter","$sce",function(a,b,c){return{restrict:"E",require:"?ngModel",scope:{dateFilter:"=?",onChange:"&",required:"@"},replace:!0,link:function(d,e,f,g){var h,i,j,k,l,m,n,o,p,q,r,s,t;return m=function(){return q(),d.toggleCalendar(!1),d.weeks=[],d.inputDate=null,d.inputTime=null,d.invalid=!0,"string"==typeof f.initValue&&g.$setViewValue(f.initValue),p(),o()},q=function(){var b,e;for(b in a)e=a[b],b.match(/[Hh]tml/)?d[b]=c.trustAsHtml(a[b]||""):!d[b]&&f[b]?d[b]=f[b]:d[b]||(d[b]=a[b]);return d.labelFormat||(d.labelFormat=d.dateFormat,d.disableTimepicker||(d.labelFormat+=" "+d.timeFormat)),f.iconClass&&f.iconClass.length?d.buttonIconHtml=c.trustAsHtml("<i ng-show='iconClass' class='"+f.iconClass+"'></i>"):void 0},i=!1,window.document.addEventListener("click",function(){return d.calendarShown&&!i&&(d.toggleCalendar(!1),d.$apply()),i=!1}),angular.element(e[0])[0].addEventListener("click",function(){return i=!0}),o=function(){var a;return a=g.$modelValue?new Date(g.$modelValue):null,s(),r(a),d.mainButtonStr=a?b("date")(a,d.labelFormat):d.placeholder,d.invalid=g.$invalid},r=function(a){return null!=a?(d.inputDate=b("date")(a,d.dateFormat),d.inputTime=b("date")(a,d.timeFormat)):(d.inputDate=null,d.inputTime=null)},p=function(a){var b;return null==a&&(a=null),b=null!=a?new Date(a):new Date,"Invalid Date"===b.toString()&&(b=new Date),b.setDate(1),d.calendarDate=new Date(b)},s=function(){var a,b,c,e,f,h,i,k,m,n,o,p,q,r;for(h=d.calendarDate.getDay(),e=l(d.calendarDate.getFullYear(),d.calendarDate.getMonth()),f=Math.ceil((h+e)/7),o=[],a=new Date(d.calendarDate),a.setDate(a.getDate()+-1*h),i=p=0,r=f-1;r>=0?r>=p:p>=r;i=r>=0?++p:--p)for(o.push([]),c=q=0;6>=q;c=++q)b=new Date(a),d.defaultTime&&(m=d.defaultTime.split(":"),b.setHours(m[0]||0),b.setMinutes(m[1]||0),b.setSeconds(m[2]||0)),k=g.$modelValue&&b&&j(b,g.$modelValue),n=j(b,new Date),o[i].push({date:b,selected:k,disabled:"function"==typeof d.dateFilter?!d.dateFilter(b):!1,other:b.getMonth()!==d.calendarDate.getMonth(),today:n}),a.setDate(a.getDate()+1);return d.weeks=o},g.$parsers.push(function(a){return d.required&&null==a?(g.$setValidity("required",!1),null):angular.isDate(a)?(g.$setValidity("required",!0),a):angular.isString(a)?(g.$setValidity("required",!0),d.parseDateFunction(a)):null}),g.$formatters.push(function(a){return angular.isDate(a)?a:angular.isString(a)?d.parseDateFunction(a):void 0}),h=function(a,c){return b("date")(a,c)},t=function(a){return"string"==typeof a?n(a):a},n=a.parseDateFunction,j=function(a,b,c){return null==c&&(c=!1),c?a-b===0:(a=t(a),b=t(b),a&&b&&a.getYear()===b.getYear()&&a.getMonth()===b.getMonth()&&a.getDate()===b.getDate())},k=function(a,b){return a&&b?parseInt(a.getTime()/6e4)===parseInt(b.getTime()/6e4):!1},l=function(a,b){return[31,a%4===0&&a%100!==0||a%400===0?29:28,31,30,31,30,31,31,30,31,30,31][b]},g.$render=function(){return p(g.$viewValue),o()},g.$viewChangeListeners.unshift(function(){return p(g.$viewValue),o(),d.onChange?d.onChange():void 0}),d.$watch("calendarShown",function(a){var b;return a?(b=angular.element(e[0].querySelector(".quickdate-date-input"))[0],b.select()):void 0}),d.toggleCalendar=function(a){return d.calendarShown=isFinite(a)?a:!d.calendarShown},d.selectDate=function(a,b){var c;return null==b&&(b=!0),c=!g.$viewValue&&a||g.$viewValue&&!a||a&&g.$viewValue&&a.getTime()!==g.$viewValue.getTime(),"function"!=typeof d.dateFilter||d.dateFilter(a)?(g.$setViewValue(a),b&&d.toggleCalendar(!1),!0):!1},d.selectDateFromInput=function(a){var b,c,e,f;null==a&&(a=!1);try{if(c=n(d.inputDate),!c)throw"Invalid Date";if(!d.disableTimepicker&&d.inputTime&&d.inputTime.length&&c){if(f=d.disableTimepicker?"00:00:00":d.inputTime,e=n(""+d.inputDate+" "+f),!e)throw"Invalid Time";c=e}if(!k(g.$viewValue,c)&&!d.selectDate(c,!1))throw"Invalid Date";return a&&d.toggleCalendar(!1),d.inputDateErr=!1,d.inputTimeErr=!1}catch(h){if(b=h,"Invalid Date"===b)return d.inputDateErr=!0;if("Invalid Time"===b)return d.inputTimeErr=!0}},d.onDateInputTab=function(){return d.disableTimepicker&&d.toggleCalendar(!1),!0},d.onTimeInputTab=function(){return d.toggleCalendar(!1),!0},d.nextMonth=function(){return p(new Date(new Date(d.calendarDate).setMonth(d.calendarDate.getMonth()+1))),o()},d.prevMonth=function(){return p(new Date(new Date(d.calendarDate).setMonth(d.calendarDate.getMonth()-1))),o()},d.clear=function(){return d.selectDate(null,!0)},m()},template:"<div class='quickdate'>\n  <a href='' ng-focus='toggleCalendar()' ng-click='toggleCalendar()' class='quickdate-button' title='{{hoverText}}'><div ng-hide='iconClass' ng-bind-html='buttonIconHtml'></div>{{mainButtonStr}}</a>\n  <div class='quickdate-popup' ng-class='{open: calendarShown}'>\n    <a href='' tabindex='-1' class='quickdate-close' ng-click='toggleCalendar()'><div ng-bind-html='closeButtonHtml'></div></a>\n    <div class='quickdate-text-inputs'>\n      <div class='quickdate-input-wrapper'>\n        <label>Date</label>\n        <input class='quickdate-date-input' ng-class=\"{'ng-invalid': inputDateErr}\" name='inputDate' type='text' ng-model='inputDate' placeholder='1/1/2013' ng-enter=\"selectDateFromInput(true)\" ng-blur=\"selectDateFromInput(false)\" on-tab='onDateInputTab()' />\n      </div>\n      <div class='quickdate-input-wrapper' ng-hide='disableTimepicker'>\n        <label>Time</label>\n        <input class='quickdate-time-input' ng-class=\"{'ng-invalid': inputTimeErr}\" name='inputTime' type='text' ng-model='inputTime' placeholder='12:00 PM' ng-enter=\"selectDateFromInput(true)\" ng-blur=\"selectDateFromInput(false)\" on-tab='onTimeInputTab()'>\n      </div>\n    </div>\n    <div class='quickdate-calendar-header'>\n      <a href='' class='quickdate-prev-month quickdate-action-link' tabindex='-1' ng-click='prevMonth()'><div ng-bind-html='prevLinkHtml'></div></a>\n      <span class='quickdate-month'>{{calendarDate | date:'MMMM yyyy'}}</span>\n      <a href='' class='quickdate-next-month quickdate-action-link' ng-click='nextMonth()' tabindex='-1' ><div ng-bind-html='nextLinkHtml'></div></a>\n    </div>\n    <table class='quickdate-calendar'>\n      <thead>\n        <tr>\n          <th ng-repeat='day in dayAbbreviations'>{{day}}</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr ng-repeat='week in weeks'>\n          <td ng-mousedown='selectDate(day.date, true, true)' ng-class='{\"other-month\": day.other, \"disabled-date\": day.disabled, \"selected\": day.selected, \"is-today\": day.today}' ng-repeat='day in week'>{{day.date | date:'d'}}</td>\n        </tr>\n      </tbody>\n    </table>\n    <div class='quickdate-popup-footer'>\n      <a href='' class='quickdate-clear' tabindex='-1' ng-hide='disableClearButton' ng-click='clear()'>Clear</a>\n    </div>\n  </div>\n</div>"}}]),a.directive("ngEnter",function(){return function(a,b,c){return b.bind("keydown keypress",function(b){return 13===b.which?(a.$apply(c.ngEnter),b.preventDefault()):void 0})}}),a.directive("onTab",function(){return{restrict:"A",link:function(a,b,c){return b.bind("keydown keypress",function(b){return 9!==b.which||b.shiftKey?void 0:a.$apply(c.onTab)})}}})}).call(this);
app.controller('BillingController', function BillingController($scope, Billing, Accounts, Users) {

    $scope.blank = {
        cardholder_name: '',
        card_number: '',
        card_exp_month: '',
        card_exp_year: '',
        card_cvv: ''
    }

    $scope.info = angular.copy($scope.blank);

    $scope.section = 'update';
    $scope.processing = false;
    $scope.users = [];
    $scope.total_costs = 0;

    $scope.did_success = false;
    $scope.did_failure = false;
    $scope.error_message = 'There was a problem updating your billing details.';

    $scope.update = function(info) {
        $scope.did_success = false;
        $scope.did_failure = false;
        $scope.processing = true;

        var account = Accounts.Get(account_id);

        if (account && account.stripe_card_id) {
            Billing.update(info, function(error, data) {
                $scope.processing = false;

                if (error) {
                    $scope.did_failure = true;
                    $scope.error_message = error.message.message;
                } else {
                    $scope.did_success = true;
                    $scope.info = angular.copy($scope.blank);
                }

                $scope.form_update_billing.$setPristine();
                $('#billing-alert').focus();
            });
        } else {
            Billing.create(info, function(error, data) {
                $scope.processing = false;

                if (error) {
                    $scope.did_failure = true;
                    $scope.error_message = error.message.message;
                } else {
                    $scope.did_success = true;
                    $scope.info = angular.copy($scope.blank);
                }

                $scope.form_update_billing.$setPristine();
                $('#billing-alert').focus();
            });
        }
    }

    $scope.delete = function() {
        $scope.did_success = false;
        $scope.did_failure = false;
        $scope.processing = true;
        Billing.delete(function(error, data) {
            $scope.processing = false;

            if (error) {
                $scope.did_failure = true;
                $scope.error_message = error.message.message;
            } else {
                $scope.did_success = true;
            }

            $('#modal_confirm_cancel').modal('hide');
            $('#billing-alert-d').focus();
        });
    }

    $scope.select_section = function(section) {
        $scope.did_success = false;
        $scope.did_failure = false;
        $scope.section = section;
    }

    // --
    // Init
    // --

    Accounts.Ready().then(function() {
        $scope.account = Accounts.Get(account_id);
    });

    Users.Ready().then(function() {
        $scope.users = Users.FindAll();

        if ($scope.users.length > 2) {
            $scope.total_costs = (($scope.users.length - 2) * 10) + 30;
        } else {
            $scope.total_costs = 30;
        }
    });
});
app.controller('CampaignsController', function CampaignsController($scope, $rootScope, $cookies, Company, Companies, Campaign, Campaigns, CampaignNote, Task, Tasks, User, Users, TaskTemplate, TaskTemplates, MonthTemplate, MonthTemplates, Socket) {

    $scope.month_names = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

    $scope.campaigns = [];
    $scope.campaign_indexes = [];
    $scope.companies = [];
    $scope.company_indexes = [];
    $scope.users = [];
    $scope.user_indexes = [];
    $scope.templates = [];
    $scope.template_indexes = [];
    $scope.month_templates = [];
    $scope.month_template_indexes = [];
    $scope.show_task_typeahead = false;

    $scope.init_status = 0;
    $scope.ob_active = 0;
    $scope.showing_modal = false;
    $scope.doing_init = true;

    $scope.current = false;
    $scope.actual_month = 1;
    $scope.current_month = 1;
    $scope.selected_template = '';
    $scope.selected_task = false;
    $scope.selectedmt = '';
    $scope.current_notes = [];
    $scope.target_height = 1024;
    $scope.notes_to_clear = false;
    $scope.months_to_add = 1;
    $scope.month_load_status = 0
    $scope.month_load_mode = 0;
    $scope.label_modal_edit_task = 'Add Task to Timeline';
    $scope.label_bt_edit_task = 'Add New Task';
    $scope.label_modal_edit_campaign = 'New Campaign';
    $scope.label_bt_edit_campaign = 'Add New Campaign';

    $scope.new = {
        task: {
            title: '',
            description: '',
            selected_template: ''
        },
        campaign: {
            company_id: '',
            start_at: '',
            stop_at: ''
        },
        note: {
            note: ''
        },
        set: {
            name: ''
        }
    }

    $scope.editing_task = angular.copy($scope.new.task);
    $scope.new_campaign = angular.copy($scope.new.campaign);
    $scope.note         = angular.copy($scope.new.note);
    $scope.set          = angular.copy($scope.new.set);

    $scope.ob = {
        step: 0
    }


    $scope.init_check = function() {
        $scope.init_status++;

        if ($scope.init_status == 3) {
            $scope.prepare_data();
            //$scope.refresh_workwall();
        }
    }

    $scope.prepare_data = function() {
        if ($scope.campaigns.length) {

            for (var i in $scope.campaigns) {
                $scope.campaigns[i].total_months = month_span($scope.campaigns[i].start_at, $scope.campaigns[i].stop_at) + 1;
                $scope.campaigns[i].months_done = month_span($scope.campaigns[i].start_at, new Date().getTime() / 1000) + 1;
                $scope.campaigns[i].current_month = month_span($scope.campaigns[i].start_at, new Date().getTime() / 1000) + 1;
                $scope.campaigns[i].unviewed = false;
                $scope.campaigns[i].company_name = $scope.company_indexes[$scope.campaigns[i].company_id].name;

                for (var k in $scope.campaigns[i].notes) {
                    if (!$scope.campaigns[i].notes[k].viewed) {
                        $scope.campaigns[i].unviewed = true;
                        break;
                    }
                }

                // Sort Tasks into month blocks and tag overdues
                $scope.process_tasks( $scope.campaigns[i] );
            }

            // Sort Campaigns
            $scope.campaigns.sort(function(a, b) {
                // Step 1 - Compare Status
                if (a.status != b.status) {
                    return a.status - b.status;
                }

                // Step 2 - Compare Client Name
                if (a.company_name != b.company_name) {
                    if (a.company_name < b.company_name) {
                        return -1;
                    }
                    if (a.company_name > b.company_name) {
                        return 1;
                    }
                    return 0;
                }

                // Step 3 - Compare Campaign Name
                if (a.name != b.name) {
                    if (a.name < b.name) {
                        return -1;
                    }
                    if (a.name > b.name) {
                        return 1;
                    }
                    return 0;
                }
            });

            if ($scope.campaigns.length && !$scope.user_indexes[user_id].onboarding.campaigns) {
                $scope.user_indexes[user_id].onboarding.campaigns = true;
                User.update($scope.user_indexes[user_id]);
            }

            if ($scope.campaigns.length && !$scope.user_indexes[user_id].onboarding.tasks) {
                if ($scope.user_indexes[user_id].type > 0) {
                    $scope.user_indexes[user_id].onboarding.tasks = true;
                    User.update($scope.user_indexes[user_id]);
                    $scope.ob.step = 3;
                } else {
                    $scope.ob.step = 2;
                }
                $scope.ob_active = true;

            }

            if ($scope.campaigns.length && $scope.user_indexes[user_id].onboarding.tasks && !$scope.user_indexes[user_id].onboarding.toggle) {
                $scope.ob_active = true;
                $scope.ob.step = 3;
            }

            if ($scope.campaigns.length && $scope.user_indexes[user_id].onboarding.tasks && $scope.user_indexes[user_id].onboarding.toggle && !$scope.user_indexes[user_id].onboarding.notes) {
                $scope.ob_active = true;
                $scope.ob.step = 4;
            }

            if ($scope.campaigns.length && $scope.user_indexes[user_id].onboarding.tasks && $scope.user_indexes[user_id].onboarding.toggle && $scope.user_indexes[user_id].onboarding.notes && !$scope.user_indexes[user_id].onboarding.templates) {
                $scope.ob_active = true;
                $scope.ob.step = 5;
            }

            // TODO: Remove this tmp code
            $scope.select_campaign($scope.campaigns[0]);
        } else if (!$scope.campaigns.length && !$scope.user_indexes[user_id].onboarding.campaigns) {
            $scope.ob.step = 1;
            $scope.ob_active = true;
        }

        $scope.doing_init = false;
    }

    $scope.select_campaign = function(campaign) {
        if (campaign && campaign.id) {
            $scope.mark_notes_viewed(campaign);
            $scope.current = campaign;
            $scope.current_month = month_span(campaign.start_at, new Date().getTime() / 1000) + 1;
            $scope.current.actual_month = $scope.current_month;
            var exists = false;
            for (var i in $scope.current.blocks) {
                if ($scope.current.blocks[i].num == $scope.current_month) {
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                for (var i in $scope.current.blocks) {
                    $scope.current_month = $scope.current.blocks[i].num;
                    break;
                }
            }


        } else {
            console.log('Selected invalid campaign entry');
        }
    }

    $scope.toggle_reporting = function() {
        var body = {};
        body.do_reporting = !$scope.current.do_reporting;
        body.id = $scope.current.company_id;
        var company_promise = Company.update(body);
        company_promise.then(function(company) {
            $scope.companies[company.id] = company;
            $scope.current.do_reporting = company.do_reporting;
        });
    }

    $scope.toggle_campaign_status = function() {
        var body = {};
        body.status = ($scope.current.status == 1) ? 2 : 1;
        body.id = $scope.current.id;
        var camp_promise = Campaign.update(body);
        camp_promise.then(function(company) {
            $scope.current.status = company.status;
        });
    }

    $scope.sort_users = function() {
        $scope.users.sort(function(a, b) {
            if (a.fname.toUpperCase() < b.fname.toUpperCase()) {
                return -1;
            } else if (a.fname.toUpperCase() > b.fname.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    $scope.select_template = function() {
        if ($scope.selected_template == '' || !$scope.selected_template) {
            $scope.editing_task.title = '';
            $scope.editing_task.description = '';
        } else {
            $scope.editing_task.title = $scope.template_indexes[$scope.selected_template].title;
            $scope.editing_task.description = $scope.template_indexes[$scope.selected_template].description;
        }
    }

    $scope.select_task = function(task) {
        $scope.selected_task = task;
    }

    $scope.save_task = function(task) {
        task.due_on = parseInt(task.due_on_js.getTime() / 1000);
        if (task.id) {
            // Editing a Task
            Task.update(task).then(function(task) {
                $scope.editing_task = angular.copy($scope.new.task);
                $rootScope.$broadcast('event_task_toggle', task);
                $scope.process_tasks($scope.current);
                $('#modal_edit_task').modal('hide');
            });
        } else {
            // New Task - Add supporting task data
            task.campaign_id = $scope.current.id;
            task.company_id = $scope.current.company_id;

            Task.create(task).then(function(task) {
                $scope.editing_task = angular.copy($scope.new.task);
                //Tasks.Add(task);
                $rootScope.$broadcast('event_task_toggle', task);
                $scope.showing_modal = false;

                // Onboarding Check
                if (!$scope.user_indexes[user_id].onboarding.tasks) {
                    $scope.ob.step = 3;
                    $scope.user_indexes[user_id].onboarding.tasks = true;
                    User.update($scope.user_indexes[user_id]);
                }

                $scope.showing_modal = false;
                $('#modal_edit_task').modal('hide');
            });
        }
    }

    $scope.delete_task = function(task) {
        task.status = 0;
        var task_promise = Task.delete(task.id);
        task_promise.then(function(data) {
            Tasks.Delete(task);
            $rootScope.$broadcast('event_task_toggle', task);
            $('#modal_delete_task').modal('hide');
        });
    }

    $scope.toggle_task = function(task) {
        if (task.status != 3) {
            task.status = 3;

            // Onboarding Check
            if (!$scope.user_indexes[user_id].onboarding.toggle) {
                $scope.ob.step = 4;
                $scope.user_indexes[user_id].onboarding.toggle = true;
                User.update($scope.user_indexes[user_id]);
            }
        } else {
            task.status = 1;
        }
        var task_promise = Task.update(task);
        task_promise.then(function(data) {
            Tasks.Update(data);
            $rootScope.$broadcast('event_task_toggle', data);

            // Onboarding Check
            if (!$scope.user_indexes[user_id].onboarding.toggle) {
                $scope.ob.step = 4;
                $scope.user_indexes[user_id].onboarding.toggle = true;
                User.update($scope.user_indexes[user_id]);
            }

            $('#modal_complete_task').modal('hide');
        });


    }
    
    $scope.process_tasks = function(campaign) {
        var now_obj = new Date();
        var now = parseInt(now_obj.getTime() / 1000);
        var start = new Date(campaign.start_at * 1000);
        var cur_month = start.getMonth() + 1;
        var cur_year = start.getFullYear();
        campaign.total_completed = 0;
        campaign.has_overdue = false;
        campaign.active_month = now_obj.getFullYear() + '/' + (now_obj.getMonth() + 1);

        campaign.blocks = [];
        for (var j = 1; j <= campaign.total_months; j++) {
            var new_block = {
                num: j,
                tasks: [],
                completed: 0,
                _hidden: false,
                name: $scope.month_names[ cur_month - 1 ] + ' ' + start.getFullYear()
            };
            campaign.blocks[cur_year + '/' + cur_month] = new_block;
            campaign.blocks.push(new_block);
            if ((cur_month + 1) <= 12) {
                cur_month++;
            } else {
                cur_month = 1;
                cur_year++;
            }
        }
        
        for (var j in campaign.tasks) {
            campaign.tasks[j].is_overdue = (now >= campaign.tasks[j].due_on) ? true : false;

            if (campaign.tasks[j].is_overdue && campaign.tasks[j].status != 3) {
                campaign.has_overdue = true;
            }

            var due = new Date(campaign.tasks[j].due_on * 1000);
            if (campaign.blocks[(due.getFullYear()) + '/' + (due.getMonth() + 1)]) {
                campaign.blocks[(due.getFullYear()) + '/' + (due.getMonth() + 1)].tasks.push(campaign.tasks[j]);
                if (campaign.tasks[j].status == 3) {
                    campaign.total_completed++;
                    campaign.blocks[(due.getFullYear()) + '/' + (due.getMonth() + 1)].completed++;
                }
            } else {
                campaign.blocks[(start.getFullYear()) + '/' + (start.getMonth() + 1)].tasks.push(campaign.tasks[j]);
                if (campaign.tasks[j].status == 3) {
                    campaign.total_completed++;
                    campaign.blocks[(start.getFullYear()) + '/' + (start.getMonth() + 1)].completed++;
                }
            }
        }

        for (var i in campaign.blocks) {
            campaign.blocks[i].percent_done = campaign.blocks[i].completed / campaign.blocks[i].tasks.length * 100;
        }
    }

    $scope.edit_campaign = function(campaign) {
        $scope.new_campaign = campaign;

        $scope.new_campaign.start_at_js = new Date($scope.new_campaign.start_at * 1000);

        $scope.label_modal_edit_campaign = 'Edit Campaign';
        $scope.label_bt_edit_campaign = 'Save Campaign';
    }

    $scope.save_campaign = function() {
        var start = $scope.new_campaign.start_at_js;
        start.setDate(1);
        start.setHours(0);
        start.setMinutes(0);
        start.setSeconds(0);

        var stop = new Date($scope.new_campaign.start_at_js.getTime());
        stop.setDate(1);
        stop.setHours(0);
        stop.setMinutes(0);
        stop.setSeconds(0);
        stop.setMonth(start.getMonth() + 1);

        $scope.new_campaign.start_at = parseInt(start.getTime() / 1000);
        $scope.new_campaign.stop_at = parseInt(stop.getTime() / 1000) - 1;

        if ($scope.new_campaign.id) {
            console.log('Updating Campaign');
            var task_promise = Campaign.update($scope.new_campaign);
            task_promise.then(function(camp) {
                camp = $scope.new_campaign;
                camp.total_months = month_span(camp.start_at, camp.stop_at) + 1;
                camp.months_done = month_span(camp.start_at, new Date().getTime() / 1000);
                camp.current_month = month_span(camp.start_at, new Date().getTime() / 1000) + 1;

                // Sort Tasks into month blocks and tag overdues
                $scope.process_tasks( camp );

                $scope.showing_modal = false;
                $('#modal_edit_campaign').modal('hide');
            });
        } else {
            console.log('Creating Campaign');
            var task_promise = Campaign.create($scope.new_campaign);
            task_promise.then(function(camp) {
                camp.tasks = [];
                camp.notes = [];

                camp.total_months = month_span(camp.start_at, camp.stop_at) + 1;
                camp.months_done = month_span(camp.start_at, new Date().getTime() / 1000);
                camp.current_month = month_span(camp.start_at, new Date().getTime() / 1000) + 1;

                // Sort Tasks into month blocks and tag overdues
                $scope.process_tasks( camp );

                $scope.campaign_indexes[camp.id] = camp;
                $scope.campaigns.push( $scope.campaign_indexes[camp.id] );
                $scope.new_campaign = angular.copy($scope.new.campaign);
                $scope.select_campaign( $scope.campaign_indexes[camp.id] );

                // Onboarding Check
                if ($scope.campaigns.length >= 1 && $scope.ob.step < 2 && !$scope.user_indexes[user_id].onboarding.campaigns) {
                    $scope.ob.step = 2;
                    $scope.user_indexes[user_id].onboarding.campaigns = true;
                    User.update($scope.user_indexes[user_id]);
                }

                $scope.showing_modal = false;
                $('#modal_edit_campaign').modal('hide');
            });
        }
    }

    $scope.delete_campaign = function(campaign) {
        campaign.status = 0;
        Campaign.delete(campaign.id).then(function(data) {
            Campaigns.Delete(campaign);
            Tasks.DeleteCampaign(campaign.id);
            $rootScope.$broadcast('event_campaign_deleted', campaign);
            $scope.select_campaign( $scope.campaigns[0] );
            $('#modal_delete_campaign').modal('hide');
        });
    }

    $scope.complete_task = function(task, $event) {
        var checkbox = $event.target;

        if (task.status != 3) {
            checkbox.checked = false;
            task.status = 1;
            $scope.editing_task = task;
            $('#modal_complete_task').modal('show');
        } else {
            checkbox.checked = true;
            $scope.toggle_task(task);
        }
    }

    $scope.save_note = function(note) {
        note.campaign_id = $scope.current.id;

        CampaignNote.create(note).then(function(note) {
            //$scope.current.notes.push( note );
            $scope.note = angular.copy($scope.new.note);
            $('#modal_add_note').modal('hide');
        });
    }

    $scope.mark_notes_viewed = function(campaign) {
        var ids = [];

        for (var i in campaign.notes) {
            if (!campaign.notes[i].viewed) {
                ids.push( campaign.notes[i].id );
            }
        }

        if ($scope.notes_to_clear) {
            $scope.campaign_indexes[$scope.notes_to_clear].unviewed = false;
            for (var i in $scope.campaign_indexes[$scope.notes_to_clear].notes) {
                $scope.campaign_indexes[$scope.notes_to_clear].notes[i].viewed = true;
            }
            $scope.notes_to_clear = false;
        }

        if (ids.length) {
            CampaignNote.view(campaign.id, ids).then(function(res) {
                $scope.notes_to_clear = campaign.id;
            });
        }
    }

    $scope.filterDates = function(d) {
        var now = new Date();
        return now.getTime() <= d.getTime()
    }

    $scope.process_task_change = function(task) {
        $scope.campaign_indexes[ task.campaign_id ].has_overdue = false;
        var now = parseInt(new Date().getTime() / 1000);
        var due = new Date(task.due_on * 1000);
        var block_set = $scope.campaign_indexes[ task.campaign_id ].blocks[ (due.getFullYear()) + '/' + (due.getMonth() + 1) ];

        task.is_overdue = (now >= task.due_on) ? true : false;

        if ($scope.campaign_indexes[ task.campaign_id ]) {
            var task_exists = false;
            for (var i in $scope.campaign_indexes[ task.campaign_id ].tasks) {
                if ($scope.campaign_indexes[ task.campaign_id ].tasks[i].is_overdue && $scope.campaign_indexes[ task.campaign_id ].tasks[i].status == 1) {
                    $scope.campaign_indexes[ task.campaign_id ].has_overdue = true;
                }

                if ($scope.campaign_indexes[ task.campaign_id ].tasks[i].id == task.id) {
                    task_exists = true;

                    if (task.status == 0) {
                        // Task was deleted, remove it from primary task list
                        // as well as current block.
                        $scope.campaign_indexes[ task.campaign_id ].tasks.splice(i, 1);
                        for (var k in block_set.tasks) {
                            if (block_set.tasks[k].id == task.id) {
                                block_set.tasks.splice(k, 1);
                            }
                        }
                    } else {
                        // Update task's data
                        for (var k in task) {
                            $scope.campaign_indexes[ task.campaign_id ].tasks[i][k] = task[k];
                        }
                    }
                }
            }

            var completed = 0;

            if (!task_exists) {
                $scope.campaign_indexes[ task.campaign_id ].tasks.push(task);
                block_set.tasks.push(task);
            }

            for (var i in block_set.tasks) {
                if (block_set.tasks[i].status == 3) {
                    completed++;
                }
            }

            var diff = completed - block_set.completed;
            block_set.completed = completed;
            $scope.campaign_indexes[ task.campaign_id ].total_completed += diff;
            block_set.percent_done = block_set.completed / block_set.tasks.length * 100;
        }
    }

    $scope.onboarding_done = function() {
        if (typeof $scope.user_indexes[user_id].onboarding.campaigns == "undefined") {
            $scope.user_indexes[user_id].onboarding.campaigns = true;

            User.update($scope.user_indexes[user_id]).then(function() {
                console.log('Onboarding Done');
                console.log($scope.user_indexes[user_id].onboarding);
            });
        }
    }

    $scope.add_months = function() {
        var stop = new Date($scope.current.stop_at * 1000);
        stop.setDate(1);
        stop.setHours(0);
        stop.setMinutes(0);
        stop.setSeconds(0);
        stop.setMonth( stop.getMonth() + $scope.months_to_add + 1 );
        $scope.current.stop_at = parseInt(stop.getTime() / 1000) - 1;
        $scope.current.total_months = month_span($scope.current.start_at, $scope.current.stop_at) + 1;
        $scope.current.months_done = month_span($scope.current.start_at, new Date().getTime() / 1000);
        $scope.current.current_month = month_span($scope.current.start_at, new Date().getTime() / 1000) + 1;

        Campaign.update($scope.current).then(function(camp) {
            $scope.process_tasks( $scope.current );
            $('#modal_add_months').modal('hide');
        });
    }

    $scope.save_set = function() {
        $scope.set.tasks = [];

        for (var i in $scope.current.blocks[$scope.current_month - 1].tasks) {
            var task = {};

            for (var k in $scope.current.blocks[$scope.current_month - 1].tasks[i]) {
                task.title = $scope.current.blocks[$scope.current_month - 1].tasks[i].title;
                task.description = $scope.current.blocks[$scope.current_month - 1].tasks[i].description;
                task.assigned_to = $scope.current.blocks[$scope.current_month - 1].tasks[i].assigned_to;
            }

            $scope.set.tasks.push( task );
        }

        MonthTemplate.create($scope.set).then(function(template) {
            template.tasks = JSON.parse(template.tasks);
            console.log(template);
            $scope.month_template_indexes[template.id] = template;
            $scope.month_templates.push(template);
            $('#modal_create_set').modal('hide');
        });
    }

    $scope.load_month = function() {
        $scope.month_load_mode = 1;
        $scope.month_load_status = 0;

        var weight_per_task = 100 / $scope.month_template_indexes[$scope.selectedmt.id].tasks.length;



        for (var i in $scope.month_template_indexes[$scope.selectedmt.id].tasks) {
            var start = new Date($scope.current.start_at * 1000);
            start.setMonth( start.getMonth() + $scope.current_month - 1);
            var due_on = new Date(start.getTime());
            due_on.setDate(1);
            due_on.setHours(0);
            due_on.setMinutes(0);
            due_on.setSeconds(0);
            due_on.setMonth( start.getMonth() + 1 );
            due_on = parseInt(due_on.getTime() / 1000) - 1;

            var task = {
                title: $scope.month_template_indexes[$scope.selectedmt.id].tasks[i].title,
                description: $scope.month_template_indexes[$scope.selectedmt.id].tasks[i].description,
                assigned_to: $scope.month_template_indexes[$scope.selectedmt.id].tasks[i].assigned_to,
                campaign_id: $scope.current.id,
                company_id: $scope.current.company_id,
                due_on: due_on
            }

            Task.create(task).then(function(task) {
                $scope.month_load_status += weight_per_task;
                $scope.month_load_status = Math.ceil($scope.month_load_status);
                $rootScope.$broadcast('event_task_toggle', task);

                if ($scope.month_load_status >= 100) {
                    $('#modal_load_month').modal('hide');
                    $scope.month_load_mode = 0;
                }
            });
        }
    }

    $scope.edit_task = function(task) {
        $scope.editing_task = angular.copy(task);
        $scope.label_modal_edit_task = 'Edit Task';
        $scope.label_bt_edit_task = 'Save Task';
        $scope.editing_task.due_on_js = new Date($scope.editing_task.due_on * 1000);
    }

    $scope.new_task = function() {
        $scope.editing_task = angular.copy($scope.new.task);

        var start = new Date($scope.current.start_at * 1000);
        start.setMonth( start.getMonth() + $scope.current_month - 1);

        $scope.editing_task.due_on = new Date(start.getTime());
        $scope.editing_task.due_on.setDate(1);
        $scope.editing_task.due_on.setHours(0);
        $scope.editing_task.due_on.setMinutes(0);
        $scope.editing_task.due_on.setSeconds(0);
        $scope.editing_task.due_on.setMonth( start.getMonth() + 1 );

        $scope.editing_task.due_on = parseInt($scope.editing_task.due_on.getTime() / 1000) - 1;

        $scope.editing_task.due_on_js = new Date($scope.editing_task.due_on * 1000);

        $scope.label_modal_edit_task = 'Add Task to Timeline';
        $scope.label_bt_edit_task = 'Add New Task';
        $scope.show_modal();
    }

    $scope.show_modal = function() {
        $scope.showing_modal = true;
    }

    $scope.progress_ob = function(step) {
        $scope.ob.step = step;

        switch (step) {
            case 3:
                $scope.user_indexes[user_id].onboarding.tasks = true;
                $scope.ob.step = 3;

                User.update($scope.user_indexes[user_id]);
                break;
            case 4:
                $scope.user_indexes[user_id].onboarding.toggle = true;
                $scope.ob.step = 4;

                User.update($scope.user_indexes[user_id]);
                break;

            case 5:
                $scope.user_indexes[user_id].onboarding.notes = true;

                if ($scope.user_indexes[user_id].type > 0) {
                    $scope.user_indexes[user_id].onboarding.templates = true;
                    $scope.ob.step = 6;
                }

                User.update($scope.user_indexes[user_id]);
                break;
            case 6:
                $scope.user_indexes[user_id].onboarding.templates = true;
                User.update($scope.user_indexes[user_id]);
                break;

            case 7:
                $scope.ob_active = false;
                break;
        }


    }

    $scope.hide_task_typeahead = function() {
        setTimeout(function() {
            $scope.show_task_typeahead = false;
            $scope.$apply();
        }, 130);
    }

    $scope.choose_task_template = function(template) {
        $scope.editing_task.title = $scope.template_indexes[template.id].title;
        $scope.editing_task.description = $scope.template_indexes[template.id].description;
        $scope.show_task_typeahead = false;
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('socket_available', function(event, data) {
        Socket.emit('subscribe', { channel: 'task_activity_' + account_id });
        Socket.emit('subscribe', { channel: 'note_activity_' + account_id });
    });

    $scope.$on('task_activity_' + account_id, function(event, task) {
        task = JSON.parse(task);
        $scope.process_task_change(task);
    });

    $scope.$on('note_activity_' + account_id, function(event, note) {
        note = JSON.parse(note);
        if (note.user_id != user_id) {
            note.unviewed = true;
        }

        var exists = false;
        for (var i in $scope.campaign_indexes[note.campaign_id].notes) {
            if ($scope.campaign_indexes[note.campaign_id].notes[i].id == note.id) {
                exists = true;
                break;
            }
        }

        if (!exists) {
            $scope.campaign_indexes[note.campaign_id].notes.push( note );
        }
    });

    $scope.$on('event_task_toggle', function (event, data) {
        $scope.process_task_change( data );
    });

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    Campaigns.Ready().then(function() {
        $scope.campaigns = Campaigns.FindAll();
        $scope.campaign_indexes = Campaigns.FindAll(true);
        $scope.init_check();
    });

    Companies.Ready().then(function() {
        $scope.companies = Companies.FindAll();
        $scope.company_indexes = Companies.FindAll(true);

        if ($scope.companies.length < 1) {
            window.location = '/clients';
        }

        $scope.companies.sort(function(a, b) {
            if (a.name < b.name) {
                return -1;
            } else if (a.name > b.name) {
                return 1;
            } else {
                return 0;
            }
        });

        $scope.init_check();
    });

    Users.Ready().then(function() {
        $scope.users = Users.FindAll();
        $scope.user_indexes = Users.FindAll(true);
        $scope.sort_users();
        $scope.init_check();
    });

    TaskTemplates.Ready().then(function() {
        $scope.templates = TaskTemplates.FindAll();
        $scope.template_indexes = TaskTemplates.FindAll(true);

        $scope.templates.sort(function(a, b) {
            if (a.title.toLowerCase() < b.title.toLowerCase()) {
                return -1;
            } else if (a.title.toLowerCase() > b.title.toLowerCase()) {
                return 1;
            } else {
                return 0;
            }
        })
    });

    MonthTemplates.Ready().then(function() {
        $scope.month_templates = MonthTemplates.FindAll();
        $scope.month_template_indexes = MonthTemplates.FindAll(true);
    });

    $('#left-scroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});

});

app.config(function(ngQuickDateDefaultsProvider) {
    // Configure with icons from font-awesome
    return ngQuickDateDefaultsProvider.set({
        closeButtonHtml: "<i class='fa fa-times'></i>",
        buttonIconHtml: "<i class='fa fa-calendar'></i>",
        nextLinkHtml: "<i class='fa fa-chevron-right'></i>",
        prevLinkHtml: "<i class='fa fa-chevron-left'></i>"
    });
});
app.controller('ManageAccountsController', function ManageAccountsController($scope, $cookies, Account, User) {

    $scope.accounts = [];
    $scope.selected = '';
    $scope.target_height = 1024;

    $scope.new = {
        account: {
            billing_fname: '',
            billing_lname: '',
            billing_email: '',
            billing_company: '',
            plan_id: ''
        },
        admin: {
            fname: '',
            lname: '',
            email: '',
            password: '',
            password2: ''
        }
    }

    $scope.editing = angular.copy($scope.new.account);
    $scope.modal = angular.copy($scope.new.account);
    $scope.admin = angular.copy($scope.new.admin);

    $scope.mode = 'new';

    $scope.modal_title_edit_account = 'Add New Account';

    $scope.select_account = function(account) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_account = 'Edit Account';
        $scope.editing = account;
        $scope.form_edit_entry.$setPristine();
    }

    $scope.new_account = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_account = 'New Account';
        $scope.modal = angular.copy($scope.new.account);
        $scope.admin = angular.copy($scope.new.admin);
        $scope.form_add_entry.$setPristine();
    }

    $scope.save_account = function(account) {
        if ($scope.mode == 'new') {
            Account.create(account).then(function(account) {
                $scope.accounts.push(account);
                $scope.sort_accounts();
                $scope.select_account( account );

                $scope.admin.account_id = account.id;
                $scope.admin.type = 0;

                User.create($scope.admin).then(function(user) {
                    account.user_count = 1;
                    account.cost = 30;
                    account.created_at = new Date(account.created_at).toString();

                    $('#modal_edit_account').modal('hide');
                    $scope.form_add_entry.$setPristine();
                });
            });
        } else {
            var account_promise = Account.update(account);
            account_promise.then(function(account) {
                $('#modal_edit_account').modal('hide');
                $scope.form_edit_entry.$setPristine();
            });
        }

    }

    $scope.delete_account = function(account) {
        var account_promise = Account.delete(account.id);
        account_promise.then(function(data) {
            for (var i in $scope.accounts) {
                if ($scope.accounts[i].id == account.id) {
                    $scope.accounts.splice(i, 1);
                }
            }
            $scope.select_account( $scope.accounts[0] );
            $('#modal_delete_account').modal('hide');
        });
    }

    $scope.sort_accounts = function() {
        $scope.accounts.sort(function(a, b) {
            if (a.billing_fname.toUpperCase() < b.billing_fname.toUpperCase()) {
                return -1;
            } else if (a.billing_fname.toUpperCase() > b.billing_fname.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    var accounts_promise = Account.list();
    accounts_promise.then(function(data) {
        for (var i in data) {
            if (data[i].user_count > 2) {
                data[i].cost = 30 + ((data[i].user_count - 2) * 10);
            } else {
                data[i].cost = 30;
            }
            data[i].created_at = new Date(data[i].created_at).toString();
            $scope.accounts.push(data[i]);
        }
        $scope.sort_accounts();
        $scope.select_account( $scope.accounts[0] );
        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    });

});
app.controller('ManageCompaniesController', function ManageCompaniesController($scope, Users, User, $cookies, Companies, Company) {

    $scope.companies = [];
    $scope.selected = '';
    $scope.users = [];
    $scope.user_indexes = {};
    $scope.target_height = 1024;

    $scope.init = 0;
    $scope.ob_active = 0;
    $scope.showing_modal = false;
    $scope.ob = {
        step: 0
    }

    $scope.new = {
        company: {
            id: '',
            account_id: '',
            name: '',
            website: '',
            contact: '',
            email: '',
            phone: '',
            do_reporting: '',
            status: ''
        }
    }

    $scope.editing = angular.copy($scope.new.company);
    $scope.creating = angular.copy($scope.new.company);
    $scope.mode = 'new';
    $scope.modal_title_edit_company = 'Add New Company';

    $scope.select_company = function(company) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_company = 'Edit Company';
        $scope.editing = company;
    }

    $scope.new_company = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_company = 'New Company';
        $scope.creating = angular.copy($scope.new.company);
        $scope.showing_modal = true;
    }

    $scope.save_company = function(company) {
        if ($scope.mode == 'new') {
            company.do_reporting = true;
            company.phone = company.phone.replace(/-/g, '');
            company.phone = company.phone.replace(/\(/g, '');
            company.phone = company.phone.replace(/\)/g, '');
            company.phone = company.phone.replace(/ /g, '');
            var company_promise = Company.create(company);
            company_promise.then(function(company) {
                $scope.companies.push(company);
                $scope.sort_companies();
                $scope.select_company( company );
                $('#modal_edit_company').modal('hide');
            });
        } else {
            var company_promise = Company.update(company);
            company_promise.then(function(company) {
                $('#modal_edit_company').modal('hide');
            });
        }

    }

    $scope.delete_company = function(company) {
        var company_promise = Company.delete(company.id);
        company_promise.then(function(data) {
            for (var i in $scope.companies) {
                if ($scope.companies[i].id == company.id) {
                    $scope.companies.splice(i, 1);
                }
            }
            $scope.select_company($scope.companies[0]);
            $('#modal_delete_company').modal('hide');
        });
    }

    $scope.sort_companies = function() {
        $scope.companies.sort(function(a, b) {
            if (a.name.toUpperCase() < b.name.toUpperCase()) {
                return -1;
            } else if (a.name.toUpperCase() > b.name.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    $scope.onboarding_done = function() {
        if (typeof $scope.user_indexes[user_id].onboarding.companies == "undefined") {
            $scope.user_indexes[user_id].onboarding.companies = true;

            User.update($scope.user_indexes[user_id]).then(function() {
                console.log('Onboarding Done');
                console.log($scope.user_indexes[user_id].onboarding);
            });
        }
    }

    $scope.progress_ob = function(step) {
        $scope.ob.step = step;

        switch (step) {
            case 2:
                $scope.user_indexes[user_id].onboarding.clients = true;
                $scope.ob.step = 2;

                User.update($scope.user_indexes[user_id]);
                break;

            case 3:
                window.location = '/campaigns';
                $scope.ob_active = false;
                break;
        }


    }

    $scope.init_check = function() {
        $scope.init++;

        if ($scope.init > 1) {
            if (!$scope.user_indexes[user_id].onboarding.clients && !$scope.companies.length) {
                $scope.ob.step = 1;
                $scope.ob_active = true;
            } else {
                $scope.progress_ob(2);
            }
            $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        }
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    Users.Ready().then(function() {
        $scope.users = Users.FindAll();
        $scope.user_indexes = Users.FindAll(true);

        $scope.init_check();
    });

    Companies.Ready().then(function() {
        $scope.companies = Companies.FindAll();
        $scope.sort_companies();
        $scope.select_company( $scope.companies[0] );

        var preselect = window.location.hash.split('/')[1];
        if (preselect) {
            for (var i in $scope.companies) {
                if ($scope.companies[i].id == preselect) {
                    $scope.select_company( $scope.companies[i] );
                    break;
                }
            }
        }

        $scope.init_check();

        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    });


});
app.controller('ManageUsersController', function ManageUsersController($scope, $cookies, User, Users) {

    $scope.users = [];
    $scope.selected = '';
    $scope.user_indexes = {};
    $scope.target_height = 1024;

    $scope.new = {
        user: {
            fname: '',
            lname: '',
            email: '',
            type: ''
        }
    }

    $scope.editing = angular.copy($scope.new.user);
    $scope.creating = angular.copy($scope.new.user);

    $scope.mode = 'new';
    $scope.modal_title_edit_user = 'Add New User';

    $scope.select_user = function(user) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_user = 'Edit User';
        $scope.editing = angular.copy(user);
        $scope.form_edit_entry.$setPristine();
    }

    $scope.new_user = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_user = 'New User';
        $scope.creating = angular.copy($scope.new.user);
        $scope.form_add_entry.$setPristine();
    }

    $scope.save_user = function(user) {
        if ($scope.mode == 'new') {
            var user_promise = User.create(user);
            user_promise.then(function(user) {
                $scope.users.push(user);
                $scope.sort_users();
                $scope.select_user(user);
                $('#modal_edit_user').modal('hide');
            });
        } else {
            var user_promise = User.update(user);
            user_promise.then(function(user) {
                for (var i in $scope.users) {
                    if (user.id == $scope.users[i].id) {
                        $scope.users[i] = user;
                        return;
                    }
                }
                $('#modal_edit_user').modal('hide');
            });
        }

    }

    $scope.delete_user = function(user) {
        var user_promise = User.delete(user.id);
        user_promise.then(function(data) {
            for (var i in $scope.users) {
                if ($scope.users[i].id == user.id) {
                    $scope.users.splice(i, 1);
                }
            }
            $scope.select_user($scope.users[0]);
            $('#modal_delete_user').modal('hide');
        });
    }

    $scope.sort_users = function() {
        $scope.users.sort(function(a, b) {
            if (a.fname.toUpperCase() < b.fname.toUpperCase()) {
                return -1;
            } else if (a.fname.toUpperCase() > b.fname.toUpperCase()) {
                return 2;
            } else {
                return 0;
            }
        });
    }

    $scope.onboarding_done = function() {
        if (typeof $scope.user_indexes[user_id].onboarding.users == "undefined") {
            $scope.user_indexes[user_id].onboarding.users = true;

            User.update($scope.user_indexes[user_id]).then(function() {
                console.log('Onboarding Done');
                console.log($scope.user_indexes[user_id].onboarding);
            });
        }
    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    Users.Ready().then(function(users) {
        $scope.users = Users.FindAll();
        $scope.user_indexes = Users.FindAll(true);

        $scope.select_user($scope.users[0]);

        if (!$scope.user_indexes[user_id].onboarding.users) {
            //$scope.do_onboarding = true;
        }

        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    })

});
app.controller('NavController', function NavController($scope) {

});
app.controller('TasksController', function TasksController($scope, $cookies, TaskTemplate) {

    $scope.tasks = [];
    $scope.current = false;
    $scope.mode = 'new';
    $scope.modal_title_edit_task = 'Add New Task';
    $scope.target_height = 1024;

    $scope.new = {
        task: {
            title: '',
            description: ''
        }
    }

    $scope.editing = angular.copy($scope.new.task);

    $scope.select_task = function(task) {
        $scope.current = task;
        $scope.mode = 'editing';
        $scope.modal_title_edit_task = 'Edit Task';
        $scope.editing = angular.copy(task);
    }

    $scope.new_task = function() {
        $scope.mode = 'new';
        $scope.modal_title_edit_task = 'Add New Task';
        $scope.editing = angular.copy($scope.new.task);
        $scope.form_edit_task.$setPristine();
    }

    $scope.edit_task = function(task) {
        $scope.mode = 'editing';
        $scope.modal_title_edit_task = 'Edit Task';
        $scope.editing = angular.copy(task);
        $scope.form_edit_task.$setPristine();
    }

    $scope.delete_task = function(task) {
        var task_promise = TaskTemplate.delete(task.id);
        task_promise.then(function(data) {
            for (var i in $scope.tasks) {
                if ($scope.tasks[i].id == task.id) {
                    $scope.tasks.splice(i, 1);
                    $scope.current = $scope.tasks[0];
                }
            }
            $('#modal_delete_task').modal('hide');
        });
    }

    /**
     * Saves or Creates a new TaskTemplate
     */
    $scope.save_task = function(task) {
        if ($scope.mode == 'new') {
            var task_promise = TaskTemplate.create(task);
            task_promise.then(function(task) {
                $scope.tasks.push(task);
                $scope.select_task(task);
                $('#modal_edit_task').modal('hide');
            });
        } else {
            var task_promise = TaskTemplate.update(task);
            task_promise.then(function(task) {
                $('#modal_edit_task').modal('hide');
            });
        }

    }

    // -------------------------
    //   Socket & Scope Events
    // -------------------------

    $scope.$on('event_height_target_change', function(event, theight) {
        $scope.target_height = theight - $('#left-cont > .el-head').height();
    });

    // ---------------
    //  Init
    // ---------------

    var tasks_promise = TaskTemplate.list();
    tasks_promise.then(function(data) {
        for (var i in data) {
            $scope.tasks.push(data[i]);
        }
        $scope.select_task($scope.tasks[0]);
        $('#task-left').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        $('#cscroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
    });

});
app.controller('TopNavController', function TopNavController($scope, $rootScope,  Tasks) {

    $scope.assigned = 0;
    $scope.notification_width = 283;
    $scope.notifications_on = false;
    $scope.tasks = [];

    // --
    // Functions
    // --

    $scope.show_notifications = function(show) {
        $scope.notification_width = $('#member-notifications').width() - 40;
        if (show) {
            $scope.notifications_on = true;
        } else {
            $scope.notifications_on = false;
        }
    }

    // --
    // Init
    // --

    Tasks.Ready().then(function() {
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $rootScope.$on('event_task_toggle', function (event, data) {
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $rootScope.$on('event_campaign_deleted', function (event, data) {
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $rootScope.$on('task_activity_' + account_id, function(event, task) {
        task = JSON.parse(task);
        Tasks.Update(task);
        $scope.tasks = Tasks.FetchAssigned(user_id, true);
        $scope.assigned = $scope.tasks.length;
    });

    $('#notifications-container').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});

});
app.directive('wdActivity', function () {
    return {
        restrict: 'E',
        scope: {
            campaign: '='
        },

        templateUrl: '/js/views/activity.html',

        link: function (scope, element, attrs) {

        },
        controller: function($scope, Tasks, Users) {
            $scope.users_ready = false;
            $scope.tasks_ready = false;

            $scope.activity = [];
            $scope.activity_indexes = [];
            $scope.user_indexes = {};

            $scope.build_activity = function() {
                $scope.activity = [];
                $scope.activity_indexes = [];

                Tasks.FindAll().forEach(function(task, index) {
                    if (task.status == 3 && task.campaign_id == $scope.campaign) {
                        $scope.activity_indexes[task.id] = task;
                        $scope.activity.push( task );
                    }
                });
            }

            $scope.init_check = function() {
                if ($scope.tasks_ready && $scope.users_ready && $scope.campaign) {
                    $scope.build_activity();
                }
            }

            $scope.process_task = function(task) {
                if ($scope.activity_indexes[task.id]) {
                    if (task.status == 3) {
                        // If the task exists and is completed, update it's properties
                        for (var i in task) {
                            $scope.activity_indexes[task.id][i] = task[i];
                        }
                    } else {
                        // Task exists, but isn't completed, remove it
                        for (var i in $scope.activity) {
                            if ($scope.activity[i].id == task.id) {
                                $scope.activity.splice(i, 1);
                                delete $scope.activity_indexes[task.id];
                            }
                        }
                    }
                } else {
                    // Task wasn't here already, add it if it's completed
                    if (task.status == 3) {
                        $scope.activity_indexes[task.id] = task;
                        $scope.activity.push( $scope.activity_indexes[task.id] );
                    }
                }
            }

            // --
            // Init
            // --

            var tasks_ready = Tasks.Ready();
            tasks_ready.then(function() {
                $scope.tasks_ready = true;
                $scope.init_check();
            });

            var users_ready = Users.Ready();
            users_ready.then(function() {
                $scope.users_ready = true;
                $scope.user_indexes = Users.FindAll(true);
                $scope.init_check();
            });

            $scope.$on('event_task_toggle', function (event, data) {
                $scope.process_task( data );
            });

            $scope.$on('task_activity_' + account_id, function(event, task) {
                $scope.process_task( JSON.parse(task) );
            });

            $scope.$watch('campaign', function() {
                $scope.init_check();
            });
        }
    };
});
app.directive('fullheight', function ($rootScope) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            $rootScope.$on('event_height_target_change', function(event, theight) {
                element[0].style.height = theight + 'px';
            });
        }
    };
});

app.directive('heighttarget', function ($rootScope) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            scope.$watch(function() {
                var viewport_height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
                var topnav_height = $('#topnav').height();

                $rootScope.$broadcast('event_height_target_change', viewport_height - topnav_height);
            });
        }
    };
})
app.directive('gravatar', function () {
    return {
        restrict: 'A',
        scope: '@',
        link: function (scope, element, attrs) {
            var size = scope.$eval(attrs.gravatarSize);
            attrs.$observe('gravatarSrc', function(value) {
                element[0].src = 'https://www.gravatar.com/avatar/' + md5(value) + '?d=mm&s=' + size;
            })
        }
    };
});
app.directive('prefixHttp', function () {
    return {
        restrict: 'A',
        scope: '@',
        require: 'ngModel',
        link: function (scope, element, attrs, controller) {
            function ensurePrefix(value) {
                if (!value) { value = '' }
                if (value.length > 0 && value.indexOf('http://') < 0 && value.indexOf('https://') < 0) {
                    controller.$setViewValue('http://' + value);
                    controller.$render();
                    return 'http://' + value;
                } else {
                    return value;
                }
            }
            controller.$formatters.push(ensurePrefix);
            controller.$parsers.push(ensurePrefix);
        }
    };
});
app.directive('task-typeahead', function () {
    return {
        restrict: 'A',
        scope: '@',
        link: function (scope, element, attrs) {

        }
    };
});
app.directive('wdTimeline', function () {
    return {
        restrict: 'E',
        scope: {
            campaign: '=',
            cmonth: '='
        },

        templateUrl: '/js/views/timeline.html',

        link: function (scope, element, attrs) {

        },
        controller: function($scope, Tasks, Users) {
            $scope.offset = 0;

            $scope.change_month = function(month) {
                $scope.cmonth = month;
                console.log('Month changed', month);
            }

            $scope.set_offset = function(plus) {
                if (plus && $scope.offset < ($scope.campaign.blocks.length - 1)) {
                    $scope.offset++;
                } else if (!plus && $scope.offset > 0) {
                    $scope.offset--;
                }
                for (var i in $scope.campaign.blocks) {
                    $scope.campaign.blocks[i]._hidden = ($scope.campaign.blocks[i].num > $scope.offset) ? false : true;
                }
            }
        }
    };
});
app.directive('wdWorkwall', function () {
    return {
        restrict: 'E',
        scope: {},
        transclude: true,

        templateUrl: '/js/views/workwall.html' ,

        link: function (scope, element, attrs) {

        },
        controller: function($scope, Tasks, Users) {
            $scope.users_ready = false;
            $scope.tasks_ready = false;

            $scope.entries = [];
            $scope.entry_indexes = [];
            $scope.users = [];

            $scope.target_height = 1024;

            $scope.build_workwall = function() {
                $scope.entries = [];
                $scope.entry_indexes = [];

                Tasks.FindAll().forEach(function(task, index) {
                    if (task.status == 3) {
                        $scope.entry_indexes[task.id] = task;
                        $scope.entries.push( task );
                    }
                });
            }

            $scope.init_check = function() {
                if ($scope.tasks_ready && $scope.users_ready) {
                    $scope.build_workwall();
                }
            }

            $scope.process_task = function(task) {
                if ($scope.entry_indexes[task.id]) {
                    if (task.status == 3) {
                        // If the task exists and is completed, update it's properties
                        for (var i in task) {
                            $scope.entry_indexes[task.id][i] = task[i];
                        }
                    } else {
                        // Task exists, but isn't completed, remove it
                        for (var i in $scope.entries) {
                            if ($scope.entries[i].id == task.id) {
                                $scope.entries.splice(i, 1);
                                delete $scope.entry_indexes[task.id];
                            }
                        }
                    }
                } else {
                    // Task wasn't here already, add it if it's completed
                    if (task.status == 3) {
                        $scope.entry_indexes[task.id] = task;
                        $scope.entries.push( $scope.entry_indexes[task.id] );
                    }
                }
            }
            
            // --
            // Init
            // --

            var tasks_ready = Tasks.Ready();
            tasks_ready.then(function() {
                $scope.tasks_ready = true;
                $scope.init_check();
            });

            var users_ready = Users.Ready();
            users_ready.then(function() {
                $scope.users_ready = true;
                $scope.users = Users.FindAll(true);
                $scope.init_check();
            });

            $scope.$on('event_task_toggle', function (event, data) {
                $scope.process_task( data );
            });

            $scope.$on('task_activity_' + account_id, function(event, task) {
                $scope.process_task( JSON.parse(task) );
            });

            $scope.$on('event_height_target_change', function(event, theight) {
                $scope.target_height = theight - $('#left-cont > .el-head').height();
            });

            $('#right-scroll').perfectScrollbar({suppressScrollX: true, minScrollbarLength:100, wheelSpeed: 100});
        }
    };
});
app.factory('Account', function($http) {

    var Account = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Account
     *
     * @param   id      int     Account Identifier
     */
    Account.get = function(id) {
        var url = api_host + '/account/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Fetch a List of the Account's Accounts
     */
    Account.list = function() {
        var url = api_host + '/accounts';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Create a Account
     *
     * @param   account      obj     Account Obj
     */
    Account.create = function(account) {
        var url = api_host + '/account';
        return $http.post(url + '?token=' + token, account).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Update a Account
     *
     * @param   account      obj     Account Obj
     */
    Account.update = function(account) {
        var url = api_host + '/account/' + account.id;
        return $http.put(url + '?token=' + token, account).then(function(response) {
            return new Account(response.data);
        });
    };

    /**
     * Delete a Account
     *
     * @param   id      int     Account Identifier
     */
    Account.delete = function(id) {
        var url = api_host + '/account/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Account(response.data);
        });
    };

    return Account;
});
app.factory('Billing', function($http) {

    var Billing = function(data) {
        angular.extend(this, data);
    };

    /**
     * Create a Billing Agreement
     *
     * @param   billing      obj     Billing Obj
     * @param   cb           func    Callback Function
     */
    Billing.create = function(billing, cb) {
        var url = api_host + '/billing';
        return $http.post(url + '?token=' + token, billing)
            .success(function(data, status, headers, config) {
                cb(null, data);
            })
            .error(function(data, status, headers, config) {
                cb(data, status);
            });
    };

    /**
     * Create a Billing Agreement
     *
     * @param   billing      obj     Billing Obj
     * @param   cb           func    Callback Function
     */
    Billing.update = function(billing, cb) {
        var url = api_host + '/billing';
        return $http.put(url + '?token=' + token, billing)
            .success(function(data, status, headers, config) {
                cb(null, data);
            })
            .error(function(data, status, headers, config) {
                cb(data, status);
            });
    };

    /**
     * Delete a Billing Agreement
     *
     * @param   cb      func     Callback Function
     */
    Billing.delete = function(cb) {
        var url = api_host + '/billing';
        return $http.delete(url + '?token=' + token)
            .success(function(data, status, headers, config) {
                cb(null, data);
            })
            .error(function(data, status, headers, config) {
                cb(data, status);
            });
    };

    return Billing;
});
app.factory('Campaign', function($http) {

    var Campaign = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Campaign
     *
     * @param   id      int     Campaign Identifier
     */
    Campaign.get = function(id) {
        var url = api_host + '/campaign/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Fetch a List of the Campaign's Campaigns
     */
    Campaign.list = function() {
        var url = api_host + '/campaigns';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Create a Campaign
     *
     * @param   campaign      obj     Campaign Obj
     */
    Campaign.create = function(campaign) {
        var url = api_host + '/campaign';
        return $http.post(url + '?token=' + token, campaign).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Update a Campaign
     *
     * @param   campaign      obj     Campaign Obj
     */
    Campaign.update = function(campaign) {
        var url = api_host + '/campaign/' + campaign.id;
        return $http.put(url + '?token=' + token, campaign).then(function(response) {
            return new Campaign(response.data);
        });
    };

    /**
     * Delete a Campaign
     *
     * @param   id      int     Campaign Identifier
     */
    Campaign.delete = function(id) {
        var url = api_host + '/campaign/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Campaign(response.data);
        });
    };

    return Campaign;
});
app.factory('CampaignNote', function($http) {

    var CampaignNote = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific CampaignNote
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.get = function(campaign_id, id) {
        var url = api_host + '/campaign/' + campaign_id + '/note/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Fetch a List of the CampaignNote's CampaignNotes
     *
     * @param   campaign_id     string     Owning Campaign id
     */
    CampaignNote.list = function(campaign_id) {
        var url = api_host + '/campaign/' + campaign_id + '/notes';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Create a CampaignNote
     *
     * @param   note      obj     CampaignNote obj
     */
    CampaignNote.create = function(note) {
        var url = api_host + '/campaign/' + note.campaign_id + '/note';
        return $http.post(url + '?token=' + token, note).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Update a CampaignNote
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.update = function(campaign_id, id) {
        var url = api_host + '/campaign/' + campaign_id + '/note/' + id;
        return $http.put(url + '?token=' + token, campaign).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Delete a CampaignNote
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.delete = function(campaign_id, id) {
        var url = api_host + '/campaign/' + campaign_id + '/note/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    /**
     * Mark a set of CampaignNotes as viewed.
     *
     * @param   campaign_id     string     Owning Campaign id
     * @param   id              string     CampaignNote id
     */
    CampaignNote.view = function(campaign_id, ids) {
        var url = api_host + '/campaign/' + campaign_id + '/notes/view';
        return $http.put(url + '?token=' + token, { note_ids: ids }).then(function(response) {
            return new CampaignNote(response.data);
        });
    };

    return CampaignNote;
});
app.factory('Company', function($http) {

    var Company = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Company
     *
     * @param   id      int     Company Identifier
     */
    Company.get = function(id) {
        var url = api_host + '/company/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Fetch a List of the Company's Companys
     */
    Company.list = function() {
        var url = api_host + '/companies';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Create a Company
     *
     * @param   company      obj     Company Obj
     */
    Company.create = function(company) {
        var url = api_host + '/company';
        return $http.post(url + '?token=' + token, company).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Update a Company
     *
     * @param   company      obj     Company Obj
     */
    Company.update = function(company) {
        var url = api_host + '/company/' + company.id;
        return $http.put(url + '?token=' + token, company).then(function(response) {
            return new Company(response.data);
        });
    };

    /**
     * Delete a Company
     *
     * @param   id      int     Company Identifier
     */
    Company.delete = function(id) {
        var url = api_host + '/company/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Company(response.data);
        });
    };

    return Company;
});
app.factory('MonthTemplate', function($http) {

    var MonthTemplate = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific MonthTemplate
     *
     * @param   id      int     MonthTemplate Identifier
     */
    MonthTemplate.get = function(id) {
        var url = api_host + '/template/set/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Fetch a List of the MonthTemplate's MonthTemplates
     */
    MonthTemplate.list = function() {
        var url = api_host + '/template/sets';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Create a MonthTemplate
     *
     * @param   task      obj     MonthTemplate Obj
     */
    MonthTemplate.create = function(task) {
        var url = api_host + '/template/set';
        return $http.post(url + '?token=' + token, task).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Update a MonthTemplate
     *
     * @param   task      obj     MonthTemplate Obj
     */
    MonthTemplate.update = function(task) {
        var url = api_host + '/template/set/' + task.id;
        return $http.put(url + '?token=' + token, task).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    /**
     * Delete a MonthTemplate
     *
     * @param   id      int     MonthTemplate Identifier
     */
    MonthTemplate.delete = function(id) {
        var url = api_host + '/template/set/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new MonthTemplate(response.data);
        });
    };

    return MonthTemplate;
});
app.factory('Task', function($http) {

    var Task = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific Task
     *
     * @param   id      int     Task Identifier
     */
    Task.get = function(id) {
        var url = api_host + '/task/' + id;
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Fetch a List of the Task's Tasks
     */
    Task.list = function() {
        var url = api_host + '/tasks';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Fetch a List of the Latest Activity
     */
    Task.activity = function() {
        var url = api_host + '/tasks/activity';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Fetch a List of the Latest Activity
     */
    Task.assigned = function() {
        var url = api_host + '/tasks/assigned';
        return $http.get(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Create a Task
     *
     * @param   task      obj     Task Obj
     */
    Task.create = function(task) {
        var url = api_host + '/task';
        return $http.post(url + '?token=' + token, task).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Update a Task
     *
     * @param   task      obj     Task Obj
     */
    Task.update = function(task) {
        var url = api_host + '/task/' + task.id;
        return $http.put(url + '?token=' + token, task).then(function(response) {
            return new Task(response.data);
        });
    };

    /**
     * Delete a Task
     *
     * @param   id      int     Task Identifier
     */
    Task.delete = function(id) {
        var url = api_host + '/task/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new Task(response.data);
        });
    };

    return Task;
});
app.factory('TaskTemplate', function($http) {

    var TaskTemplate = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific TaskTemplate
     *
     * @param   id      int     TaskTemplate Identifier
     */
    TaskTemplate.get = function(id) {
        var url = api_host + '/template/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Fetch a List of the TaskTemplate's TaskTemplates
     */
    TaskTemplate.list = function() {
        var url = api_host + '/templates';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Create a TaskTemplate
     *
     * @param   task      obj     TaskTemplate Obj
     */
    TaskTemplate.create = function(task) {
        var url = api_host + '/template';
        return $http.post(url + '?token=' + token, task).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Update a TaskTemplate
     *
     * @param   task      obj     TaskTemplate Obj
     */
    TaskTemplate.update = function(task) {
        var url = api_host + '/template/' + task.id;
        return $http.put(url + '?token=' + token, task).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    /**
     * Delete a TaskTemplate
     *
     * @param   id      int     TaskTemplate Identifier
     */
    TaskTemplate.delete = function(id) {
        var url = api_host + '/template/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new TaskTemplate(response.data);
        });
    };

    return TaskTemplate;
});
app.factory('User', function($http) {

    var User = function(data) {
        angular.extend(this, data);
    };

    /**
     * Fetch a specific User
     *
     * @param   id      int     User Identifier
     */
    User.get = function(id) {
        var url = api_host + '/user/' + id;
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Fetch a List of the User's Users
     */
    User.list = function() {
        var url = api_host + '/users';
        return $http.get(url + '?token=' + token ).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Create a User
     *
     * @param   user      obj     User Obj
     */
    User.create = function(user) {
        var url = api_host + '/user';
        return $http.post(url + '?token=' + token, user).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Update a User
     *
     * @param   user      obj     User Obj
     */
    User.update = function(user) {
        var url = api_host + '/user/' + user.id;
        return $http.put(url + '?token=' + token, user).then(function(response) {
            return new User(response.data);
        });
    };

    /**
     * Delete a User
     *
     * @param   id      int     User Identifier
     */
    User.delete = function(id) {
        var url = api_host + '/user/' + id;
        return $http.delete(url + '?token=' + token).then(function(response) {
            return new User(response.data);
        });
    };

    return User;
});
angular.module('WorkadoApp.Accounts', [])
    .value('version', '1.0')
    .service('Accounts', function ($rootScope, $q, Account) {

        // --
        // Properties
        // --

        var accounts = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in accounts) {
                index[ accounts[i].id ] = accounts[i];
            }
        }

        // --
        // Init
        // --

        console.log('Accounts Service initializing...');

        var accounts_promise = Account.list()
        accounts_promise.then(function(rows) {
            for (var i in rows) {
                accounts.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return accounts;
                }
            },

            /**
             * Returns access to a single account
             *
             * @param   id     UUID        The desired account id
             */
            Get: function(id) {
                return index[id];
            }
            
        }
    });
angular.module('WorkadoApp.Campaigns', [])
    .value('version', '1.0')
    .service('Campaigns', function ($rootScope, $q, Campaign) {

        // --
        // Properties
        // --

        var campaigns = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in campaigns) {
                index[ campaigns[i].id ] = campaigns[i];
            }
        }

        // --
        // Init
        // --

        console.log('Campaigns Service initializing...');

        var campaigns_promise = Campaign.list()
        campaigns_promise.then(function(rows) {
            for (var i in rows) {
                campaigns.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return campaigns;
                }
            },

            /**
             * Add a Task
             */
            Delete: function(camp) {
                for (var i in campaigns) {
                    if (campaigns[i].id == camp.id) {
                        campaigns.splice(i, 1);
                    }
                }
                delete index[camp.id];
            }

        }
    });
angular.module('WorkadoApp.Companies', [])
    .value('version', '1.0')
    .service('Companies', function ($rootScope, $q, Company) {

        // --
        // Properties
        // --

        var companies = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in companies) {
                index[ companies[i].id ] = companies[i];
            }
        }

        // --
        // Init
        // --

        console.log('Companies Service initializing...');

        var companies_promise = Company.list()
        companies_promise.then(function(rows) {
            for (var i in rows) {
                companies.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the companies list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return companies;
                }
            }

        }
    });
angular.module('WorkadoApp.MonthTemplates', [])
    .value('version', '1.0')
    .service('MonthTemplates', function ($rootScope, $q, MonthTemplate) {

        // --
        // Properties
        // --

        var month_templates = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in month_templates) {
                index[ month_templates[i].id ] = month_templates[i];
            }
        }

        // --
        // Init
        // --

        console.log('MonthTemplates Service initializing...');

        var month_templates_promise = MonthTemplate.list()
        month_templates_promise.then(function(rows) {
            for (var i in rows) {
                month_templates.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return month_templates;
                }
            }

        }
    });
angular.module('WorkadoApp.Socket', [])
    .value('version', '1.0')
    .service('Socket', function ($rootScope) {
        if (typeof token != "undefined" && token != '') {
            var socket = io.connect(socket_host, { query: "auth_param=" + token });

            return {

                on:function (eventName, callback) {
                    socket.on(eventName, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            callback.apply(socket, args);
                        });
                    });
                },

                emit:function (eventName, data, callback) {
                    socket.emit(eventName, data, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    })
                }
            };
        } else {
            return false;
        }
    });

angular.module('WorkadoApp.TaskTemplates', [])
    .value('version', '1.0')
    .service('TaskTemplates', function ($rootScope, $q, TaskTemplate) {

        // --
        // Properties
        // --

        var task_templates = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in task_templates) {
                index[ task_templates[i].id ] = task_templates[i];
            }
        }

        // --
        // Init
        // --

        console.log('TaskTemplates Service initializing...');

        var task_templates_promise = TaskTemplate.list()
        task_templates_promise.then(function(rows) {
            for (var i in rows) {
                task_templates.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the campaigns list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return task_templates;
                }
            }

        }
    });
angular.module('WorkadoApp.Tasks', [])
    .value('version', '1.0')
    .service('Tasks', function ($rootScope, $q, Task) {

        // --
        // Properties
        // --

        var tasks = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in tasks) {
                index[ tasks[i].id ] = tasks[i];
            }
        }

        // --
        // Init
        // --

        console.log('Tasks Service initializing...');

        var tasks_promise = Task.list()
        tasks_promise.then(function(rows) {
            for (var i in rows) {
                tasks.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the tasks list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return tasks;
                }
            },

            /**
             * Returns a count of tasks assigned to a user
             *
             * @param   user_id             string      The desired user id
             * @param   incomplete_only     bool        Return only incomplete tasks?
             */
            CountAssigned: function(user_id, incomplete_only) {
                var count = 0;
                for (var i in tasks) {
                    if (incomplete_only) {
                        if (tasks[i].assigned_to == user_id && tasks[i].status < 3 && tasks[i].status > 0) {
                            count++;
                        }
                    } else {
                        if (tasks[i].assigned_to == user_id && tasks[i].status > 0) {
                            count++;
                        }
                    }
                }

                return count;
            },

            /**
             * Returns a count of tasks assigned to a user
             *
             * @param   user_id             string      The desired user id
             * @param   incomplete_only     bool        Return only incomplete tasks?
             */
            FetchAssigned: function(user_id, incomplete_only) {
                var res = [];
                for (var i in tasks) {
                    if (incomplete_only) {
                        if (tasks[i].assigned_to == user_id && tasks[i].status < 3 && tasks[i].status > 0) {
                            res.push( tasks[i] );
                        }
                    } else {
                        if (tasks[i].assigned_to == user_id && tasks[i].status > 0) {
                            res.push( tasks[i] );
                        }
                    }
                }

                return res;
            },

            /**
             * Update a Task
             */
            Update: function(task) {
                if (index[task.id]) {
                    index[task.id] = task;
                    for (var i in tasks) {
                        if (tasks[i].id == task.id) {
                            tasks[i] = task;
                        }
                    }
                } else {
                    index[task.id] = task;
                    tasks.push( index[task.id] );
                }
            },

            /**
             * Add a Task
             */
            Add: function(task) {
                index[task.id] = task;
                tasks.push(index[task.id]);
            },

            /**
             * Add a Task
             */
            Delete: function(task) {
                for (var i in tasks) {
                    if (tasks[i].id == task.id) {
                        tasks.splice(i, 1);
                    }
                }
                delete index[task.id];
            },

            /**
             * Delete Tasks related to a specific Campaign
             */
            DeleteCampaign: function(campaign_id) {
                for (var i in tasks) {
                    if (tasks[i].campaign_id == campaign_id) {
                        delete index[tasks[i].id]
                        tasks.splice(i, 1);
                    }
                }
            }
        }
    });
angular.module('WorkadoApp.Users', [])
    .value('version', '1.0')
    .service('Users', function ($rootScope, $q, User) {

        // --
        // Properties
        // --

        var users = [];
        var index = {};

        var available = false;

        // --
        // Private Service Methods
        // --

        var refresh_index = function() {
            index = {};
            for (var i in users) {
                users[i].onboarding = JSON.parse(users[i].onboarding || '{}') || {};
                index[ users[i].id ] = users[i];
            }
        }

        // --
        // Init
        // --

        console.log('Users Service initializing...');

        var users_promise = User.list()
        users_promise.then(function(rows) {
            for (var i in rows) {
                users.push(rows[i]);
            }
            refresh_index();
            available = true;
        });

        return {

            // --
            // Public Service Methods
            // --

            Ready: function() {
                var deferred = $q.defer();

                var timer = setInterval(function() {
                    if (available) {
                        deferred.resolve(true);
                        clearTimeout(timer);
                    }
                }, 50);

                return deferred.promise;
            },

            /**
             * Returns access to the users list
             *
             * @param   indexed     bool        Return the indexed version?
             */
            FindAll: function(indexed) {
                if (indexed) {
                    return index;
                } else {
                    return users;
                }
            },

            Get: function(user_id) {
                return index[user_id];
            }
        }
    });

/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */

(function($) {

var types = ['DOMMouseScroll', 'mousewheel'];

if ($.event.fixHooks) {
    for ( var i=types.length; i; ) {
        $.event.fixHooks[ types[--i] ] = $.event.mouseHooks;
    }
}

$.event.special.mousewheel = {
    setup: function() {
        if ( this.addEventListener ) {
            for ( var i=types.length; i; ) {
                this.addEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = handler;
        }
    },
    
    teardown: function() {
        if ( this.removeEventListener ) {
            for ( var i=types.length; i; ) {
                this.removeEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = null;
        }
    }
};

$.fn.extend({
    mousewheel: function(fn) {
        return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
    },
    
    unmousewheel: function(fn) {
        return this.unbind("mousewheel", fn);
    }
});


function handler(event) {
    var orgEvent = event || window.event, args = [].slice.call( arguments, 1 ), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
    event = $.event.fix(orgEvent);
    event.type = "mousewheel";
    
    // Old school scrollwheel delta
    if ( orgEvent.wheelDelta ) { delta = orgEvent.wheelDelta/120; }
    if ( orgEvent.detail     ) { delta = -orgEvent.detail/3; }
    
    // New school multidimensional scroll (touchpads) deltas
    deltaY = delta;
    
    // Gecko
    if ( orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
        deltaY = 0;
        deltaX = -1*delta;
    }
    
    // Webkit
    if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY/120; }
    if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = -1*orgEvent.wheelDeltaX/120; }
    
    // Add event and delta to the front of the arguments
    args.unshift(event, delta, deltaX, deltaY);
    
    return ($.event.dispatch || $.event.handle).apply(this, args);
}

})(jQuery);

/* Copyright (c) 2012 HyeonJe Jun (http://github.com/noraesae)
 * Licensed under the MIT License
 */
'use strict';
(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else {
    // Browser globals
    factory(jQuery);
  }
}(function ($) {

  // The default settings for the plugin
  var defaultSettings = {
    wheelSpeed: 10,
    wheelPropagation: false,
    minScrollbarLength: null,
    useBothWheelAxes: false,
    useKeyboard: true,
    suppressScrollX: false,
    suppressScrollY: false,
    scrollXMarginOffset: 0,
    scrollYMarginOffset: 0
  };

  var getEventClassName = (function () {
    var incrementingId = 0;
    return function () {
      var id = incrementingId;
      incrementingId += 1;
      return '.perfect-scrollbar-' + id;
    };
  }());

  $.fn.perfectScrollbar = function (suppliedSettings, option) {

    return this.each(function () {
      // Use the default settings
      var settings = $.extend(true, {}, defaultSettings),
          $this = $(this);

      if (typeof suppliedSettings === "object") {
        // But over-ride any supplied
        $.extend(true, settings, suppliedSettings);
      } else {
        // If no settings were supplied, then the first param must be the option
        option = suppliedSettings;
      }

      // Catch options

      if (option === 'update') {
        if ($this.data('perfect-scrollbar-update')) {
          $this.data('perfect-scrollbar-update')();
        }
        return $this;
      }
      else if (option === 'destroy') {
        if ($this.data('perfect-scrollbar-destroy')) {
          $this.data('perfect-scrollbar-destroy')();
        }
        return $this;
      }

      if ($this.data('perfect-scrollbar')) {
        // if there's already perfect-scrollbar
        return $this.data('perfect-scrollbar');
      }


      // Or generate new perfectScrollbar

      // Set class to the container
      $this.addClass('ps-container');

      var $scrollbarXRail = $("<div class='ps-scrollbar-x-rail'></div>").appendTo($this),
          $scrollbarYRail = $("<div class='ps-scrollbar-y-rail'></div>").appendTo($this),
          $scrollbarX = $("<div class='ps-scrollbar-x'></div>").appendTo($scrollbarXRail),
          $scrollbarY = $("<div class='ps-scrollbar-y'></div>").appendTo($scrollbarYRail),
          scrollbarXActive,
          scrollbarYActive,
          containerWidth,
          containerHeight,
          contentWidth,
          contentHeight,
          scrollbarXWidth,
          scrollbarXLeft,
          scrollbarXBottom = parseInt($scrollbarXRail.css('bottom'), 10),
          scrollbarYHeight,
          scrollbarYTop,
          scrollbarYRight = parseInt($scrollbarYRail.css('right'), 10),
          eventClassName = getEventClassName();

      var updateContentScrollTop = function (currentTop, deltaY) {
        var newTop = currentTop + deltaY,
            maxTop = containerHeight - scrollbarYHeight;

        if (newTop < 0) {
          scrollbarYTop = 0;
        }
        else if (newTop > maxTop) {
          scrollbarYTop = maxTop;
        }
        else {
          scrollbarYTop = newTop;
        }

        var scrollTop = parseInt(scrollbarYTop * (contentHeight - containerHeight) / (containerHeight - scrollbarYHeight), 10);
        $this.scrollTop(scrollTop);
        $scrollbarXRail.css({bottom: scrollbarXBottom - scrollTop});
      };

      var updateContentScrollLeft = function (currentLeft, deltaX) {
        var newLeft = currentLeft + deltaX,
            maxLeft = containerWidth - scrollbarXWidth;

        if (newLeft < 0) {
          scrollbarXLeft = 0;
        }
        else if (newLeft > maxLeft) {
          scrollbarXLeft = maxLeft;
        }
        else {
          scrollbarXLeft = newLeft;
        }

        var scrollLeft = parseInt(scrollbarXLeft * (contentWidth - containerWidth) / (containerWidth - scrollbarXWidth), 10);
        $this.scrollLeft(scrollLeft);
        $scrollbarYRail.css({right: scrollbarYRight - scrollLeft});
      };

      var getSettingsAdjustedThumbSize = function (thumbSize) {
        if (settings.minScrollbarLength) {
          thumbSize = Math.max(thumbSize, settings.minScrollbarLength);
        }
        return thumbSize;
      };

      var updateScrollbarCss = function () {
        $scrollbarXRail.css({left: $this.scrollLeft(), bottom: scrollbarXBottom - $this.scrollTop(), width: containerWidth, display: scrollbarXActive ? "inherit": "none"});
        $scrollbarYRail.css({top: $this.scrollTop(), right: scrollbarYRight - $this.scrollLeft(), height: containerHeight, display: scrollbarYActive ? "inherit": "none"});
        $scrollbarX.css({left: scrollbarXLeft, width: scrollbarXWidth});
        $scrollbarY.css({top: scrollbarYTop, height: scrollbarYHeight});
      };

      var updateBarSizeAndPosition = function () {
        containerWidth = $this.width();
        containerHeight = $this.height();
        contentWidth = $this.prop('scrollWidth');
        contentHeight = $this.prop('scrollHeight');

        if (!settings.suppressScrollX && containerWidth + settings.scrollXMarginOffset < contentWidth) {
          scrollbarXActive = true;
          scrollbarXWidth = getSettingsAdjustedThumbSize(parseInt(containerWidth * containerWidth / contentWidth, 10));
          scrollbarXLeft = parseInt($this.scrollLeft() * (containerWidth - scrollbarXWidth) / (contentWidth - containerWidth), 10);
        }
        else {
          scrollbarXActive = false;
          scrollbarXWidth = 0;
          scrollbarXLeft = 0;
          $this.scrollLeft(0);
        }

        if (!settings.suppressScrollY && containerHeight + settings.scrollYMarginOffset < contentHeight) {
          scrollbarYActive = true;
          scrollbarYHeight = getSettingsAdjustedThumbSize(parseInt(containerHeight * containerHeight / contentHeight, 10));
          scrollbarYTop = parseInt($this.scrollTop() * (containerHeight - scrollbarYHeight) / (contentHeight - containerHeight), 10);
        }
        else {
          scrollbarYActive = false;
          scrollbarYHeight = 0;
          scrollbarYTop = 0;
          $this.scrollTop(0);
        }

        if (scrollbarYTop >= containerHeight - scrollbarYHeight) {
          scrollbarYTop = containerHeight - scrollbarYHeight;
        }
        if (scrollbarXLeft >= containerWidth - scrollbarXWidth) {
          scrollbarXLeft = containerWidth - scrollbarXWidth;
        }

        updateScrollbarCss();
      };

      var bindMouseScrollXHandler = function () {
        var currentLeft,
            currentPageX;

        $scrollbarX.bind('mousedown' + eventClassName, function (e) {
          currentPageX = e.pageX;
          currentLeft = $scrollbarX.position().left;
          $scrollbarXRail.addClass('in-scrolling');
          e.stopPropagation();
          e.preventDefault();
        });

        $(document).bind('mousemove' + eventClassName, function (e) {
          if ($scrollbarXRail.hasClass('in-scrolling')) {
            updateContentScrollLeft(currentLeft, e.pageX - currentPageX);
            e.stopPropagation();
            e.preventDefault();
          }
        });

        $(document).bind('mouseup' + eventClassName, function (e) {
          if ($scrollbarXRail.hasClass('in-scrolling')) {
            $scrollbarXRail.removeClass('in-scrolling');
          }
        });

        currentLeft =
        currentPageX = null;
      };

      var bindMouseScrollYHandler = function () {
        var currentTop,
            currentPageY;

        $scrollbarY.bind('mousedown' + eventClassName, function (e) {
          currentPageY = e.pageY;
          currentTop = $scrollbarY.position().top;
          $scrollbarYRail.addClass('in-scrolling');
          e.stopPropagation();
          e.preventDefault();
        });

        $(document).bind('mousemove' + eventClassName, function (e) {
          if ($scrollbarYRail.hasClass('in-scrolling')) {
            updateContentScrollTop(currentTop, e.pageY - currentPageY);
            e.stopPropagation();
            e.preventDefault();
          }
        });

        $(document).bind('mouseup' + eventClassName, function (e) {
          if ($scrollbarYRail.hasClass('in-scrolling')) {
            $scrollbarYRail.removeClass('in-scrolling');
          }
        });

        currentTop =
        currentPageY = null;
      };

      // check if the default scrolling should be prevented.
      var shouldPreventDefault = function (deltaX, deltaY) {
        var scrollTop = $this.scrollTop();
        if (deltaX === 0) {
          if (!scrollbarYActive) {
            return false;
          }
          if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= contentHeight - containerHeight && deltaY < 0)) {
            return !settings.wheelPropagation;
          }
        }

        var scrollLeft = $this.scrollLeft();
        if (deltaY === 0) {
          if (!scrollbarXActive) {
            return false;
          }
          if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= contentWidth - containerWidth && deltaX > 0)) {
            return !settings.wheelPropagation;
          }
        }
        return true;
      };

      // bind handlers
      var bindMouseWheelHandler = function () {
        var shouldPrevent = false;
        $this.bind('mousewheel' + eventClassName, function (e, delta, deltaX, deltaY) {
          if (!settings.useBothWheelAxes) {
            // deltaX will only be used for horizontal scrolling and deltaY will
            // only be used for vertical scrolling - this is the default
            $this.scrollTop($this.scrollTop() - (deltaY * settings.wheelSpeed));
            $this.scrollLeft($this.scrollLeft() + (deltaX * settings.wheelSpeed));
          } else if (scrollbarYActive && !scrollbarXActive) {
            // only vertical scrollbar is active and useBothWheelAxes option is
            // active, so let's scroll vertical bar using both mouse wheel axes
            if (deltaY) {
              $this.scrollTop($this.scrollTop() - (deltaY * settings.wheelSpeed));
            } else {
              $this.scrollTop($this.scrollTop() + (deltaX * settings.wheelSpeed));
            }
          } else if (scrollbarXActive && !scrollbarYActive) {
            // useBothWheelAxes and only horizontal bar is active, so use both
            // wheel axes for horizontal bar
            if (deltaX) {
              $this.scrollLeft($this.scrollLeft() + (deltaX * settings.wheelSpeed));
            } else {
              $this.scrollLeft($this.scrollLeft() - (deltaY * settings.wheelSpeed));
            }
          }

          // update bar position
          updateBarSizeAndPosition();

          shouldPrevent = shouldPreventDefault(deltaX, deltaY);
          if (shouldPrevent) {
            e.preventDefault();
          }
        });

        // fix Firefox scroll problem
        $this.bind('MozMousePixelScroll' + eventClassName, function (e) {
          if (shouldPrevent) {
            e.preventDefault();
          }
        });
      };

      var bindKeyboardHandler = function () {
        var hovered = false;
        $this.bind('mouseenter' + eventClassName, function (e) {
          hovered = true;
        });
        $this.bind('mouseleave' + eventClassName, function (e) {
          hovered = false;
        });

        var shouldPrevent = false;
        $(document).bind('keydown' + eventClassName, function (e) {
          if (!hovered) {
            return;
          }

          var deltaX = 0,
              deltaY = 0;

          switch (e.which) {
          case 37: // left
            deltaX = -3;
            break;
          case 38: // up
            deltaY = 3;
            break;
          case 39: // right
            deltaX = 3;
            break;
          case 40: // down
            deltaY = -3;
            break;
          case 33: // page up
            deltaY = 9;
            break;
          case 32: // space bar
          case 34: // page down
            deltaY = -9;
            break;
          case 35: // end
            deltaY = -containerHeight;
            break;
          case 36: // home
            deltaY = containerHeight;
            break;
          default:
            return;
          }

          $this.scrollTop($this.scrollTop() - (deltaY * settings.wheelSpeed));
          $this.scrollLeft($this.scrollLeft() + (deltaX * settings.wheelSpeed));

          shouldPrevent = shouldPreventDefault(deltaX, deltaY);
          if (shouldPrevent) {
            e.preventDefault();
          }
        });
      };

      var bindRailClickHandler = function () {
        var stopPropagation = function (e) { e.stopPropagation(); };

        $scrollbarY.bind('click' + eventClassName, stopPropagation);
        $scrollbarYRail.bind('click' + eventClassName, function (e) {
          var halfOfScrollbarLength = parseInt(scrollbarYHeight / 2, 10),
              positionTop = e.pageY - $scrollbarYRail.offset().top - halfOfScrollbarLength,
              maxPositionTop = containerHeight - scrollbarYHeight,
              positionRatio = positionTop / maxPositionTop;

          if (positionRatio < 0) {
            positionRatio = 0;
          } else if (positionRatio > 1) {
            positionRatio = 1;
          }

          $this.scrollTop((contentHeight - containerHeight) * positionRatio);
        });

        $scrollbarX.bind('click' + eventClassName, stopPropagation);
        $scrollbarXRail.bind('click' + eventClassName, function (e) {
          var halfOfScrollbarLength = parseInt(scrollbarXWidth / 2, 10),
              positionLeft = e.pageX - $scrollbarXRail.offset().left - halfOfScrollbarLength,
              maxPositionLeft = containerWidth - scrollbarXWidth,
              positionRatio = positionLeft / maxPositionLeft;

          if (positionRatio < 0) {
            positionRatio = 0;
          } else if (positionRatio > 1) {
            positionRatio = 1;
          }

          $this.scrollLeft((contentWidth - containerWidth) * positionRatio);
        });
      };

      // bind mobile touch handler
      var bindMobileTouchHandler = function () {
        var applyTouchMove = function (differenceX, differenceY) {
          $this.scrollTop($this.scrollTop() - differenceY);
          $this.scrollLeft($this.scrollLeft() - differenceX);

          // update bar position
          updateBarSizeAndPosition();
        };

        var startCoords = {},
            startTime = 0,
            speed = {},
            breakingProcess = null,
            inGlobalTouch = false;

        $(window).bind("touchstart" + eventClassName, function (e) {
          inGlobalTouch = true;
        });
        $(window).bind("touchend" + eventClassName, function (e) {
          inGlobalTouch = false;
        });

        $this.bind("touchstart" + eventClassName, function (e) {
          var touch = e.originalEvent.targetTouches[0];

          startCoords.pageX = touch.pageX;
          startCoords.pageY = touch.pageY;

          startTime = (new Date()).getTime();

          if (breakingProcess !== null) {
            clearInterval(breakingProcess);
          }

          e.stopPropagation();
        });
        $this.bind("touchmove" + eventClassName, function (e) {
          if (!inGlobalTouch && e.originalEvent.targetTouches.length === 1) {
            var touch = e.originalEvent.targetTouches[0];

            var currentCoords = {};
            currentCoords.pageX = touch.pageX;
            currentCoords.pageY = touch.pageY;

            var differenceX = currentCoords.pageX - startCoords.pageX,
              differenceY = currentCoords.pageY - startCoords.pageY;

            applyTouchMove(differenceX, differenceY);
            startCoords = currentCoords;

            var currentTime = (new Date()).getTime();
            speed.x = differenceX / (currentTime - startTime);
            speed.y = differenceY / (currentTime - startTime);
            startTime = currentTime;

            e.preventDefault();
          }
        });
        $this.bind("touchend" + eventClassName, function (e) {
          clearInterval(breakingProcess);
          breakingProcess = setInterval(function () {
            if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
              clearInterval(breakingProcess);
              return;
            }

            applyTouchMove(speed.x * 30, speed.y * 30);

            speed.x *= 0.8;
            speed.y *= 0.8;
          }, 10);
        });
      };

      var bindScrollHandler = function () {
        $this.bind('scroll' + eventClassName, function (e) {
          updateBarSizeAndPosition();
        });
      };

      var destroy = function () {
        $this.unbind(eventClassName);
        $(window).unbind(eventClassName);
        $(document).unbind(eventClassName);
        $this.data('perfect-scrollbar', null);
        $this.data('perfect-scrollbar-update', null);
        $this.data('perfect-scrollbar-destroy', null);
        $scrollbarX.remove();
        $scrollbarY.remove();
        $scrollbarXRail.remove();
        $scrollbarYRail.remove();

        // clean all variables
        $scrollbarX =
        $scrollbarY =
        containerWidth =
        containerHeight =
        contentWidth =
        contentHeight =
        scrollbarXWidth =
        scrollbarXLeft =
        scrollbarXBottom =
        scrollbarYHeight =
        scrollbarYTop =
        scrollbarYRight = null;
      };

      var ieSupport = function (version) {
        $this.addClass('ie').addClass('ie' + version);

        var bindHoverHandlers = function () {
          var mouseenter = function () {
            $(this).addClass('hover');
          };
          var mouseleave = function () {
            $(this).removeClass('hover');
          };
          $this.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarXRail.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarYRail.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarX.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarY.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
        };

        var fixIe6ScrollbarPosition = function () {
          updateScrollbarCss = function () {
            $scrollbarX.css({left: scrollbarXLeft + $this.scrollLeft(), bottom: scrollbarXBottom, width: scrollbarXWidth});
            $scrollbarY.css({top: scrollbarYTop + $this.scrollTop(), right: scrollbarYRight, height: scrollbarYHeight});
            $scrollbarX.hide().show();
            $scrollbarY.hide().show();
          };
        };

        if (version === 6) {
          bindHoverHandlers();
          fixIe6ScrollbarPosition();
        }
      };

      var supportsTouch = (('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch);

      var initialize = function () {
        var ieMatch = navigator.userAgent.toLowerCase().match(/(msie) ([\w.]+)/);
        if (ieMatch && ieMatch[1] === 'msie') {
          // must be executed at first, because 'ieSupport' may addClass to the container
          ieSupport(parseInt(ieMatch[2], 10));
        }

        updateBarSizeAndPosition();
        bindScrollHandler();
        bindMouseScrollXHandler();
        bindMouseScrollYHandler();
        bindRailClickHandler();
        if (supportsTouch) {
          bindMobileTouchHandler();
        }
        if ($this.mousewheel) {
          bindMouseWheelHandler();
        }
        if (settings.useKeyboard) {
          bindKeyboardHandler();
        }
        $this.data('perfect-scrollbar', $this);
        $this.data('perfect-scrollbar-update', updateBarSizeAndPosition);
        $this.data('perfect-scrollbar-destroy', destroy);
      };

      // initialize
      initialize();

      return $this;
    });
  };
}));
