// Utils
var Config      = require('../config.json');
var Util        = require('util');
var Helpers     = require('../utils/util').Util;
var UUID        = require('node-uuid');
var Tokens      = require('../utils/tokens').Tokens;
var should      = require('should');
var Validator   = require('validator');
var Request     = require('request');

// Models
var Models      = require('../models');
var User        = Models.User;
var Account     = Models.Account;

// Fixtures
var Fixtures    = require('../test_helpers/test_setup');

// Alias Fix
var test = it;

// --
// Globals
// --

var Globals = {}

var TestData = {
    TestUser: {
        id: UUID.v4().toString(),
        fname: 'Some',
        lname: 'Guy',
        email: 'sfurnival+workado_test_userA@gmail.com',
        password: 'testpass',
        type: User.Type.ADMIN,
        status: 1
    }
}

var IsReady = {
    TestUser: false
}

// --
// Test User object
// --

describe('Users :', function() {

    // --
    // Suite Setup
    // --

    before(function(done) {
        Fixtures.DoSetup(function(g, key) {
            Globals.Key = key;
            Globals.Super = g.Super;
            Globals.Account = g.Account;
            TestData.TestUser.account_id = g.Account.id;
            done();
        });
    });

    // --
    // Start Pre Testing
    // --

    describe('Pre Tests', function() {

        test('> Valid test Account should exist.', function() {
            Globals.Account.should.be.ok;
        });

        test('> Valid super User should exist.', function() {
            Globals.Super.should.be.ok;
        });

        test('> Valid API Key should exist.', function() {
            Globals.Key.should.be.ok;
            Globals.Key.should.be.lengthOf('36');
            Validator.isUUID(Globals.Key, 4).should.be.ok;
        });

    });

    // --
    // Start Model Testing
    // --

    describe('Model Tests', function() {

        test('> Model exists', function() {
            Models.User.should.be.ok;
            User.should.be.ok;
        });

    });

    // --
    // Start API Testing
    // --

    describe('API Tests', function() {

        before(function(done) {
            User.create(TestData.TestUser)
                .success(function(user) {
                    user.should.be.ok;
                    Globals.TestUser = user;
                    done();
                })
                .error(function(error) {
                    console.log(error);
                    throw new Error(error);
                    done();
                })
        });

        // --
        // Listing: GET /users
        // --

        test('> GET /users', function(done) {
            Request.get(Config.api_host + '/users?token=' + Globals.Key, function(error, response, body) {
                if (error) {
                    throw new Error(error);
                    return done();
                }

                // Verify Response
                response.should.have.status(200);
                body = JSON.parse(body); // Throws on bad JSON
                (body.length >= 1).should.be.ok;

                done();
            })
        });

        test('> GET /users (Bad Token)', function(done) {
            Request.get(Config.api_host + '/users?token=' + UUID.v4().toString(), function(error, response, body) {
                if (error) {
                    throw new Error(error);
                    return done();
                }

                // Verify Response
                response.should.have.status(401);
                body = JSON.parse(body); // Throws on bad JSON
                body.should.have.property('message');

                done();
            })
        });

        // --
        // Retrieving: GET /user/:user_id
        // --

        test('> GET /user/:user_id', function(done) {
            Request.get(Config.api_host + '/user/' + TestData.TestUser.id + '?token=' + Globals.Key, function(error, response, body) {
                if (error) {
                    throw new Error(error);
                    return done();
                }

                // Verify Response
                response.should.have.status(200);
                body = JSON.parse(body); // Throws on bad JSON
                body.should.have.property('id', TestData.TestUser.id);
                body.should.have.property('fname', TestData.TestUser.fname);
                body.should.have.property('lname', TestData.TestUser.lname);
                body.should.have.property('email', TestData.TestUser.email);
                body.should.have.property('account_id', Globals.Account.id);
                body.should.have.property('status', User.Status.ACTIVE);
                body.should.have.property('type', TestData.TestUser.type);
                body.should.not.have.property('password');

                done();
            })
        });

        test('> GET /user/:user_id (Bad UUID)', function(done) {
            Request.get(Config.api_host + '/user/asdasdasdasd?token=' + Globals.Key, function(error, response, body) {
                if (error) {
                    throw new Error(error);
                    return done();
                }

                // Verify Response
                response.should.have.status(400);
                body = JSON.parse(body); // Throws on bad JSON
                body.should.have.property('message');

                done();
            })
        });

        test('> GET /user/:user_id (Non-Existing)', function(done) {
            Request.get(Config.api_host + '/user/' + UUID.v4().toString() + '?token=' + Globals.Key, function(error, response, body) {
                if (error) {
                    throw new Error(error);
                    return done();
                }

                // Verify Response
                response.should.have.status(404);
                body = JSON.parse(body); // Throws on bad JSON
                body.should.have.property('message');

                done();
            })
        });

        test('> GET /user/:user_id (Bad Token)', function(done) {
            Request.get(Config.api_host + '/user/' + TestData.TestUser.id + '?token=' + UUID.v4().toString(), function(error, response, body) {
                if (error) {
                    throw new Error(error);
                    return done();
                }

                // Verify Response
                response.should.have.status(401);
                body = JSON.parse(body); // Throws on bad JSON
                body.should.have.property('message');

                done();
            })
        });

        // --
        // Updating: PUT /user/:user_id
        // --

        test('> PUT /user/:user_id', function(done) {
            Request({ method: 'PUT',
                uri: Config.api_host + '/user/' + TestData.TestUser.id,
                json: {
                    token: Globals.Key
                    // TODO: Add some changed values
                }
            }, function (error, response, body) {
                if (error) {
                    throw new Error(error);
                    return done();
                }

                // Verify Response
                response.should.have.status(200);
                body.should.have.property('id', TestData.TestUser.id);
                body.should.have.property('fname', TestData.TestUser.fname);
                body.should.have.property('lname', TestData.TestUser.lname);
                body.should.have.property('email', TestData.TestUser.email);
                body.should.have.property('account_id', Globals.Account.id);
                body.should.have.property('status', User.Status.ACTIVE);
                body.should.have.property('type', TestData.TestUser.type);
                body.should.not.have.property('password');

                done();
            });
        });

        // --
        // Deleting: DELETE /user/:user_id
        // --

        test('> DELETE /user/:user_id', function(done) {
            // TODO: Test deletion
            done();
        })

        after(function(done) {
            Globals.TestUser.destroy()
                .success(function() {
                    delete Globals.TestUser;
                    done();
                })
                .error(function(error) {
                    console.log(error);
                    throw new Error(error);
                    done();
                })
        });

    })

    // --
    // Suite Teardown
    // --

    after(function(done) {
        Fixtures.DoTeardown(function() {
            done();
        });
    });

});