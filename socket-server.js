// ----------------
//   Dependancies
// ----------------

var UUID    = require('node-uuid');

var Config  = require('./config.json');

var io      = require('socket.io').listen(Config.socket_server_port);
var Redis   = require("redis");

// Redis Clients
var Publisher   = Redis.createClient(Config.redis_port, Config.redis_host);

// Models
var Tokens = require('./utils/tokens').Tokens;

// Data
var LiveUsers = 0;

// -----------------
//   Configure IO
// -----------------

io.configure(function () {
    io.set('heartbeat timeout', 60);
    io.set('log level', 3);  // 3 - Debug / 0 - Error
    io.set('authorization', function (handshakeData, callback) {
        if (handshakeData.query && handshakeData.query.auth_param && Config.protocol == handshakeData.headers['x-forwarded-proto']) {
            Tokens.Authenticate(handshakeData.query.auth_param, function(token) {
                //console.log('Authenticated: ', token);
                callback(null, token.is_valid);
            });
        } else {
            // error first callback style
            callback('Not Authorized', false);
        }
    });
});

// -----------------
//   Server Events
// -----------------
io.sockets.on('connection', function(socket) {

    LiveUsers++;
    var Subscriber  = Redis.createClient(Config.redis_port, Config.redis_host);

    socket.emit('log', "Welcome to Workado Socket Server");

    // -----------------
    //   Socket Events
    // -----------------

    socket.on('subscribe', function(message) {
        if (message && message.channel) {
            Subscriber.subscribe(message.channel);
            socket.emit('subscription_ready', message);
            socket.emit('log', "Registered for: [" + message.channel + "]");
        } else {
            socket.emit('log', "Failed registering for: [" + message.channel + "]");
        }
    });

    socket.on('disconnect', function() {
        console.log('...disconnect');
        LiveUsers--;
        Subscriber.end();
        console.log('Current Users: ', LiveUsers);
    });

    // -----------------------------
    //   Redis Subscription Events
    // -----------------------------

    Subscriber.on('message', function(channel, event) {
        console.log('Redis channel message ['+channel+']: ' + event.toString() );
        socket.emit('subscription_event', {
            channel: channel,
            event: event
        });
    });

    Subscriber.on('subscribe', function(channel, count) {
        console.log('Subscribed to Channel [' + channel + ']:' + count);
    });

    Subscriber.on('unsubscribe', function(channel, count) {
        console.log('Unsubscribed from Channel [' + channel + ']:' + count);
    });

    console.log('Current Users: ', LiveUsers);
});

io.sockets.on('error', function (reason){
    console.error(reason);
});
