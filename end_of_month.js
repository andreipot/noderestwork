// ----------------
//   Dependancies
// ----------------

var Util    = require('util');
var Config  = require('./config.json');

var Redis   = require("redis");
var Kue     = require('kue');

Kue.redis.createClient = function() {
    var client = Redis.createClient(Config.redis_port, Config.redis_host);
    return client;
};

var Jobs = Kue.createQueue();

var job = Jobs.create('end_of_month', {
    title: 'System: End of Month Tasks',
    event_completed: 'event_end_of_month_completed',
    event_progress: 'event_end_of_month_progress',
    event_failed: 'event_end_of_month_failed'
})
    .priority('high')
    .save();

job.on('complete', function() {
    Util.log('--- End of Month completed.');
    process.exit(0);
}).on('failed', function() {
    Util.log('--- End of Month failed.');
    process.exit(0);
});