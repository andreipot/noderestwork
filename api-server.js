// ----------------
//   Dependencies
// ----------------
var Restify             = require('restify');
var Cluster             = require('cluster');
var Util                = require('util');

// Config
var Config              = require('./config.json');

// --------------------------
//   Server Settings & Init
// --------------------------

var Settings = {
    cpu_cores:      require('os').cpus().length,
    port:           Config.api_server_port
}

// ------------------------
//   Start Server
// ------------------------

if (Cluster.isMaster && Config.use_cluster) {
    // -----------------
    // --
    //  Cluster Master
    // --
    // -----------------

    Util.log('Workado API Master Started ...');

    // Start as many workers as we have CPU cores
    for (var i = 1; i <= Settings.cpu_cores; i++) {
        Cluster.fork();
    }

    Cluster.on('disconnect', function(worker) {
        console.error('Worker Disconnect!');
        Cluster.fork();
    });

} else {
    // -----------------
    // --
    //  Workers
    // --
    // -----------------

    Util.log('Workado API Worker Started ...');

    // -----------------------
    //   Worker Dependencies
    // -----------------------

    // Utilities
    var Time                = require('time');
    var Request             = require('./utils/request').Request;
    var Client              = require('./utils/client').Client;
    var Redis               = require('redis');
    var Tokens              = require('./utils/tokens').Tokens;

    // Database
    var RedisStore          = Redis.createClient(Config.redis_port, Config.redis_port, {});

    // Controllers
    var UsersController         = require('./controllers/users_controller').UsersController;
    var TasksController         = require('./controllers/tasks_controller').TasksController;
    var CampaignsController     = require('./controllers/campaigns_controller').CampaignsController;
    var CompaniesController     = require('./controllers/companies_controller').CompaniesController;
    var TaskTemplatesController = require('./controllers/templates_controller').TaskTemplatesController;
    var MonthTemplatesController= require('./controllers/month_templates_controller').MonthTemplatesController;
    var AccountsController      = require('./controllers/accounts_controller').AccountsController;
    var BillingController       = require('./controllers/billing_controller').BillingController;
    var CampaignNotesController = require('./controllers/campaign_notes_controller').CampaignNotesController;

    // -----------------
    //   Create Server
    // -----------------
    var server = Restify.createServer({
        name                  : 'Workado-API',
        version               : '1.0.0',
        accept                : ['application/json'],
        formatters            : {
            'application/json': function(req, res, body) {
                return JSON.stringify(body);
            },
            'application/javascript': function(req, res, body) {
                return body;
            }
        },
        responseTimeHeader    : 'X-Runtime',
        responseTimeFormatter : function(durationInMilliseconds) {
            return durationInMilliseconds / 1000;
        }
    });

    // Auto parse query parameters into req.params
    server.use(Restify.queryParser());

    // Auto parse any json from the body into parameters
    // -- We also reject any content type that is not application/json
    server.use(Restify.bodyParser({ rejectUnknown: false }));

    // Enable compressed responses
    server.use(Restify.gzipResponse());

    // Check for a token
    server.use(function(req, res, next){
        if (req.headers['x-forwarded-proto'] != Config.protocol) {
            res.header('Location', 'https://' + req.headers.host + req.url);
            res.send(302);
            return next(false);
        }

        if (req.params.token) {
            Tokens.Authenticate(req.params.token, function(token) {
                req.token = token;
                if (req.token.is_valid) {
                    return next();
                } else {
                    return Client.NotAuthorized(req, res);
                }
            });
        } else if (!req.params.token && req.params.stripe_id) {
            if (req.params.stripe_id == Config.stripe.identifier) {
                return next();
            } else {
                return Client.NotAuthorized(req, res);
            }
        } else {
            return Client.NotAuthorized(req, res);
        }
    });

    // This runs prior to route functions
    server.pre(function(req, res, next) {
        // Query params for GET are not available at this point.  Parse them manually.
        var params = Request.GetParams(req.href());

        // Allow method overrides (for stuff like ajax that can't do PUT/DELETE etc)
        req.method = (params._method) ? params._method.toUpperCase() : req.method;
        return next();
    });

    // ----------------
    //   API Routing
    // ----------------

    /** Account Profile Data **/
    server.get({ path: '/accounts',                         version: '1.0.0' },       AccountsController.List);
    server.get({ path: '/account/:account_id',              version: '1.0.0' },       AccountsController.Get);
    server.put({ path: '/account/:account_id',              version: '1.0.0' },       AccountsController.Update);
    server.del({ path: '/account/:account_id',              version: '1.0.0' },       AccountsController.Delete);
    server.post({ path: '/account',                         version: '1.0.0' },       AccountsController.Create);

    /** User Profile Data **/
    server.get({ path: '/users',                            version: '1.0.0' },       UsersController.List);
    server.get({ path: '/user/:user_id',                    version: '1.0.0' },       UsersController.Get);
    server.put({ path: '/user/:user_id',                    version: '1.0.0' },       UsersController.Update);
    server.del({ path: '/user/:user_id',                    version: '1.0.0' },       UsersController.Delete);
    server.post({ path: '/user',                            version: '1.0.0' },       UsersController.Create);

    /** Campaign Data **/
    server.put({ path: '/campaign/:campaign_id/notes/view',    version: '1.0.0' },    CampaignNotesController.View);
    server.get({ path: '/campaign/:campaign_id/notes',         version: '1.0.0' },    CampaignNotesController.List);
    server.get({ path: '/campaign/:campaign_id/note/:note_id', version: '1.0.0' },    CampaignNotesController.Get);
    server.post({ path: '/campaign/:campaign_id/note',         version: '1.0.0' },    CampaignNotesController.Create);
    server.del({ path: '/campaign/:campaign_id/note/:note_id', version: '1.0.0' },    CampaignNotesController.Delete);
    server.put({ path: '/campaign/:campaign_id/note/:note_id', version: '1.0.0' },    CampaignNotesController.Update);

    server.get({ path: '/campaigns',                        version: '1.0.0' },       CampaignsController.List);
    server.get({ path: '/campaign/:campaign_id',            version: '1.0.0' },       CampaignsController.Get);
    server.put({ path: '/campaign/:campaign_id',            version: '1.0.0' },       CampaignsController.Update);
    server.del({ path: '/campaign/:campaign_id',            version: '1.0.0' },       CampaignsController.Delete);
    server.post({ path: '/campaign',                        version: '1.0.0' },       CampaignsController.Create);

    /** Company Data **/
    server.get({ path: '/companies',                        version: '1.0.0' },       CompaniesController.List);
    server.get({ path: '/company/:company_id',              version: '1.0.0' },       CompaniesController.Get);
    server.put({ path: '/company/:company_id',              version: '1.0.0' },       CompaniesController.Update);
    server.del({ path: '/company/:company_id',              version: '1.0.0' },       CompaniesController.Delete);
    server.post({ path: '/company',                         version: '1.0.0' },       CompaniesController.Create);

    /** Task Data **/
    server.get({ path: '/tasks',                            version: '1.0.0' },       TasksController.List);
    server.get({ path: '/tasks/activity',                   version: '1.0.0' },       TasksController.Activity);
    server.get({ path: '/tasks/assigned',                   version: '1.0.0' },       TasksController.Assigned);
    server.get({ path: '/task/:task_id',                    version: '1.0.0' },       TasksController.Get);
    server.put({ path: '/task/:task_id',                    version: '1.0.0' },       TasksController.Update);
    server.del({ path: '/task/:task_id',                    version: '1.0.0' },       TasksController.Delete);
    server.post({ path: '/task',                            version: '1.0.0' },       TasksController.Create);

    /** Month Template Data **/
    server.get({ path: '/template/sets',                    version: '1.0.0' },       MonthTemplatesController.List);
    server.get({ path: '/template/set/:set_id',             version: '1.0.0' },       MonthTemplatesController.Get);
    server.put({ path: '/template/set/:set_id',             version: '1.0.0' },       MonthTemplatesController.Update);
    server.del({ path: '/template/set/:set_id',             version: '1.0.0' },       MonthTemplatesController.Delete);
    server.post({ path: '/template/set',                    version: '1.0.0' },       MonthTemplatesController.Create);

    /** Task Template Data **/
    server.get({ path: '/templates',                        version: '1.0.0' },       TaskTemplatesController.List);
    server.get({ path: '/template/:template_id',            version: '1.0.0' },       TaskTemplatesController.Get);
    server.put({ path: '/template/:template_id',            version: '1.0.0' },       TaskTemplatesController.Update);
    server.del({ path: '/template/:template_id',            version: '1.0.0' },       TaskTemplatesController.Delete);
    server.post({ path: '/template',                        version: '1.0.0' },       TaskTemplatesController.Create);

    /** Billing **/
    server.post({ path: '/billing',                         version: '1.0.0' },       BillingController.Create);
    server.del({ path: '/billing',                          version: '1.0.0' },       BillingController.Delete);
    server.post({ path: '/billing/event',                   version: '1.0.0' },       BillingController.Notify);
    server.put({ path: '/billing',                          version: '1.0.0' },       BillingController.Update);

    // -----------------
    //   Server Events
    // -----------------

    server.on('NotFound', function(req, res) {
        return Client.NotFound(req, res, 'Invalid API endpoint.');
    });

    server.on('NotAuthorized', function(req, res) {
        return Client.NotAuthorized(req, res, 'Not Authorized.');
    });

    server.on('uncaughtException', function (req, res, route, err) {
        Client.ServerError(req, res);
        Util.log('Error in: ', route);
        Util.inspect(err);

        // stop taking new requests.
        server.close();

        // Tell the master we're toast
        Cluster.worker.disconnect();

        // Give other requests a 5 seconds to finish
        var killtimer = setTimeout(function() {
            process.exit(1);
        }, 5000);

        // But don't keep the process open just for that!
        killtimer.unref();

        return;
    });

    // --------------------
    //   Start the Server
    // --------------------

    server.listen(Settings.port, function() {
        Util.log('Worker Listening: ' + server.url);
    });
}

