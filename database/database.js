// ----------------
//   Dependencies
// ----------------

var Config              = require('../config.json');
var Sequelize           = require("sequelize");

// ------------------------
//   MySQL Connection
// ------------------------

var DB = new Sequelize(
    Config.database.name,
    Config.database.username,
    Config.database.password,
    {
        dialect: 'mysql',
        maxConcurrentQueries: 100,
        logging: console.log,
        define: { timestamps: false },
        sync: { force: false },
        pool: { maxConnections: 100, maxIdleTime: 5 },
        host: Config.database.host,
        port: Config.database.port
    }
);

exports.DB = DB;