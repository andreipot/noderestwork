// ----------------
//   Dependancies
// ----------------

// Config
var Config      = require('../config.json');
var Tokens      = require('../utils/tokens').Tokens;

// -----------------
//  Manage Users
// -----------------
exports.manage = function(req, res) {
    if (req.session.user.type == 0) {
        var scripts = new Array();
        var stylesheets = new Array();

        // Queue any scripts for inclusion
        scripts.push('js/models/billing.js');
        scripts.push('js/controllers/billing_controller.js');

        // Queue any stylesheets for inclusion
        // stylesheets.push('');

        res.render('billing/manage', {
            js: scripts,
            stylesheets: stylesheets,
            config: Config,
            session: req.session,
            flash: req.flash(),
            title: 'Manage Billing - ',
            title_suffix: '',
            section: 'billing'
        });
    } else {
        res.redirect('/404');
    }
}