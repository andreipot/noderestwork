// ----------------
//   Dependancies
// ----------------

// Config
var Config      = require('../config.json');
var Tokens      = require('../utils/tokens').Tokens;

// -----------------
//  Manage Users
// -----------------
exports.manage = function(req, res) {
    if (req.session.user.type == 0 || req.session.user.type == 1) {
        var scripts = new Array();
        var stylesheets = new Array();

        // Queue any scripts for inclusion
        scripts.push('js/models/user.js');
        scripts.push('js/models/task_template.js');
        scripts.push('js/directives/workwall.js');
        scripts.push('js/directives/gravatar.js');
        scripts.push('js/controllers/tasks_controller.js');

        // Queue any stylesheets for inclusion
        // stylesheets.push('');

        res.render('tasks/manage', {
            js: scripts,
            stylesheets: stylesheets,
            config: Config,
            session: req.session,
            flash: req.flash(),
            title: 'Manage Tasks - ',
            title_suffix: '',
            section: 'tasks'
        });
    } else {
        res.redirect('/404');
    }
}