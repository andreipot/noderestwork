// ----------------
//   Dependancies
// ----------------

// Config
var Config      = require('../config.json');
var UUID        = require('node-uuid');
var Util        = require('util');
var Utils       = require('../utils/util').Util;
var Tokens      = require('../utils/tokens').Tokens;

// Models
var Models      = require('../models/');
var User        = Models.User;

// -----------------
//   404 Not Found
// -----------------
exports.not_found = function(req, res) {
    var scripts		= new Array();
    var stylesheets = new Array();

    res.setHeader('Cache-Control', 'public, max-age=' + (3600 * 12));
    res.setHeader('Vary', 'Accept Encoding');

    // Queue any scripts for inclusion
    // scripts.push('');

    // Queue any stylesheets for inclusion
    // stylesheets.push('');

    res.render('index/404', {
        js: scripts,
        stylesheets: stylesheets,
        config: Config,
        session: req.session,
        flash: req.flash(),
        title: 'Page Not Found (404) - ',
        title_suffix: '',
        section: false
    });
}

// -----------------
//  Homepage
// -----------------
exports.index = function(req, res) {
    if (req.session.token && req.session.user) {
        res.redirect('/campaigns');
    } else {
        res.redirect('/login');
    }
    return;
}

// -----------------
//  Process Login
// -----------------
exports.process_login = function(req, res) {
    if (req.body.username && req.body.password) {
        req.body.email = req.body.username;
        delete req.body.username;
        User.find({ where: { email: req.body.email, password: Utils.md5(req.body.password) } })
            .success(function(user) {
                if (user) {
                    req.session.user_id = user.id;
                    req.session.token = Tokens.Issue(user);
                    req.session.account_id = user.account_id;
                    res.redirect('/campaigns');
                } else {
                    req.flash('error', 'Invalid credentials.' );
                    res.redirect('/login');
                }
            })
            .error(function(e, e2) {
                Util.log('LOGIN ERROR');
                Util.log(e, e2);
                res.redirect('/login');
            });
    } else {
        Util.log('LOGIN ERROR (MISSING PARAMS)');
        Util.log(req.body);
        res.redirect('/login');
    }
}

// -----------------
//  Login
// -----------------
exports.login = function(req, res) {
    if (req.cookies.remember_me && req.session.token && req.session.user) {
        res.redirect('/campaigns');
        return;
    }

    var scripts		= new Array();
    var stylesheets = new Array();

    // Queue any scripts for inclusion
    //scripts.push('js/controllers/search.js');

    // Queue any stylesheets for inclusion
    // stylesheets.push('');

    res.render('index/login', {
        js: scripts,
        stylesheets: stylesheets,
        config: Config,
        session: req.session,
        flash: req.flash(),
        title: 'Login - ',
        title_suffix: '',
        section: 'home'
    });
}

// -----------------
//  Create Account
// -----------------
/*exports.create_account = function(req, res) {
    // Check if this is a reg submit
    if (req.body.email && req.body.password) {
        var user_id = UUID.v4();
        console.log('Generated v4 UUID', user_id);

        User.create({
            id: user_id.toString(),
            email: req.body.email,
            fname: req.body.fname,
            lname: req.body.lname,
            password: req.body.password
        })
            .success(function(user, created) {
                if (user) {
                    console.log('FRESHLY CREATED');
                    console.log(user);
                    req.flash('info', 'Registration successful.  Please check your email and click the confirmation link.' );
                    res.redirect('/login');
                } else {
                    console.log('ERROR: CREATING USER');
                    console.log(user);
                    req.flash('error', 'There was an error creating your account. Please contact support.' );
                    res.redirect('/register');
                }
            })
            .error(function(error, e2) {
                console.log('ERROR: USER EXISTS');
                console.log(error, e2);
                req.flash('error', 'User already exists.' );
                res.redirect('/register');
            });
    } else {
        res.redirect('/login');
    }
}*/

// -----------------
//  Logout
// -----------------
exports.logout = function(req, res) {
    delete req.session.user_id;
    delete req.session.token;
    delete req.session.user;
    res.redirect('/login');
}