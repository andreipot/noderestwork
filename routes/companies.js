// ----------------
//   Dependancies
// ----------------

// Config
var Config      = require('../config.json');
var Tokens      = require('../utils/tokens').Tokens;

// -----------------
//  Manage Users
// -----------------
exports.manage = function(req, res) {
    if (req.session.user.type == 0 || req.session.user.type == 1) {
        var scripts = new Array();
        var stylesheets = new Array();

        // Queue any scripts for inclusion
        scripts.push('js/models/user.js');
        scripts.push('js/models/company.js');
        scripts.push('js/controllers/manage_companies_controller.js');

        // Queue any stylesheets for inclusion
        // stylesheets.push('');

        res.render('companies/manage', {
            js: scripts,
            stylesheets: stylesheets,
            config: Config,
            session: req.session,
            flash: req.flash(),
            title: 'Manage Clients - ',
            title_suffix: '',
            section: 'clients'
        });
    } else {
        res.redirect('/404');
    }
}