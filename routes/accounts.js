// ----------------
//   Dependancies
// ----------------

// Config
var Config      = require('../config.json');
var Tokens      = require('../utils/tokens').Tokens;

// -----------------
//  Manage Users
// -----------------
exports.manage = function(req, res) {
    if (req.session.user.is_super_admin == 0) {
        var scripts = new Array();
        var stylesheets = new Array();

        // Queue any scripts for inclusion
        scripts.push('js/models/user.js');
        scripts.push('js/models/account.js');
        scripts.push('js/controllers/manage_accounts_controller.js');

        // Queue any stylesheets for inclusion
        // stylesheets.push('');

        res.render('accounts/manage', {
            js: scripts,
            stylesheets: stylesheets,
            config: Config,
            session: req.session,
            flash: req.flash(),
            title: 'Manage Accounts - ',
            title_suffix: '',
            section: 'admin'
        });
    } else {
        res.redirect('/404');
    }
}