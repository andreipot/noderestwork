// ----------------
//   Dependancies
// ----------------

// Config
var Config      = require('../config.json');
var Tokens      = require('../utils/tokens').Tokens;

// -----------------
//  Manage Users
// -----------------
exports.view = function(req, res) {
    var scripts		= new Array();
    var stylesheets = new Array();

    // Queue any scripts for inclusion
    scripts.push('js/models/user.js');
    scripts.push('js/models/company.js');
    scripts.push('js/models/campaign.js');
    scripts.push('js/models/task_template.js');
    scripts.push('js/directives/gravatar.js');
    scripts.push('js/directives/workwall.js');
    scripts.push('js/directives/timeline.js');
    scripts.push('js/directives/activity.js');
    scripts.push('js/controllers/campaigns_controller.js');

    // Queue any stylesheets for inclusion
    //stylesheets.push('');

    if (req.originalUrl == '/campaigns') {
        return res.redirect('/campaigns/#/');
    }

    res.render('campaigns/view', {
        js: scripts,
        stylesheets: stylesheets,
        config: Config,
        session: req.session,
        flash: req.flash(),
        title: 'Campaign Manager - ',
        title_suffix: '',
        section: 'campaign'
    });
}