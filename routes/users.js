// ----------------
//   Dependancies
// ----------------

// Config
var Config      = require('../config.json');
var Tokens      = require('../utils/tokens').Tokens;

// -----------------
//  Manage Users
// -----------------
exports.manage = function(req, res) {
    if (req.session.user.type == 0 || req.session.user.type == 1) {
        var scripts		= new Array();
        var stylesheets = new Array();

        // Queue any scripts for inclusion
        scripts.push('js/models/user.js');
        scripts.push('js/controllers/manage_users_controller.js');

        // Queue any stylesheets for inclusion
        // stylesheets.push('');

        res.render('users/manage', {
            js: scripts,
            stylesheets: stylesheets,
            config: Config,
            session: req.session,
            flash: req.flash(),
            title: 'Manage Users - ',
            title_suffix: '',
            section: 'admin'
        });
    } else {
        res.redirect('/404');
    }
}

// ------------------
//  Change Password
// ------------------
exports.change_pwd = function(req, res) {
    var scripts		= new Array();
    var stylesheets = new Array();

    // Queue any scripts for inclusion
    scripts.push('js/models/user.js');
    scripts.push('js/controllers/change_password_controller.js');

    // Queue any stylesheets for inclusion
    // stylesheets.push('');

    res.render('users/change_password', {
        js: scripts,
        stylesheets: stylesheets,
        config: Config,
        session: req.session,
        flash: req.flash(),
        title: 'Change Your Password - ',
        title_suffix: '',
        section: 'admin'
    });
}