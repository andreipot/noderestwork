// ----------------
//   Dependancies
// ----------------

var Time 	= require('time');
var URL		= require('url');
var Crypto	= require('crypto');

/**
 * Utilities Object
 */
var Util = function() { }

/**
 * Implementation of php md5
 *
 * @param	string	data	string for md5 hashing
 * @return	string			hashed string
 **/
Util.prototype.md5 = function(str) {
    return Crypto.createHash('md5').update(str.toString()).digest('hex');
}

this.Util = new Util();
