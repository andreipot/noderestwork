// Config
var Config = require('../config.json');

// Redis
var Redis = require('redis');
var RedisStore = Redis.createClient(Config.redis_port, Config.redis_host);

exports.RedisStore = RedisStore;
