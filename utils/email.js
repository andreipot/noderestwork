// ----------------
//   Dependencies
// ----------------

var Util            = require('util');
var path            = require('path');
var templatesDir    = path.resolve('./views/email_templates');
var EmailTemplates  = require('email-templates');
var NodeMailer      = require('nodemailer');
var Config          = require('../config.json');

// -------------------------
//   Controller Definition
// -------------------------

var Email = function() { }

/**
 * Send email system functions
 *
 * @param	Object    locals	         Data for template
 * @param	String	  template_name      Name of email template
 * @param	String	  email_theme    	 Email subject
 */
Email.prototype.Send = function(locals, template_name, email_theme, done) {
    EmailTemplates(templatesDir, function(err, Send) {
        if (err) {
            console.log(err);
        } else {
            // Prepare nodemailer transport object
            var transport = NodeMailer.createTransport(Config.email.protocol, Config.email.server);

            // Send an email
            Send(template_name, locals, function(err, html, text) {
                if (err) {
                    Util.log(err);
                } else {
                    transport.sendMail({
                        from: Config.email.from_name + ' <' + Config.email.from_email + '>',
                        to: locals.email,
                        subject: email_theme,
                        html: html,
                        text: text
                    }, function(err, responseStatus) {
                        if (err) {
                            Util.log(err);
                        } else {
                            Util.log(responseStatus.message);
                        }

                        Util.log('-> Email.Send()');
                        if (typeof done == 'function') {
                            done(err, responseStatus);
                        }
                    });
                }
            });
        }
    });
}

this.Email = new Email();