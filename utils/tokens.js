// ----------------
//   Dependancies
// ----------------

var Time        = require('time');
var UUID        = require('node-uuid');
var RedisStore  = require('./redis_store').RedisStore;

/**
 * Utilities Object
 */
var Tokens = function() { }

/**
 * Lookup a token and check its validity
 *
 * @param    string    str    Token string
 * @return   obj              Object with token data
 **/
Tokens.prototype.Authenticate = function(str, callback) {
    RedisStore.get('token_' + str, function(error, token) {
        if (error) {
            console.log(error);
            var token = { is_valid: false };
        }

        var token = JSON.parse(token);

        var now = parseInt(new Time.Date().getTime() / 1000);
        if (token && token.expires && token.expires > now) {
            token.is_valid = true;
        } else {
            var token = { is_valid: false };
        }

         callback(token);
    });
}

/**
 * Issue a token for the user
 *
 * @param    string    str    Token string
 * @return   obj              Object with token data
 **/
Tokens.prototype.Issue = function(user) {
    var now = parseInt(new Time.Date().getTime() / 1000);
    var token_id = UUID.v4();

    var token = {
        issued: now,
        expires: (3600 * 24) + now,
        user: user
    }

    RedisStore.set('token_' + token_id.toString(), JSON.stringify(token));

    return token_id.toString();
}

this.Tokens = new Tokens();
