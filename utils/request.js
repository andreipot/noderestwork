// ----------------
//   Dependancies
// ----------------

var Time 	= require('time');
var URL		= require('url');

/**
 * Request Utilities Object
 */
var Request = function() { }

/**
 * Returns an array of params
 *
 * @param	string	url		Full URL to parse
 * @return	array			Array of URL parameters
 **/
Request.prototype.GetParams = function(url) {
    var params = URL.parse(url, true);

    return params.query;
}

/**
 * Instance the object
 */
this.Request = new Request();