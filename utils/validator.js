// ----------------
//   Dependancies
// ----------------

var validator = require('validator');

/**
 * Assists with validating properties
 */
var Validation = function() {
    this.errors = [];
}

Validation.prototype.AssertTrue = function(func, args, message) {
    console.log(func, args);
    if (func == 'inRange') {
        if (!InRange(args[0], args[1][0], args[1][1])) {
            this.errors.push(message);
        }
    } else if (!validator[func].apply(validator, args)) {
        console.log(args[0], func, validator[func].apply(validator, args));
        this.errors.push(message);
    }

    return this;
}

Validation.prototype.AssertFalse = function(func, args, message) {
    console.log(func, args);
    if (func == 'inRange') {
        if (InRange(args[0], args[1][0], args[1][1])) {
            this.errors.push(message);
        }
    } else if (validator[func].apply(validator, args)) {
        console.log(args[0], func, validator[func].apply(validator, args));
        this.errors.push(message);
    }

    return this;
}

Validation.prototype.GetErrors = function() {
    return this.errors;
}

var InRange = function(value, min, max) {
    if (parseInt(value) >= parseInt(min) && parseInt(value) <= parseInt(max)) {
        return true;
    } else {
        return false;
    }
}

exports.Validation = Validation;


