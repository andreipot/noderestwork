// Utils
var frisby      = require('frisby');
var Async       = require('async');

// Fixtures
var Fixtures    = require('./../../test_helpers/test_setup');

// Models
var Models      = require('../../models');
var User        = Models.User;

// --
// Test Data
// --

var test_user = {
    fname: 'TestUser',
    lname: 'LastName',
    email: 'sfurnival+workadotest@gmail.com',
    type: User.Type.ADMIN
}

var api_key = false;

// --
// Start Tests
// --

Async.series([
    function(cb) {
        Fixtures.DoSetup(function(key) {
            api_key = key;
            cb();
        });
    },
    /*function(cb) {
        // --
        frisby.create('Users: Create a User (ADMIN)')
        // --
            .get('http://dev.workado.io/api/user/10c0dde4-cf83-4a4c-8536-ead8c4bfefec')
            .expectStatus(200)
            .expectHeader('Content-Type', 'application/json')
            .expectJSON({
                id: '10c0dde4-cf83-4a4c-8536-ead8c4bfefec',
                fname: "Stephen",
                lname: "Furnival",
                email: "sfurnival@gmail.com",
                plan_id: 0,
                status: 0
            })
            .expectJSONTypes({
                id: String,
                fname: String,
                lname: String,
                email: String,
                plan_id: Number,
                status: Number
            })
            .expectJSONLength('id', 36)
            .after(cb)
            .toss();
    },
    function(cb) {
        // --
        frisby.create('Users: Retrieve an Invalid User')
        // --
            .get('http://dev.workado.io/api/user/xxxxxxxxx')
            .expectStatus(400)
            .expectHeader('Content-Type', 'application/json')
            .expectJSON({
                message:[
                    "Path parameter :user_id must be a valid UUID."
                ]
            })
            .expectJSONTypes({
                message: Array
            })
            .after(cb)
            .toss();
    },
    function(cb) {
        // --
        frisby.create('Users: Retrieve a user with no ID')
        // --
            .get('http://dev.workado.io/api/user/')
            .expectStatus(404)
            .expectHeader('Content-Type', 'application/json')
            .expectJSON({
                message: 'Invalid API endpoint.'
            })
            .expectJSONTypes({
                message: String
            })
            .after(cb)
            .toss();
    }*/
],

function(error, results) {
    Fixtures.DoTeardown();
});





