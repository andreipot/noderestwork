// ----------------
//   Dependancies
// ----------------

var Helenus			= require('helenus');
var db				= require('./database/database').Pool;
var Schema			= require('./database/keyspace.json');

var Config  = require('./config.json');

//var privateKey      = fs.readFileSync('../key').toString();
//var certificate     = fs.readFileSync('../crt').toString();
//var ca              = fs.readFileSync('../intermediate.crt').toString();

var io		= require('socket.io').listen(Config.socket_server_port);
var Redis	= require("redis");

// Redis Clients
var Publisher = Redis.createClient(Config.redis_port, Config.redis_host);

// Models
var Token = require('./models/token.js').Token;

// Data
var LiveUsers = [];

// Start things up
db.connect(function(err, keyspace) {
    if (err) {
        console.log('Failed to connect to Cassandra...');
        console.log(err);
    } else {
        console.log('Connected to Cassandra...');

        // -----------------
        //   Configure IO
        // -----------------

        /*socketio = require('socket.io')

         var io = socketio.listen(app, {origins: '*:*'});

         var host = 'redisserver'
         var opts = {redisSub:{host:host},redisPub:{host:host},redisClient:{host:host}}
         io.set('store', new socketio.RedisStore(opts))*/

        io.configure(function () {
            io.set('log level', 3);  // 3 - Debug / 0 - Error
            io.set('authorization', function (handshakeData, callback) {
                if (handshakeData.query && handshakeData.query.auth_param) {
                    Token.Find(handshakeData.query.auth_param, function(error, token) {
                        if (token && token.is_valid) {
                            callback(error, true);
                        } else {
                            callback(error, false);
                        }
                    });
                } else {
                    callback(new Error('Client failed to supply authorization.'), false); // error first callback style
                }

                console.log(handshakeData);
            });
        });

        // -----------------
        //   Server Events
        // -----------------
        io.sockets.on('connection', function(socket) {

            var Subscriber = Redis.createClient(Config.redis_port, Config.redis_host);

            var this_user = {
                id: new Helenus.UUID().toString(),
                socket: socket,
                token: false
            }

            LiveUsers.push(this_user);

            // -----------------
            //   Socket Events
            // -----------------

            socket.on('subscribe', function(message) {
                if (message && message.channel) {
                    Subscriber.subscribe(message.channel);
                    socket.emit('subscription_ready', message);
                    socket.emit('log', "Registered for: [" + message.channel + "]");
                } else {
                    socket.emit('log', "Failed registering for: [" + message.channel + "]");
                }
            });

            socket.on('disconnect', function() {
                console.log('...disconnect');
                delete this_user;
                Subscriber.end();
            });

            // -----------------------------
            //   Redis Subscription Events
            // -----------------------------

            Subscriber.on('message', function(channel, event) {
                console.log('Redis channel message ['+channel+']: ' + event.toString() );
                socket.emit('subscription_event', {
                    channel: channel,
                    event: event
                });
            });

            Subscriber.on('subscribe', function(channel, count) {
                console.log('Subscribed to Channel [' + channel + ']:' + count);
            });

            Subscriber.on('unsubscribe', function(channel, count) {
                console.log('Unsubscribed from Channel [' + channel + ']:' + count);
            });

            // ------------------
            //   Send a welcome
            // ------------------

            //socket.emit('log', "Welcome to MobAff Tracker live socket.");
            //Subscriber.subscribe('events_page_viewed');
        });

        io.sockets.on('error', function (reason){
            console.error(reason);
        });

    }
});
