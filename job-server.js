// ----------------
//   Dependencies
// ----------------

var Util            = require('util');
var Kue             = require('kue');
var Redis           = require('redis');

var Config	        = require('./config.json');

// Job Handlers
var SystemHandler      = require('./jobs/system').SystemHandler;

// --------------------------
//   Server Settings & Init
// --------------------------

var Settings = {
    cpu_cores:		require('os').cpus().length,
    debug_on:		false
}

Kue.redis.createClient = function() {
    var client = Redis.createClient(Config.redis_port, Config.redis_host);
    return client;
};

var Jobs            = Kue.createQueue();


Kue.app.set('title', 'Workado Job Server');
Kue.app.listen(Config.kue_server_port);


// --------------------------
//   System Jobs
// --------------------------

Jobs.process('end_of_month', SystemHandler.EndOfMonth);

Util.log('Workado Job Server started...');
