// Utils
var Util        = require('util');
var Utils       = require('../utils/util').Util;
var UUID        = require('node-uuid');
var Tokens      = require('../utils/tokens').Tokens;
var Async       = require('async');

// Models
var Models      = require('../models/');
var User        = Models.User;
var Account     = Models.Account;


var Globals = {
    Super: {}
};

// --
// Create an Account to Test with.
// --

var create_test_account = function(callback) {
    Account.create({
        id: UUID.v4().toString(),
        billing_fname: 'Testing',
        billing_lname: 'Fixtures',
        status: Account.Status.ACTIVE,
        billing_email: 'sfurnival+temp_workado_fixtures@gmail.com',
        billing_company: 'Acme Test Company'
    })
        .success(function(account) {
            Globals.Account = account;
            callback(false);
        })
        .error(function(error) {
            Util.log('Error creating test Account.');
            console.log(error);
            callback(true);
        });
}

// --
// Create a Super Admin to Test with.
// --

var create_super_user = function(callback) {
    User.create({
        id: UUID.v4().toString(),
        fname: 'TestsSuper',
        lname: 'TestsAdmin',
        status: User.Status.ACTIVE,
        email: 'sfurnival+temp_workado_su@gmail.com',
        account_id: Globals.Account.id,
        is_super_admin: false,
        type: User.Type.ADMIN,
        password: Utils.md5('asdasdas')
    })
        .success(function(user) {
            Globals.Super = user;
            callback(false);
        })
        .error(function(error) {
            Util.log('Error creating Super User.');
            console.log(error);
            callback(true);
        });
}

// --
// Export Setup Function
// --

exports.DoSetup = function(callback) {
    create_test_account(function() {
        create_super_user(function() {
            callback(Globals, Tokens.Issue(Globals.Super));
        })
    });
}

// --
// Export Teardown Function
// --

exports.DoTeardown = function(callback) {
    Async.parallel([
        function(cb) {
            Globals.Account.destroy()
                .success(function() {
                    cb(false, 'Teardown of testing Account completed.');
                })
                .error(function(error) {
                    cb('Error finishing Account teardown.');
                });
        },
        function(cb) {
            Globals.Super.destroy()
                .success(function() {
                    cb(false, 'Teardown of Super User completed.');
                })
                .error(function(error) {
                    cb('Error finishing Super User teardown.');
                });
        }
    ],

    function(errors, results) {
        if (errors) {
            console.log(errors);
            console.log('Orphaned test data may exist.');
        }

        callback();
    });
}


