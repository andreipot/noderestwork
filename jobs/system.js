// Utils
var Util        = require('util');
var Email       = require('../utils/email.js').Email;
var Config      = require('../config.json');
var Async       = require('async');

var Models      = require('../models');
var Campaign    = Models.Campaign;
var Company     = Models.Company;
var Task        = Models.Task;
var Account     = Models.Account;

var SystemHandler = {}

SystemHandler = {
    EndOfMonth: function(job, done) {
        var now = new Date().getTime();
        var start_hr = process.hrtime();

        // Start our job workload counters
        job.data.workload_weight = 1;
        job.data.workload_done   = 0;

        // Start Job Log
        job.log('Started job at: [' + now + '] [' + start_hr + ']');
        console.log('Started job at: [' + now + '] [' + start_hr + ']');

        job.progress(1);

        // --
        // Fetch all active campaigns for activity review.
        // --
        Campaign.findAll({ where: { status: Campaign.Status.ACTIVE } })
            .success(function(campaigns) {
                // Set total job workload.
                job.data.workload_done++;
                job.data.workload_weight += campaigns.length;
                job.progress(job.data.workload_done, job.data.workload_weight);

                Util.log(campaigns.length + ' Campaigns to process...');

                if (campaigns.length) {
                    Async.forEachSeries(campaigns, function(camp, cb) {
                        Company.find({ where: { id: camp.company_id }})
                            .success(function(company) {
                                // --
                                // Check if this campaign needs summary reports
                                // --
                                if (company.do_reporting) {
                                    // mstart should always be the beginning of the month just
                                    // finished. This assumes that right now is at least one
                                    // second after the end of the month.
                                    var mstart = new Date();
                                    mstart.setMonth( mstart.getMonth() - 1 );
                                    mstart.setHours(0);
                                    mstart.setSeconds(0);
                                    mstart.setDate(1);

                                    Account.find({ where: { id: camp.account_id }})
                                        .success(function(account) {
                                            console.log('Finding tasks since: ', mstart.toISOString());

                                            Task.findAll({ where: { campaign_id: camp.id, completed_on: { gte: mstart.getTime() / 1000 } } })
                                                .success(function(tasks) {
                                                    var email = 'sfurnival@gmail.com';

                                                    if (camp.override_email) {
                                                        email = camp.override_email;
                                                    } else {
                                                        email = company.email;
                                                    }

                                                    console.log('Email:', email);

                                                    var data = {
                                                        email: email,
                                                        company: company,
                                                        campaign: camp,
                                                        tasks: tasks,
                                                        date: mstart,
                                                        account: account
                                                    }

                                                    // --
                                                    // Data Collected, Send the Email
                                                    // --
                                                    Email.Send(data, 'activity_summary', 'Your Monthly Campaign Summary', function(err, responseStatus) {
                                                        job.data.workload_done++;
                                                        job.progress(job.data.workload_done, job.data.workload_weight);

                                                        cb();
                                                    });
                                                })
                                                .error(function(error) {
                                                    cb(error);
                                                });
                                        })
                                        .error(function(e) {
                                            cb(error);
                                        })
                                } else {
                                    job.data.workload_done++;
                                    job.progress(job.data.workload_done, job.data.workload_weight);
                                    cb();
                                }
                            })
                            .error(function(error) {
                                cb(error);
                            })
                    },

                    // --
                    // All Campaigns Processed
                    // --
                    function(errors) {
                        if (errors) {
                            Util.log('Errors Processing -- End Of Month -- Job');
                            console.log(errors);
                        }

                        job.progress(100);
                        job.save();

                        job.log('Job Done! [' + process.hrtime(start_hr) + '] [' + start_hr + ']');

                        done();
                    });
                } else {
                    Util.log('Done Processing -- End Of Month -- [ No Campaigns ]');

                    job.progress(100);
                    job.save();

                    job.log('Job Done! [' + process.hrtime(start_hr) + '] [' + start_hr + ']');

                    done();
                }

            })
            .error(function(err) {
                Util.log(err);
                Util.log('Did -- End Of Month -- Job');
                done();
            })
    }
}

exports.SystemHandler = SystemHandler;