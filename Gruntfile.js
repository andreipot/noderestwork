module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            css: {
                src: [
                    'public/css/*',
                    'public/scrollbar/src/perfect-scrollbar.css'
                ],
                dest: 'public/compiled.css'
            },
            js: {
                src: [
                    'public/js/*.js',
                    'public/js/**/*.js',
                    'public/scrollbar/src/*.js',
                    'public/ngOnboarding/dist/ng-onboarding.js'
                ],
                dest: 'public/compiled.js'
            }
        },

        cssmin: {
            css: {
                src: 'public/compiled.css',
                dest: 'public/compiled.min.css'
            }
        },

        uglify: {
            js: {
                files: {
                    'public/compiled.min.js': ['public/compiled.js']
                }
            },
            options: {
                mangle: false
            }
        },

        watch: {
            files: [ 'public/css/*', 'public/js/*' ],
            tasks: ['concat', 'cssmin', 'uglify']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-ngmin');
    grunt.registerTask('default', [ 'concat:css', 'cssmin:css', 'concat:js', 'uglify:js' ]);
};
