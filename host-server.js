// --
// Dependancies
// --

var httpProxy       = require('http-proxy');
var path            = require("path");
var fs              = require("fs");
var crypto          = require("crypto");

var Config          = require('./config.json');

var routing_domains = {

    // --
    // Staging Domains
    // --

    'dev.workado.io/api':                '127.0.0.1:41081',
    'dev.workado.io':                    '127.0.0.1:41080',
    'dev.ws.workado.io':                 '127.0.0.1:41055'

};

// --
// Configure Our Hosts
// --

var options = {
    router: routing_domains
}

// --
// Create and Start the Proxy
// --

var proxyServer = httpProxy.createServer(options);
proxyServer.listen(80);
console.log('Now Accepting Requests...');
