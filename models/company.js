// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('Company',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The company\'s id.',
                primaryKey: true,
                allowNull: false
            },

            account_id: {
                type: Sequelize.STRING(36),
                comment: 'The account that owns this company.',
                primaryKey: true,
                allowNull: false
            },

            name: {
                type: Sequelize.STRING(128),
                comment: 'The company\'s name.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 128]
                }
            },

            website: {
                type: Sequelize.STRING(128),
                comment: 'The company\'s website.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 128]
                }
            },

            logo: {
                type: Sequelize.STRING(256),
                comment: 'The company\'s logo.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 256]
                }
            },

            contact: {
                type: Sequelize.STRING(64),
                comment: 'The company\'s primary contact.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 64]
                }
            },

            email: {
                type: Sequelize.STRING(128),
                comment: 'The company contact\'s primary email.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 128],
                    isEmail: true
                }
            },

            phone: {
                type: Sequelize.INTEGER,
                comment: 'The company contact\'s primary phone.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    isNumeric: true
                }
            },

            do_reporting: {
                type: Sequelize.BOOLEAN,
                comment: 'Does this company require status updates?',
                allowNull: false,
                validate: {
                    isIn: [[true, false]]
                }
            },

            status: {
                type: Sequelize.INTEGER,
                comment: 'Company Status // 0: Deleted, 1: Active, 2: Paused',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 2 }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'companies',

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

                Status: {
                    DELETED: 0,
                    ACTIVE: 1,
                    PAUSED: 2
                }

            }
        }
    );
};