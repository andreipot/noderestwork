// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('CampaignNoteView',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            user_id: {
                type: Sequelize.STRING(36),
                primaryKey: true,
                comment: 'The user that created viewed the note.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            campaign_note_id: {
                type: Sequelize.STRING(36),
                primaryKey: true,
                comment: 'The campaign note this note belongs to.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'campaign_note_views',

            // -------------------------
            //   Instance Methods
            // -------------------------

            instanceMethods: {

            },

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

            }
        }
    );
};