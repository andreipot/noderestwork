// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('Campaign',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The campaign\'s id.',
                primaryKey: true,
                allowNull: false
            },

            company_id: {
                type: Sequelize.STRING(36),
                comment: 'The company\'s id.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            account_id: {
                type: Sequelize.STRING(36),
                comment: 'The account that owns this company.',
                primaryKey: true,
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            name: {
                type: Sequelize.STRING(128),
                comment: 'Campaign identifying name.',
                allowNull: true,
                validate: {
                    len: [0, 128]
                }
            },

            start_at: {
                type: Sequelize.INTEGER,
                comment: 'Unix timestamp the campaign will start at (ie. Month 1)',
                allowNull: true
            },

            stop_at: {
                type: Sequelize.INTEGER,
                comment: 'Unix timestamp the campaign will stop at (ie. The last month)',
                allowNull: true
            },
//
            override_contact: {
                type: Sequelize.STRING(64),
                comment: 'The company\'s primary contact.',
                allowNull: true,
                validate: {
                    len: [0, 64]
                }
            },

            override_email: {
                type: Sequelize.STRING(128),
                comment: 'The company contact\'s primary email.',
                allowNull: true,
                validate: {
                    len: [0, 128]
                }
            },

            override_phone: {
                type: Sequelize.INTEGER,
                comment: 'The company contact\'s primary phone.',
                allowNull: true,
                validate: {
                    isNumeric: true
                }
            },
//
            status: {
                type: Sequelize.INTEGER,
                comment: 'User Status // 0: New (Unconfirmed), 1: Active, 2: Paused',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 2 }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'campaigns',

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

                Status: {
                    DELETED: 0,
                    ACTIVE: 1,
                    PAUSED: 2
                }

            },

            // -------------------------
            //   Instance Methods
            // -------------------------

            instanceMethods: {

                getNotesForUser: function(user_id, cb) {
                    try {
                        db.query(
                            "SELECT campaign_notes.*, campaign_note_views.created_at AS viewed " +
                            "  FROM campaign_notes " +
                            "  LEFT JOIN campaign_note_views ON campaign_notes.id = campaign_note_views.campaign_note_id AND campaign_note_views.user_id = '" + user_id + "'" +
                            "  WHERE campaign_notes.campaign_id = '" + this.id + "' " +
                            "  ORDER BY campaign_notes.created_at DESC"
                        )
                        .success(function(notes) {
                            cb(null, notes);
                        })
                        .error(function(e) {
                            cb(e, null);
                        })
                    }
                    catch (e) {
                        cb(e, null);
                    }
                }

            }
        }
    );
};