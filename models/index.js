var Config = require('../config.json');
var DB     = require('../database/database').DB;

// load models
var models = [
    'User',
    'Task',
    'Company',
    'Campaign',
    'Campaign_Note',
    'Campaign_Note_View',
    'Task_Template',
    'Month_Template',
    'Account'
];

models.forEach(function(model) {
    module.exports[model.replace(/_/g, '')] = DB.import(__dirname + '/' + model.toLowerCase());
});


(function(m) {

    // Describe Relationships
    m.Campaign
        .hasMany(m.Task, { as: 'Tasks', foreignKey : 'campaign_id' })
        .hasMany(m.CampaignNote, { as: 'Notes', foreignKey : 'campaign_id' })
        .hasOne(m.Company, { as: 'Campaign', foreignKey : 'company_id'});

    m.Company
        .hasMany(m.Campaign, { as: 'Campaigns', foreignKey : 'company_id' })
        .hasMany(m.Task, { as: 'Tasks', foreignKey : 'company_id' })

    m.User
        .hasMany(m.Task, { as: 'AssignedTasks', foreignKey : 'assigned_to' })
        .hasMany(m.Task, { as: 'CompletedTasks', foreignKey : 'completed_by' })

    m.Task
        .hasOne(m.Campaign, { as: 'Campaign', foreignKey : 'campaign_id' })
        .hasOne(m.User, { as: 'CompletedBy', foreignKey : 'completed_by' })
        .hasOne(m.User, { as: 'AssignedTo', foreignKey : 'assigned_to' })

})(module.exports);

// export connection
module.exports.DB = DB;