// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('MonthTemplate',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The template\'s id.',
                primaryKey: true,
                allowNull: false
            },

            account_id: {
                type: Sequelize.STRING(36),
                comment: 'The account that owns this template.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            title: {
                type: Sequelize.STRING(128),
                comment: 'The template\'s name.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 128]
                }
            },

            tasks: {
                type: Sequelize.TEXT,
                comment: 'The task description.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 65535]
                }
            },

            status: {
                type: Sequelize.INTEGER,
                comment: 'MonthTemplate Status // 0: Deleted, 1: Active',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 1 }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'month_templates',

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

                Status: {
                    DELETED: 0,
                    ACTIVE: 1
                }

            }
        }
    );
};