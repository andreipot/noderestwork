// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('Task',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The task\'s id.',
                primaryKey: true,
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36]
                }
            },

            campaign_id: {
                type: Sequelize.STRING(36),
                comment: 'The owning campaign id.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36]
                }
            },

            company_id: {
                type: Sequelize.STRING(36),
                comment: 'The owning company id.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36]
                }
            },

            account_id: {
                type: Sequelize.STRING(36),
                comment: 'The owning account id.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36]
                }
            },

            title: {
                type: Sequelize.TEXT,
                comment: 'The task\'s title.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 128]
                }
            },

            description: {
                type: Sequelize.TEXT,
                comment: 'The task\'s name.',
                allowNull: false,
                validate: {
                    len: [0, 5120]
                }
            },

            assigned_to: {
                type: Sequelize.STRING(36),
                comment: 'User the task is assigned to.',
                allowNull: true,
                validate: {
                    len: [36, 36]
                }
            },

            completed_by: {
                type: Sequelize.STRING(36),
                comment: 'User who completed the task.',
                allowNull: true,
                validate: {
                    notEmpty: true,
                    len: [36, 36]
                }
            },

            completion_msg: {
                type: Sequelize.TEXT,
                comment: 'The task\'s name.',
                allowNull: true,
                validate: {
                    notEmpty: true,
                    len: [1, 5120]
                }
            },

            completed_on: {
                type: Sequelize.INTEGER,
                comment: 'Unix timestamp for when the task was completed.',
                allowNull: true
            },

            due_on: {
                type: Sequelize.INTEGER,
                comment: 'Unix timestamp for when the task is due',
                allowNull: true
            },

            status: {
                type: Sequelize.INTEGER,
                comment: 'Task Status // 0: Deleted, 1: Active, 2: Paused',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 3 }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'tasks',

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

                Status: {
                    DELETED: 0,
                    ACTIVE: 1,
                    IN_PROGRESS: 2,
                    COMPLETED: 3
                },

                /**
                 * Fetches task activity for a given account.
                 *
                 * @param   account_id      uuid        Desired Account ID
                 * @param   cb              func        Callback function
                 */
                GetActivity: function(account_id, cb) {
                    var Task = this.options.classMethods;

                    db.query(
                        "SELECT tasks.*, users.fname, users.lname, users.email " +
                        "  FROM tasks JOIN users ON tasks.completed_by = users.id " +
                        "  WHERE tasks.account_id = '" + account_id + "' AND tasks.status = " + Task.Status.COMPLETED +
                        "  ORDER BY tasks.completed_on DESC"
                    )
                        .success(function(tasks) {
                            cb(null, tasks);
                        })
                        .error(function(e) {
                            cb(e, null);
                        })
                },

                /**
                 *
                 */
                FindAll_fromActiveCampaigns: function(account_id, cb) {
                    var Task = this.options.classMethods;

                    try {
                        db.query(
                            "SELECT tasks.* " +
                            "  FROM tasks " +
                            "  JOIN campaigns ON tasks.campaign_id = campaigns.id " +
                            "  WHERE tasks.account_id = '" + account_id + "' " +
                            "    AND tasks.status != " + Task.Status.DELETED +
                            "    AND campaigns.status != 0" +
                            "  ORDER BY tasks.completed_on DESC"
                        )
                        .success(function(tasks) {
                            cb(null, tasks);
                        })
                        .error(function(e) {
                            cb(e, null);
                        })
                    }
                    catch (e) {
                        cb(e, null);
                    }
                }
            }
        }
    );
};