// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('Template',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The template\'s id.',
                primaryKey: true,
                allowNull: false
            },

            name: {
                type: Sequelize.STRING(128),
                comment: 'The template\'s name.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 128]
                }
            },

            task_count: {
                type: Sequelize.INTEGER,
                comment: 'The number of tasks in the template.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    isNumeric: true
                }
            },

            tasks: {
                type: Sequelize.TEXT,
                comment: 'The template\'s tasks (in JSON).',
                allowNull: false,
                validate: {
                    notEmpty: true
                }
            },

            status: {
                type: Sequelize.INTEGER,
                comment: 'Template Status // 0: Deleted, 1: Active, 2: Paused',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 2 }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'templates',

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

                Status: {
                    DELETED: 0,
                    ACTIVE: 1,
                    PAUSED: 2
                }

            }
        }
    );
};