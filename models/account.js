// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('Account',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The account\'s id.',
                primaryKey: true,
                allowNull: false
            },

            billing_email: {
                type: Sequelize.STRING(32),
                comment: 'Primary account contact email.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    isEmail: true
                }
            },

            billing_fname: {
                type: Sequelize.STRING(32),
                comment: 'Account\'s first name.',
                allowNull: false,
                validate: {
                    len: [2, 32]
                }
            },

            billing_lname: {
                type: Sequelize.STRING(32),
                comment: 'Account\'s last name.',
                allowNull: false,
                validate: {
                    len: [2, 32]
                }
            },

            billing_company: {
                type: Sequelize.STRING(32),
                comment: 'Account\'s company.',
                allowNull: false,
                validate: {
                    len: [2, 32]
                }
            },

            plan_id: {
                type: Sequelize.INTEGER,
                comment: 'Subscription Plan Type',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 2 }
            },

            stripe_customer_id: {
                type: Sequelize.STRING(64),
                comment: 'Stripe Customer Identifier.',
                allowNull: true,
                validate: {
                    len: [2, 64]
                }
            },

            stripe_subscription_id: {
                type: Sequelize.STRING(64),
                comment: 'Stripe Subscription Identifier.',
                allowNull: true,
                validate: {
                    len: [2, 64]
                }
            },

            stripe_card_id: {
                type: Sequelize.STRING(64),
                comment: 'Stripe CC Identifier.',
                allowNull: true,
                validate: {
                    len: [2, 64]
                }
            },

            status: {
                type: Sequelize.INTEGER,
                comment: 'Account Status // 0: New (Unconfirmed), 1: Active, 2: Locked',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 2 }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'accounts',

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

                Status: {
                    DELETED: 0,
                    ACTIVE: 1,
                    PENDING: 2
                },

                findAllExtra: function(cb) {
                    try {
                        db.query(
                                "SELECT accounts.*, COUNT(users.id) AS user_count FROM accounts LEFT JOIN users ON accounts.id = users.account_id WHERE users.status IN (1,2,3) GROUP BY accounts.id"
                            )
                            .success(function(accounts) {
                                cb(null, accounts);
                            })
                            .error(function(e) {
                                cb(e, null);
                            })
                    }
                    catch (e) {
                        cb(e, null);
                    }
                }

            }
        }
    );
};