// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('CampaignNote',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The note\'s id.',
                primaryKey: true,
                allowNull: false
            },

            account_id: {
                type: Sequelize.STRING(36),
                comment: 'The account that owns this note.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            created_by: {
                type: Sequelize.STRING(36),
                comment: 'The user that created this note.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            campaign_id: {
                type: Sequelize.STRING(36),
                comment: 'The campaign this note belongs to.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            note: {
                type: Sequelize.TEXT,
                comment: 'The note body.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [1, 5120]
                }
            },

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'campaign_notes',

            // -------------------------
            //   Instance Methods
            // -------------------------

            instanceMethods: {

            },

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

            }
        }
    );
};