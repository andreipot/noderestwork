// ----------------
//   Dependencies
// ----------------
var Sequelize   = require('sequelize');

module.exports = function(db, DataTypes) {
    return db.define('User',
        {
            // -------------------------
            //   Model Properties
            // -------------------------

            id: {
                type: Sequelize.STRING(36),
                comment: 'The user\'s id.',
                primaryKey: true,
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            account_id: {
                type: Sequelize.STRING(36),
                comment: 'The account that owns this user.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    len: [36, 36],
                    isUUID: 4
                }
            },

            email: {
                type: Sequelize.STRING(32),
                comment: 'User\'s email address.',
                allowNull: false,
                validate: {
                    notEmpty: true,
                    isEmail: true
                }
            },

            password: {
                type: Sequelize.STRING(64),
                comment: 'User\'s password hash.',
                allowNull: false,
                validate: {
                    len: [8, 64]
                }
            },

            fname: {
                type: Sequelize.STRING(32),
                comment: 'User\'s first name.',
                allowNull: false,
                validate: {
                    len: [2, 32]
                }
            },

            lname: {
                type: Sequelize.STRING(32),
                comment: 'User\'s last name.',
                allowNull: false,
                validate: {
                    len: [2, 32]
                }
            },

            last_login: {
                type: Sequelize.DATE,
                comment: 'Date/Time the user last logged in.',
                allowNull: true
            },

            type: {
                type: Sequelize.INTEGER,
                comment: 'User Type // 0: Admin, 1: Employee, 2: Contractor',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 2 }
            },

            is_super_admin: {
                type: Sequelize.BOOLEAN,
                comment: 'Super Admin // 0: No, 1: Yes',
                allowNull: false,
                defaultValue: 0
            },

            onboarding: {
                type: Sequelize.STRING(512),
                comment: 'JSON containing onboarding completion status.',
                allowNull: true,
                validate: {
                    len: [0, 512]
                }
            },

            status: {
                type: Sequelize.INTEGER,
                comment: 'User Status // 0: New (Unconfirmed), 1: Active, 2: Locked',
                allowNull: false,
                defaultValue: 0,
                validate: { min: 0, max: 2 }
            }

        }, {
            // -------------------------
            //   Model Configuration
            // -------------------------

            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: true,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are not disabled
            paranoid: false,

            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,

            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,

            // define the table's name
            tableName: 'users',

            // -------------------------
            //   Class Methods
            // -------------------------

            classMethods: {

                Status: {
                    DELETED: 0,
                    ACTIVE: 1,
                    PENDING: 2
                },

                Type: {
                    ADMIN: 0,
                    EMPLOYEE: 1,
                    CONTRACTOR: 2
                }

            }
        }
    );
};