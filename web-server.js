// ----------------
//   Dependencies
// ----------------
var Cluster                     = require('cluster');
var Express                     = require('express');
var http                        = require('http');
var path                        = require('path');
var Config                      = require('./config.json');
var Assets                      = require('./assets.json');
var fs                          = require('fs');
var gzippo                      = require('gzippo');
var RedisStore                  = require('connect-redis')(Express);

// Utilities
var Flash                       = require('connect-flash');
var Utils                       = require('./utils/util').Util;
var Tokens                      = require('./utils/tokens').Tokens;

// Controllers
var MainController              = require('./routes/index');
var UserController              = require('./routes/users');
var TasksController             = require('./routes/tasks');
var CompaniesController         = require('./routes/companies');
var AccountController           = require('./routes/accounts');
var BillingController           = require('./routes/billing');
var CampaignsController         = require('./routes/campaigns');

// --------------------------
//   Server Settings & Init
// --------------------------

var Settings = {
    cpu_cores: require('os').cpus().length,
    port:      Config.web_server_port,
    debug_on:  true
}

var app = Express();

app.configure(function() {
    app.set('port', process.env.PORT || Settings.port);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    //app.use(require('less-middleware')({ src: __dirname + '/public' }));
    app.use(Express.favicon());
    app.use(Express.logger('dev'));
    app.use(Express.bodyParser());
    app.use(Express.methodOverride());
    app.use(Express.cookieParser('SVTjhsekwebksefo8wehof'));
    app.use(Express.session({
        store: new RedisStore({
            host: Config.redis_host,
            port: Config.redis_port
        }),
        secret: 'TDG8h023nofweSDFSD23rfas'
    }));
    app.use(Flash());
    app.use(app.router);
    //app.use(Express.static(path.join(__dirname, 'public')));
    app.use(gzippo.staticGzip(__dirname + '/public'));
    app.use(gzippo.compress());
});

app.configure('development', function() {
    app.use(Express.errorHandler());
});

// ----------------
//   Web Routing
// ----------------

app.get('*', function(req, res, next) {
    if (req.headers['x-forwarded-for'] != '99.229.103.183') {
        res.redirect('http://www.google.com');
        return next(false);
    }
    if (req.headers['x-forwarded-proto'] != Config.protocol) {
        res.redirect('https://' + req.headers.host + req.url);
        return next(false);
    }

    // Continue on and process request
    return next();
});

// Main
app.get('/',                                   MainController.index);
app.get('/404',                                MainController.not_found);
app.get('/login',                              MainController.login);
app.post('/login',                             MainController.process_login);
app.get('/logout',                             MainController.logout);
app.get('/users',                   CheckAuth, UserController.manage);
app.get('/clients',                 CheckAuth, CompaniesController.manage);
app.get('/tasks',                   CheckAuth, TasksController.manage);
app.get('/accounts',                CheckAuth, AccountController.manage);
app.get('/campaigns',               CheckAuth, CampaignsController.view);
app.get('/billing',                 CheckAuth, BillingController.manage);
app.get('/password',                CheckAuth, UserController.change_pwd);

app.use(function(req, res){
    res.redirect('/404');
});

// ------------------------
//   Middleware Helpers
// ------------------------

function CheckAuth(req, res, next) {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    if (!req.session.user_id || !req.session.token) {
        res.redirect('/login');
    } else {
        Tokens.Authenticate(req.session.token, function(token) {
            if (token.is_valid) {
                req.session.user = token.user;
                next();
            } else {
                User.find({ where: { id: req.session.user_id } })
                    .success(function(user) {
                        if (user) {
                            req.session.user_id = user.id;
                            req.session.token = Tokens.Issue(user);
                            req.session.account_id = user.account_id;
                            res.redirect('/campaigns');
                        } else {
                            res.redirect('/login');
                        }
                    })
                    .error(function(e, e2) {
                        Util.log('Token REFRESH ERROR');
                        Util.log(e, e2);
                        res.redirect('/login');
                    });
            }
        });
    }
}

// ------------------------
//   EJS Helpers
// ------------------------

app.locals.asset = function(file) {
    if (Config.use_cdn) {
        var options = Assets.cdn;
    } else {
        var options = ["/"];
    }

    return options[ Math.floor(Math.random() * options.length) ] + file + '?v=' + Assets.version;
}

app.locals.get_assets = function(file) {
    var assets = { version: Assets.version };
    if (Config.use_cdn) {
        assets.options = Assets.cdn;
    } else {
        assets.options = ["/"];
    }

    return JSON.stringify(assets);
}

app.locals.gravatar = function(email, size) {
    var hash = Utils.md5(email.toLowerCase());
    return 'https://www.gravatar.com/avatar/' + hash + '?d=mm&s=' + size;
}

// ------------------------
//   Start Server Cluster
// ------------------------

http.createServer(app).listen(app.get('port'), function() {
    console.log("Express listening on port " + app.get('port'));
});
