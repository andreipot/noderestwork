// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Util        = require('../utils/util').Util;

// Config
var Config      = require('../config.json');

// Models
var Models      = require('../models');
var TaskTemplate= Models.TaskTemplate;
var User        = Models.User;

// -------------------------
//   Controller Definition
// -------------------------

var TaskTemplatesController = function() {}

/**
 * Return TaskTemplate Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TaskTemplatesController.Get = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.template_id, 4],  'Path parameter :template_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.template_id],     'Path parameter :template_id may not be null.')
        .GetErrors();

    // Fetch and return
    if (!errors.length) {
        TaskTemplate.find({ where: { id: req.params.template_id, account_id: req.token.user.account_id, status: TaskTemplate.Status.ACTIVE }})
            .success(function(template) {
                if (template) {
                    return Client.Success(req, res, template);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('TaskTemplatesController.Get :: Error looking up template.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Create a template
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TaskTemplatesController.Create = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertFalse('isNull',  [req.params.title],                 'Body parameter :title may not be null.')
        .AssertTrue('isLength', [req.params.title, 1, 128],         'Body parameter :title must be between 1-128 characters.')
        .AssertFalse('isNull',  [req.params.description],           'Body parameter :description may not be null.')
        .AssertTrue('isLength', [req.params.description, 1, 5120], 'Body parameter :description must be between 1-5120 characters.')
        .GetErrors();

    if (!errors.length) {
        TaskTemplate.findOrCreate({
            title: req.params.title
        },{
            id: UUID.v4().toString(),
            title: req.params.title,
            description: req.params.description,
            account_id: req.token.user.account_id,
            status: TaskTemplate.Status.ACTIVE
        })
            .success(function(template, created) {
                if (!created) {
                    return Client.IllegalConflict(req, res, 'Duplicate Entity');
                }
                if (template) {
                    return Client.Success(req, res, template);
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('TaskTemplatesController.Create :: Error creating template.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a template
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TaskTemplatesController.Update = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    if (typeof req.params.title != "undefined") {
        validate.AssertFalse('isNull',  [req.params.title],                 'Body parameter :title may not be null.');
        validate.AssertTrue('isLength', [req.params.title, 1, 128],         'Body parameter :title must be between 1-128 characters.');
    }
    if (typeof req.params.description != "undefined") {
        validate.AssertFalse('isNull',  [req.params.description],           'Body parameter :description may not be null.')
        validate.AssertTrue('isLength', [req.params.description, 1, 5120],  'Body parameter :description must be between 1-5120 characters.')
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        TaskTemplate.find({ where: { id: req.params.template_id, account_id: req.token.user.account_id, status: TaskTemplate.Status.ACTIVE }})
            .success(function(template) {
                // Find entries
                if (template) {
                    if (typeof req.params.title != "undefined") {
                        template.title = req.params.title;
                    }
                    if (typeof req.params.description != "undefined") {
                        template.description = req.params.description;
                    }

                    template.save()
                        .success(function() {
                            return Client.Success(req, res, template);
                        })
                        .error(function(error) {
                            console.log('TaskTemplateController.Update :: Error updating template.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('TaskTemplatesController.Create :: Error updating template.');
                console.log(e);
                return Client.ServerError(req, res);
            })


    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return All TaskTemplate Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TaskTemplatesController.List = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    try {
        // Fetch and return
        TaskTemplate.findAll({ where: { account_id: req.token.user.account_id, status: TaskTemplate.Status.ACTIVE }})
            .success(function(templates) {
                if (templates) {
                    return Client.Success(req, res, templates);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('TaskTemplatesController.List :: Error looking up template.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    }
    catch(e) {
        console.log(e);
        console.log('TaskTemplatesController.List :: Unknown Error looking up template.');
        return Client.ServerError(req, res);
    }



}

/**
 * Mark a TaskTemplate as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TaskTemplatesController.Delete = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.template_id, 4],  'Path parameter :template_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.template_id],     'Path parameter :template_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        TaskTemplate.find({ where: { id: req.params.template_id, account_id: req.token.user.account_id, status: TaskTemplate.Status.ACTIVE }})
            .success(function(template) {
                // Find entries
                if (template) {
                    template.status = TaskTemplate.Status.DELETED;
                    template.save()
                        .success(function() {
                            return Client.Success(req, res, { message: 'TaskTemplate deleted.' });
                        })
                        .error(function(error) {
                            console.log('TaskTemplatesController.Delete :: Error deleting template.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('TaskTemplatesController.Delete :: Error looking up template.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.TaskTemplatesController = TaskTemplatesController;