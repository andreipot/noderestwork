// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;

// Config
var Config      = require('../config.json');

// Models
var Models      = require('../models');
var User        = Models.User;
var Company     = Models.Company;
var Campaign    = Models.Campaign;

// -------------------------
//   Controller Definition
// -------------------------

var CompaniesController = function() {}

/**
 * Return Company Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CompaniesController.Get = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.company_id, 4],  'Path parameter :company_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.company_id],     'Path parameter :company_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        Company.find({ where: { id: req.params.company_id, account_id: req.token.user.account_id, status: [Company.Status.ACTIVE, Company.Status.PAUSED] }})
            .success(function(company) {
                // Find entries
                if (company) {
                    return Client.Success(req, res, company);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CompaniesController.Get :: Error looking up company.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return User's Companies
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CompaniesController.List = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    Company.findAll({ where: { account_id: req.token.user.account_id, status: [Company.Status.ACTIVE, Company.Status.PAUSED] }})
        .success(function(companies) {
            if (companies && companies.length && companies.length >= 0) {
                return Client.Success(req, res, companies);
            } else {
                return Client.NoContent(req, res);
            }
        })
        .error(function(e, e2) {
            console.log('CompaniesController.List :: Error looking up companies.');
            console.log(e, e2);
            return Client.ServerError(req, res);
        })

}

/**
 * Create a company
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CompaniesController.Create = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isLength', [req.params.name, 1, 128],   'Body parameter :name must be between 1-128 characters.')
        .AssertFalse('isNull',  [req.params.name],           'Body parameter :name may not be null.')
        .AssertTrue('isURL',    [req.params.website],        'Body parameter :website must be a valid URL.')
        .AssertFalse('isNull',  [req.params.website],        'Body parameter :website may not be null.')
        .AssertTrue('isURL',    [req.params.logo],           'Body parameter :logo must be a valid URL.')
        .AssertFalse('isNull',  [req.params.logo],           'Body parameter :logo may not be null.')
        .AssertTrue('isLength', [req.params.logo, 1, 256],   'Body parameter :logo must be between 1-256 characters.')
        .AssertTrue('isLength', [req.params.contact, 1, 128],'Body parameter :contact must be between 1-128 characters.')
        .AssertFalse('isNull',  [req.params.contact],        'Body parameter :contact may not be null.')
        .AssertTrue('isInt',    [req.params.phone],          'Body parameter :phone must be a valid integer.')
        .AssertFalse('isNull',  [req.params.phone],          'Body parameter :phone may not be null.')
        .AssertTrue('isEmail',  [req.params.email],          'Body parameter :email must be a valid email.')
        .AssertFalse('isNull',  [req.params.email],          'Body parameter :email may not be null.')
        .AssertTrue('isIn',     [req.params.do_reporting, [true, false]], 'Body parameter :do_reporting must be a boolean value.')
        .AssertFalse('isNull',  [req.params.do_reporting],   'Body parameter :do_reporting may not be null.')
        .GetErrors();

    if (!errors.length) {
        Company.findOrCreate({
            id: UUID.v4().toString(),
            name: req.params.name,
            website: req.params.website,
            contact: req.params.contact,
            account_id: req.token.user.account_id,
            phone: req.params.phone,
            email: req.params.email,
            do_reporting: req.params.do_reporting,
            logo: req.params.logo,
            status: Company.Status.ACTIVE
        })
            .success(function(company, created) {
                if (!created) {
                    return Client.IllegalConflict(req, res, 'Duplicate Entity');
                }
                if (company) {
                    return Client.Success(req, res, company);
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('CompaniesController.Create :: Error creating company.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a company
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CompaniesController.Update = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    if (typeof req.params.name != "undefined") {
        validate.AssertTrue('isLength', [req.params.name, 1, 128],   'Body parameter :name must be between 1-128 characters.')
        validate.AssertFalse('isNull',  [req.params.name],           'Body parameter :name may not be null.')
    }
    if (typeof req.params.website != "undefined") {
        validate.AssertTrue('isURL',    [req.params.website],        'Body parameter :website must be a valid URL.')
        validate.AssertFalse('isNull',  [req.params.website],        'Body parameter :website may not be null.')
    }
    if (typeof req.params.logo != "undefined") {
        validate.AssertTrue('isURL',    [req.params.logo],           'Body parameter :logo must be a valid URL.')
        validate.AssertFalse('isNull',  [req.params.logo],           'Body parameter :logo may not be null.')
    }
    if (typeof req.params.contact != "undefined") {
        validate.AssertTrue('isLength', [req.params.contact, 1, 128],'Body parameter :contact must be between 1-128 characters.')
        validate.AssertFalse('isNull',  [req.params.contact],        'Body parameter :contact may not be null.')
    }
    if (typeof req.params.phone != "undefined") {
        validate.AssertTrue('isInt',    [req.params.phone],          'Body parameter :phone must be a valid integer.')
        validate.AssertFalse('isNull',  [req.params.phone],          'Body parameter :phone may not be null.')
    }
    if (typeof req.params.email != "undefined") {
        validate.AssertTrue('isEmail',  [req.params.email],          'Body parameter :email must be a valid email.')
        validate.AssertFalse('isNull',  [req.params.email],          'Body parameter :email may not be null.')
    }
    if (typeof req.params.do_reporting != "undefined") {
        validate.AssertTrue('isIn',     [req.params.do_reporting, [true, false]], 'Body parameter :do_reporting must be a boolean value.')
        validate.AssertFalse('isNull',  [req.params.do_reporting],   'Body parameter :do_reporting may not be null.')
    }
    if (typeof req.params.status != "undefined") {
        validate.AssertFalse('isNull',  [req.params.status], 'Body parameter :status may not be null.');
        validate.AssertTrue('isIn',  [req.params.status, [Company.Status.ACTIVE, Company.Status.PAUSED]], 'Body parameter :status is not valid.');
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        Company.find({ where: { id: req.params.company_id, account_id: req.token.user.account_id, status: [Company.Status.ACTIVE, Company.Status.PAUSED] }})
            .success(function(company) {
                // Find entries
                if (company) {
                    if (typeof req.params.name != "undefined") {
                        company.name = req.params.name;
                        console.log('Name set to', company.name);
                    }
                    if (typeof req.params.website != "undefined") {
                        company.website = req.params.website;
                        console.log('website set to', company.website);
                    }
                    if (typeof req.params.logo != "undefined") {
                        company.logo = req.params.logo;
                        console.log('logo set to', company.logo);
                    }
                    if (typeof req.params.contact != "undefined") {
                        company.contact = req.params.contact;
                        console.log('contact set to', company.contact);
                    }
                    if (typeof req.params.phone != "undefined") {
                        company.phone = req.params.phone;
                        console.log('phone set to', company.phone);
                    }
                    if (typeof req.params.email != "undefined") {
                        company.email = req.params.email;
                        console.log('email set to', company.email);
                    }
                    if (typeof req.params.do_reporting != "undefined") {
                        company.do_reporting = req.params.do_reporting;
                        console.log('do_reporting set to', company.do_reporting);
                    }
                    if (typeof req.params.status != "undefined" /*&& req.token.user.type == User.Type.ADMIN*/) {
                        company.status = req.params.status;
                    }

                    company.save()
                        .success(function() {
                            return Client.Success(req, res, company);
                        })
                        .error(function(error) {
                            console.log('CompaniesController.Update :: Error deleting company.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CompaniesController.Update :: Error looking up company.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Mark a Company as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CompaniesController.Delete = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.company_id, 4],  'Path parameter :company_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.company_id],     'Path parameter :company_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        Company.find({ where: { id: req.params.company_id, account_id: req.token.user.account_id, status: [Company.Status.ACTIVE, Company.Status.PAUSED] }})
            .success(function(company) {
                // Find entries
                if (company) {
                    company.status = Company.Status.DELETED;
                    company.save()
                        .success(function() {

                            Campaign.update(
                                { status: Campaign.Status.DELETED },
                                { company_id: req.params.company_id, account_id: req.token.user.account_id })
                                .success(function() {
                                    return Client.Success(req, res, { message: 'Company deleted.' });
                                })
                                .error(function(error) {
                                    console.log('CompaniesController.Delete :: Error cascading changes to campaigns.');
                                    console.log(error);
                                    return Client.ServerError(req, res);
                                });


                        })
                        .error(function(error) {
                            console.log('CompaniesController.Delete :: Error deleting company.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    console.log('no company found');
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CompaniesController.Delete :: Error looking up company.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.CompaniesController = CompaniesController;