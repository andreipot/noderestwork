// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Util        = require('../utils/util').Util;

// Config
var Config      = require('../config.json');

// Models
var Models          = require('../models');
var MonthTemplate    = Models.MonthTemplate;
var MonthTemplate   = Models.MonthTemplate;
var User            = Models.User;

// -------------------------
//   Controller Definition
// -------------------------

var MonthTemplatesController = function() {}

/**
 * Return MonthTemplate Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
MonthTemplatesController.Get = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.set_id, 4],  'Path parameter :set_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.set_id],     'Path parameter :set_id may not be null.')
        .GetErrors();

    // Fetch and return
    if (!errors.length) {
        MonthTemplate.find({ where: { id: req.params.set_id, account_id: req.token.user.account_id, status: MonthTemplate.Status.ACTIVE }})
            .success(function(template) {
                if (template) {
                    return Client.Success(req, res, template);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('MonthTemplatesController.Get :: Error looking up template.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Create a template
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
MonthTemplatesController.Create = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    console.log(req.params.tasks);

    if (req.params.tasks && req.params.tasks.length) {
        var tasks = [];

        for (var i in req.params.tasks) {
            if (req.params.tasks[i].title && typeof req.params.tasks[i].description != undefined && typeof req.params.tasks[i].assigned_to) {
                tasks.push({
                    title: req.params.tasks[i].title,
                    description: req.params.tasks[i].description,
                    assigned_to: req.params.tasks[i].assigned_to
                });
            } else {
                return Client.InvalidRequest(req, res, 'Body parameter :tasks contains misformatted data.');
            }
        }


        req.params.tasks = JSON.stringify(req.params.tasks);
    }

    // Validate
    var errors = new Validation()
        .AssertFalse('isNull',  [req.params.name],            'Body parameter :name may not be null.')
        .AssertTrue('isLength', [req.params.name, 1, 128],    'Body parameter :name must be between 1-128 characters.')
        .AssertFalse('isNull',  [req.params.tasks],           'Body parameter :tasks may not be null.')
        .AssertTrue('isLength', [req.params.tasks, 0, 65535], 'Body parameter :tasks must be between 0-65535 characters.')
        .GetErrors();

    if (!errors.length) {
        MonthTemplate.findOrCreate({
            title: req.params.name
        },{
            id: UUID.v4().toString(),
            name: req.params.name,
            tasks: req.params.tasks,
            account_id: req.token.user.account_id,
            status: MonthTemplate.Status.ACTIVE
        })
            .success(function(template, created) {
                if (!created) {
                    return Client.IllegalConflict(req, res, 'Duplicate Entity');
                }
                if (template) {
                    return Client.Success(req, res, template);
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('MonthTemplatesController.Create :: Error creating template.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a template
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
MonthTemplatesController.Update = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    if (typeof req.params.set_id != "undefined") {
        validate.AssertFalse('isNull',  [req.params.set_id],               'Path parameter :set_id may not be null.');
        validate.AssertTrue('isUUID',   [req.params.set_id, 4],            'Path parameter :set_id must be between 1-128 characters.');
    }
    if (typeof req.params.title != "undefined") {
        validate.AssertFalse('isNull',  [req.params.name],                 'Body parameter :name may not be null.');
        validate.AssertTrue('isLength', [req.params.name, 1, 128],         'Body parameter :name must be between 1-128 characters.');
    }
    if (typeof req.params.tasks != "undefined") {
        req.params.tasks = JSON.stringify(req.params.tasks);
        validate.AssertFalse('isNull',  [req.params.tasks],                'Body parameter :tasks may not be null.')
        validate.AssertTrue('isLength', [req.params.tasks, 0, 65535],      'Body parameter :tasks must be between 0-65535 characters.')
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        MonthTemplate.find({ where: { id: req.params.set_id, account_id: req.token.user.account_id, status: MonthTemplate.Status.ACTIVE }})
            .success(function(template) {
                // Find entries
                if (template) {
                    if (typeof req.params.name != "undefined") {
                        template.name = req.params.name;
                    }
                    if (typeof req.params.tasks != "undefined") {
                        template.tasks = req.params.tasks;
                    }

                    template.save()
                        .success(function() {
                            return Client.Success(req, res, template);
                        })
                        .error(function(error) {
                            console.log('MonthTemplateController.Update :: Error updating template.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('MonthTemplatesController.Create :: Error updating template.');
                console.log(e);
                return Client.ServerError(req, res);
            })


    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return All MonthTemplate Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
MonthTemplatesController.List = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    try {
        // Fetch and return
        MonthTemplate.findAll({ where: { account_id: req.token.user.account_id, status: MonthTemplate.Status.ACTIVE }})
            .success(function(templates) {
                if (templates) {

                    for (var i in templates) {
                        templates[i].tasks = JSON.parse(templates[i].tasks);
                    }

                    return Client.Success(req, res, templates);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('MonthTemplatesController.List :: Error looking up template.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    }
    catch(e) {
        console.log(e);
        console.log('MonthTemplatesController.List :: Unknown Error looking up template.');
        return Client.ServerError(req, res);
    }



}

/**
 * Mark a MonthTemplate as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
MonthTemplatesController.Delete = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.set_id, 4],  'Path parameter :set_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.set_id],     'Path parameter :set_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        MonthTemplate.find({ where: { id: req.params.set_id, account_id: req.token.user.account_id, status: MonthTemplate.Status.ACTIVE }})
            .success(function(template) {
                // Find entries
                if (template) {
                    template.status = MonthTemplate.Status.DELETED;
                    template.save()
                        .success(function() {
                            return Client.Success(req, res, { message: 'MonthTemplate deleted.' });
                        })
                        .error(function(error) {
                            console.log('MonthTemplatesController.Delete :: Error deleting template.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('MonthTemplatesController.Delete :: Error looking up template.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.MonthTemplatesController = MonthTemplatesController;