// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Util        = require('../utils/util').Util;

// Config
var Config      = require('../config.json');

// Models
var Models      = require('../models');
var Account     = Models.Account;
var User        = Models.User;

// -------------------------
//   Controller Definition
// -------------------------

var AccountsController = function() {}

/**
 * Return Account Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
AccountsController.Get = function (req, res, body) {
    // Security
    if (!req.token.user || !req.token.user.is_super_admin) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.account_id, 4],  'Path parameter :account_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.account_id],     'Path parameter :account_id may not be null.')
        .GetErrors();

    // Fetch and return
    if (!errors.length) {
        Account.find({ where: { id: req.params.account_id, status: [Account.Status.ACTIVE, Account.Status.PENDING] }})
            .success(function(account) {
                if (account) {
                    return Client.Success(req, res, account);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('AccountsController.Get :: Error looking up account.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Create a account
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
AccountsController.Create = function (req, res, body) {
    // Security
    if (!req.token.user || !req.token.user.is_super_admin) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertFalse('isNull',  [req.params.billing_fname],        'Body parameter :billing_fname may not be null.')
        .AssertFalse('isNull',  [req.params.billing_lname],        'Body parameter :billing_lname may not be null.')
        .AssertTrue('isEmail',  [req.params.billing_email],        'Body parameter :billing_email must be a valid email.')
        .AssertFalse('isNull',  [req.params.billing_email],        'Body parameter :billing_email may not be null.')
        .AssertFalse('isNull',  [req.params.billing_company],      'Body parameter :billing_company may not be null.')
        /*.AssertFalse('isNull',  [req.params.plan_id], 'Body parameter :plan_id may not be null.')
        .AssertTrue('isIn',  [req.params.plan_id, [0]], 'Body parameter :plan_id is not valid.')*/
        .GetErrors();

    if (!errors.length) {
        Account.findOrCreate({
            billing_email: req.params.billing_email
        },{
            id: UUID.v4().toString(),
            billing_fname: req.params.billing_fname,
            billing_lname: req.params.billing_lname,
            billing_company: req.params.billing_company,
            plan_id: 0,
            status: Account.Status.ACTIVE,
            billing_email: req.params.billing_email
        })
            .success(function(account, created) {
                if (!created) {
                    return Client.IllegalConflict(req, res, 'Duplicate Entity');
                }
                if (account) {
                    return Client.Success(req, res, account);
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('AccountsController.Create :: Error creating account.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a account
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
AccountsController.Update = function (req, res, body) {
    // Security
    if (!req.token.user || !req.token.user.is_super_admin) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    if (typeof req.params.billing_fname != "undefined") {
        validate.AssertFalse('isNull',  [req.params.billing_fname], 'Body parameter :billing_fname may not be null.');
    }
    if (typeof req.params.billing_lname != "undefined") {
        validate.AssertFalse('isNull',  [req.params.billing_lname], 'Body parameter :billing_lname may not be null.');
    }
    if (typeof req.params.billing_company != "undefined") {
        validate.AssertFalse('isNull',  [req.params.billing_company], 'Body parameter :billing_company may not be null.');
    }
    if (typeof req.params.billing_email != "undefined") {
        validate.AssertFalse('isNull',  [req.params.billing_email], 'Body parameter :billing_email may not be null.');
        validate.AssertTrue('isEmail',  [req.params.billing_email], 'Body parameter :billing_email must be a valid email.');
    }
    if (typeof req.params.plan_id != "undefined") {
        validate.AssertFalse('isNull',  [req.params.plan_id], 'Body parameter :plan_id may not be null.');
        validate.AssertTrue('isIn',  [req.params.plan_id, [0]], 'Body parameter :plan_id is not valid.');
    }
    if (typeof req.params.status != "undefined") {
        validate.AssertFalse('isNull',  [req.params.status], 'Body parameter :status may not be null.');
        validate.AssertTrue('isIn',  [req.params.status, [Account.Status.ACTIVE, Account.Status.PENDING]], 'Body parameter :status is not valid.');
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        Account.find({ where: { id: req.params.account_id, status: [Account.Status.ACTIVE, Account.Status.PENDING] }})
            .success(function(account) {
                // Find entries
                if (account) {
                    if (typeof req.params.billing_fname != "undefined") {
                        account.billing_fname = req.params.billing_fname;
                    }
                    if (typeof req.params.billing_lname != "undefined") {
                        account.billing_lname = req.params.billing_lname;
                    }
                    if (typeof req.params.billing_company != "undefined") {
                        account.billing_company = req.params.billing_company;
                    }
                    if (typeof req.params.billing_email != "undefined" /*&& req.token.user.type == Account.Type.ADMIN*/) {
                        account.billing_email = req.params.billing_email;
                    }
                    if (typeof req.params.plan_id != "undefined") {
                        account.plan_id = req.params.plan_id;
                    }
                    if (typeof req.params.status != "undefined" /*&& req.token.user.type == Account.Type.ADMIN*/) {
                        account.status = req.params.status;
                    }

                    account.save()
                        .success(function() {
                            return Client.Success(req, res, account);
                        })
                        .error(function(error) {
                            console.log('AccountsController.Update :: Error deleting account.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('AccountsController.Update :: Error looking up account.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return All Account Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
AccountsController.List = function (req, res, body) {
    console.log(req.token.user);
    // Security
    if (!req.token.user || !req.token.user.is_super_admin) {
        return Client.NotAuthorized(req, res);
    }

    // Fetch and return
    Account.findAllExtra(function(e, accounts) {
        if (e) {
            console.log('AccountsController.Get :: Error looking up account.');
            console.log(e, e2);
            return Client.ServerError(req, res);
        } else {
            if (accounts) {
                return Client.Success(req, res, accounts);
            } else {
                return Client.NotFound(req, res);
            }
        }
    });
}

/**
 * Mark a Account as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
AccountsController.Delete = function (req, res, body) {
    // Security
    if (!req.token.user || !req.token.user.is_super_admin) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.account_id, 4],  'Path parameter :account_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.account_id],     'Path parameter :account_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        Account.find({ where: { id: req.params.account_id, status: [Account.Status.ACTIVE, Account.Status.PENDING] }})
            .success(function(account) {
                // Find entries
                if (account) {
                    account.status = Account.Status.DELETED;
                    account.save()
                        .success(function() {
                            return Client.Success(req, res, { message: 'Account deleted.' });
                        })
                        .error(function(error) {
                            console.log('AccountsController.Delete :: Error deleting account.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('AccountsController.Delete :: Error looking up account.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.AccountsController = AccountsController;