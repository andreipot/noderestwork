// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Async       = require('async');

// Config
var Config      = require('../config.json');

// Models
var Models      = require('../models');
var User        = Models.User;
var Campaign    = Models.Campaign;
var Task        = Models.Task;

// -------------------------
//   Controller Definition
// -------------------------

var CampaignsController = function() {}

/**
 * Return Campaign Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignsController.Get = function(req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.campaign_id, 4],  'Path parameter :campaign_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.campaign_id],     'Path parameter :campaign_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        Campaign.find({ where: { id: req.params.campaign_id, account_id: req.token.user.account_id, status: [Campaign.Status.ACTIVE, Campaign.Status.PAUSED] }})
            .success(function(campaign) {
                // Find entries
                if (campaign) {
                    campaign.getTasks()
                        .success(function(tasks) {
                            campaign.dataValues.tasks = tasks;

                            return Client.Success(req, res, campaign);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignsController.Get :: Error looking up campaign.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return User's Campaigns
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignsController.List = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    Campaign.findAll({ where: { account_id: req.token.user.account_id, status: [Campaign.Status.ACTIVE, Campaign.Status.PAUSED] }})
        .success(function(campaigns) {
            if (campaigns && campaigns.length && campaigns.length >= 0) {
                Async.forEach(campaigns, function(camp, done) {
                    Async.parallel({
                        tasks: function(cb) {
                            camp.getTasks({ where: { status: [Task.Status.ACTIVE, Task.Status.IN_PROGRESS, Task.Status.COMPLETED] }})
                                .success(function(tasks) {
                                    camp.dataValues.tasks = tasks;
                                    return cb();
                                });
                        },
                        notes: function(cb) {
                            camp.getNotesForUser(req.token.user.id, function(e, notes) {
                                camp.dataValues.notes = notes || [];
                                return cb(e);
                            });
                        }
                    }, function(err, results) {
                        done(err);
                    });
                }, function(err) {
                    if (err) {
                        console.log('CampaignsController.List :: Error looking up campaigns tasks');
                        console.log(err);
                        return Client.ServerError(req, res);
                    }
                    return Client.Success(req, res, campaigns);
                })
            } else {
                return Client.NoContent(req, res);
            }
        })
        .error(function(e) {
            console.log('CampaignsController.List :: Error looking up campaigns.');
            console.log(e);
            return Client.ServerError(req, res);
        })

}

/**
 * Create a campaign
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignsController.Create = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    if (!req.params.name) {
        req.params.name = '';
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isLength', [req.params.name, 0, 128],   'Body parameter :name must be between 1-128 characters.')
        .AssertTrue('isLength', [req.params.override_email, 0, 128],   'Body parameter :override_email must be between 1-128 characters.')
        .AssertTrue('isLength', [req.params.override_contact, 0, 64],   'Body parameter :override_contact must be between 1-64 characters.')

        .AssertTrue('isUUID',   [req.params.company_id, 4],  'Body parameter :company_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.company_id],     'Body parameter :company_id may not be null.')
        .AssertTrue('isInt',    [req.params.start_at],       'Body parameter :start_at must be a valid integer timestamp.')
        .AssertFalse('isNull',  [req.params.start_at],       'Body parameter :start_at may not be null.')
        .AssertTrue('isInt',    [req.params.stop_at],        'Body parameter :stop_at must be a valid integer timestamp.')
        .AssertFalse('isNull',  [req.params.stop_at],        'Body parameter :stop_at may not be null.');

        if (req.params.override_phone) {
            errors.AssertTrue('isInt',    [req.params.override_phone],          'Body parameter :override_phone must be a valid integer.')
        }

        errors = errors.GetErrors();

    if (!errors.length) {
        Campaign.findOrCreate({
            id: UUID.v4().toString(),
            company_id: req.params.company_id,
            account_id: req.token.user.account_id,
            start_at: req.params.start_at,
            stop_at: req.params.stop_at,
            status: Campaign.Status.ACTIVE,
            name: req.params.name
        })
            .success(function(campaign, created) {
                if (!created) {
                    return Client.IllegalConflict(req, res, 'Duplicate Entity');
                }
                if (campaign) {
                    return Client.Success(req, res, campaign);
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignsController.Create :: Error creating campaign.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a campaign
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignsController.Update = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    if (typeof req.params.name != "undefined") {
        validate.AssertTrue('isLength', [req.params.name, 0, 128],   'Body parameter :name must be between 0-128 characters.');
    }
    if (typeof req.params.company_id != "undefined") {
        validate.AssertTrue('isUUID',   [req.params.company_id, 4], 'Body parameter :company_id must be a valid UUID.');
        validate.AssertFalse('isNull',  [req.params.company_id],    'Body parameter :company_id may not be null.')
    }
    if (typeof req.params.start_at != "undefined") {
        validate.AssertFalse('isNull',  [req.params.start_at], 'Body parameter :start_at may not be null.');
        validate.AssertTrue('isInt',    [req.params.start_at], 'Body parameter :start_at must be a valid integer timestamp.');
    }
    if (typeof req.params.stop_at != "undefined") {
        validate.AssertFalse('isNull',  [req.params.stop_at],  'Body parameter :stop_at may not be null.');
        validate.AssertTrue('isInt',    [req.params.stop_at],  'Body parameter :stop_at must be a valid integer timestamp.');
    }
    if (typeof req.params.status != "undefined") {
        validate.AssertFalse('isNull',  [req.params.status], 'Body parameter :status may not be null.');
        validate.AssertTrue('isIn',  [req.params.status, [Campaign.Status.ACTIVE, Campaign.Status.PAUSED]], 'Body parameter :status is not valid.');
    }
    if (typeof req.params.override_contact != "undefined") {
        validate.AssertTrue('isLength', [req.params.override_contact, 0, 64],   'Body parameter :override_contact must be between 0-64 characters.');
    }
    if (typeof req.params.override_email != "undefined") {
        validate.AssertTrue('isLength', [req.params.override_email, 0, 128],   'Body parameter :override_email must be between 0-128 characters.');
    }
    if (typeof req.params.override_phone != "undefined" && req.params.override_phone) {
        validate.AssertTrue('isInt',    [req.params.override_phone], 'Body parameter :override_phone must be a valid integer timestamp.');
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        Campaign.find({ where: { id: req.params.campaign_id, account_id: req.token.user.account_id, status: [Campaign.Status.ACTIVE, Campaign.Status.PAUSED] }})
            .success(function(campaign) {
                // Find entries
                if (campaign) {
                    if (typeof req.params.name != "undefined") {
                        campaign.name = req.params.name;
                    }
                    if (typeof req.params.company_id != "undefined") {
                        campaign.company_id = req.params.company_id;
                    }
                    if (typeof req.params.start_at != "undefined") {
                        campaign.start_at = req.params.start_at;
                    }
                    if (typeof req.params.stop_at != "undefined") {
                        campaign.stop_at = req.params.stop_at;
                    }
                    if (typeof req.params.status != "undefined" /*&& req.token.user.type == User.Type.ADMIN*/) {
                        campaign.status = req.params.status;
                    }
                    if (typeof req.params.override_contact != "undefined") {
                        campaign.override_contact = req.params.override_contact;
                    }
                    if (typeof req.params.override_email != "undefined") {
                        campaign.override_email = req.params.override_email;
                    }
                    if (typeof req.params.override_phone != "undefined") {
                        campaign.override_phone = req.params.override_phone;
                    }

                    campaign.save()
                        .success(function() {
                            return Client.Success(req, res, campaign);
                        })
                        .error(function(error) {
                            console.log('CampaignsController.Update :: Error deleting campaign.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignsController.Update :: Error looking up campaign.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Mark a Campaign as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignsController.Delete = function (req, res, body) {
    // Security
    if (req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.campaign_id, 4],  'Path parameter :campaign_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.campaign_id],     'Path parameter :campaign_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        Campaign.find({ where: { id: req.params.campaign_id, account_id: req.token.user.account_id, status: [Campaign.Status.ACTIVE, Campaign.Status.PAUSED] }})
            .success(function(campaign) {
                // Find entries
                if (campaign) {
                    campaign.status = Campaign.Status.DELETED;
                    campaign.save()
                        .success(function() {
                            return Client.Success(req, res, { message: 'Campaign deleted.' });
                        })
                        .error(function(error) {
                            console.log('CampaignsController.Delete :: Error deleting campaign.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignsController.Delete :: Error looking up campaign.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.CampaignsController = CampaignsController;