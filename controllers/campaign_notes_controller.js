// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Util        = require('../utils/util').Util;
var Redis       = require('redis');

// Config
var Config      = require('../config.json');

// Redis Clients
var Publisher   = Redis.createClient(Config.redis_port, Config.redis_host);

// Models
var Models              = require('../models');
var CampaignNote        = Models.CampaignNote;
var CampaignNoteView    = Models.CampaignNoteView;
var User                = Models.User;

// -------------------------
//   Controller Definition
// -------------------------

var CampaignNotesController = function() {}

/**
 * Return Campaign Note Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignNotesController.Get = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.note_id, 4],      'Path parameter :note_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.note_id],         'Path parameter :note_id may not be null.')
        .AssertTrue('isUUID',   [req.params.campaign_id, 4],  'Path parameter :campaign_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.campaign_id],     'Path parameter :campaign_id may not be null.')
        .GetErrors();

    // Fetch and return
    if (!errors.length) {
        CampaignNote.find({ where: { account_id: req.token.user.account_id, campaign_id: req.params.campaign_id, id: req.params.note_id }})
            .success(function(note) {
                if (note) {
                    return Client.Success(req, res, note);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignNotesController.Get :: Error looking up note.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Create a Campaign Note
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignNotesController.Create = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.campaign_id, 4],  'Path parameter :campaign_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.campaign_id],     'Path parameter :campaign_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        CampaignNote.create({
            id: UUID.v4().toString(),
            account_id: req.token.user.account_id,
            campaign_id: req.params.campaign_id,
            created_by: req.token.user.id,
            note: req.params.note
        })
            .success(function(note) {
                console.log(note);
                if (note) {
                    CampaignNoteView.findOrCreate({
                        user_id: req.token.user.id,
                        campaign_note_id: note.id
                    })
                        .success(function(cview) {
                            note.dataValues.viewed = cview.created_at;
                            Publisher.publish('note_activity_' + req.token.user.account_id, JSON.stringify(note));
                            return Client.Success(req, res, note);
                        })
                        .error(function(err) {
                            console.log('CampaignNotesController.Create :: Error marking newly created note as viewed by creator.');
                            console.log(err);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignNotesController.Create :: Error creating note.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a Campaign Note
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignNotesController.Update = function (req, res, body) {
    // Security
    if (!req.token.user || !req.token.user.is_super_admin) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    validate.AssertTrue('isUUID',   [req.params.note_id, 4],      'Path parameter :note_id must be a valid UUID.');
    validate.AssertFalse('isNull',  [req.params.note_id],         'Path parameter :note_id may not be null.');
    validate.AssertTrue('isUUID',   [req.params.campaign_id, 4],  'Path parameter :campaign_id must be a valid UUID.');
    validate.AssertFalse('isNull',  [req.params.campaign_id],     'Path parameter :campaign_id may not be null.');
    var errors = validate.GetErrors();

    if (!errors.length) {
        CampaignNote.find({ where: { account_id: req.token.user.account_id, campaign_id: req.params.campaign_id, id: req.params.note_id }})
            .success(function(note) {
                // Find entries
                if (note) {
                    if (typeof req.params.note != "undefined") {
                        note.note = req.params.note;
                    }

                    note.save()
                        .success(function() {
                            Publisher.publish('note_activity_' + req.token.user.account_id, JSON.stringify(note));
                            return Client.Success(req, res, note);
                        })
                        .error(function(error) {
                            console.log('CampaignNotesController.Update :: Error updating note.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignNotesController.Update :: Error looking up note.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return All Campaign Notes Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignNotesController.List = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.campaign_id, 4],  'Path parameter :campaign_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.campaign_id],     'Path parameter :campaign_id may not be null.')
        .GetErrors();

    // Fetch and return
    if (!errors.length) {
        CampaignNote.findAll({ where: { account_id: req.token.user.account_id, campaign_id: req.params.campaign_id }})
            .success(function(notes) {
                if (notes) {
                    return Client.Success(req, res, notes);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignNotesController.List :: Error looking up notes.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Mark a Campaign Note as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignNotesController.Delete = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.note_id, 4],  'Path parameter :note_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.note_id],     'Path parameter :note_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        CampaignNote.find({ where: { id: req.params.note_id, account_id: req.token.user.account_id }})
            .success(function(note) {
                // Find entries
                if (note) {
                    note.destroy()
                        .success(function() {
                            return Client.Success(req, res, { message: 'Note deleted.' });
                        })
                        .error(function(error) {
                            console.log('CampaignNotesController.Delete :: Error deleting note.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('CampaignNotesController.Delete :: Error looking up note.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Mark a set of Campaign Notes as viewed
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
CampaignNotesController.View = function (req, res, body) {
    console.log('CampaignNotesController.View', req.params);
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();

    if (req.params.note_ids && req.params.note_ids.length) {
        for (var i in req.params.note_ids) {
            if (typeof req.params.billing_fname != "undefined") {
                validate.AssertFalse('isUUID', [req.params.note_ids[i], 4], 'Body parameter :note_ids may only contain valid UUIDs.');
            }
        }
    } else {
        validate.AssertFalse('isNull',  ['not-null'], 'Body parameter :note_ids must be a valid array of UUIDs.');
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        Async.forEach(req.params.note_ids, function(note_id, cb) {
            CampaignNoteView.findOrCreate({
                user_id: req.token.user.id,
                campaign_note_id: note_id
            })
                .success(function(cview) {
                    cb(null, true);
                })
                .error(function(err) {
                    cb(err, false);
                });
        },
        function(errors) {
            if (errors) {
                console.log('CampaignNotesController.View :: Error marking notes as viewed.');
                console.log(errors);
                return Client.ServerError(req, res);
            }
            return Client.Success(req, res, { message: 'viewed' });
        });
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.CampaignNotesController = CampaignNotesController;