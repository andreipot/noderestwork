// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Util        = require('../utils/util').Util;

// Config
var Config      = require('../config.json');

// Models
var Models      = require('../models');
var User        = Models.User;

// -------------------------
//   Controller Definition
// -------------------------

var UsersController = function() {}

/**
 * Return User Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
UsersController.Get = function (req, res, body) {
    // Security
    if (!req.token.user || ((req.token.user.id != req.params.user_id) && (req.token.user.type != User.Type.ADMIN))) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.user_id, 4],  'Path parameter :user_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.user_id],     'Path parameter :user_id may not be null.')
        .GetErrors();

    // Fetch and return
    if (!errors.length) {
        User.find({ where: { id: req.params.user_id, account_id: req.token.user.account_id, status: [User.Status.ACTIVE, User.Status.PENDING] }})
            .success(function(user) {
                if (user) {
                    // Filter fields for output
                    delete user.dataValues.password;

                    return Client.Success(req, res, user);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('UsersController.Get :: Error looking up user.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Create a user
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
UsersController.Create = function (req, res, body) {
    // Security
    if (req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertFalse('isNull',  [req.params.fname],        'Body parameter :fname may not be null.')
        .AssertFalse('isNull',  [req.params.lname],        'Body parameter :lname may not be null.')
        .AssertTrue('isEmail',  [req.params.email],        'Body parameter :email must be a valid email.')
        .AssertFalse('isNull',  [req.params.email],        'Body parameter :email may not be null.')
        .AssertFalse('isNull',  [req.params.type],         'Body parameter :type may not be null.')
        .AssertTrue('isIn',  [req.params.type, [User.Type.ADMIN, User.Type.CONTRACTOR, User.Type.EMPLOYEE]], 'Body parameter :type is not valid.');

    // Allow super admin to override account_id
    if (req.token.user.is_super_admin && req.params.account_id) {
        errors.AssertTrue('isUUID', [req.params.account_id, 4], 'Body parameter :account_id may not be null');
    }

    // Finalize Validation Checks
    errors = errors.GetErrors;

    if (!errors.length) {

        if (req.token.user.is_super_admin && req.params.account_id) {
            var account_id = req.params.account_id;
        } else {
            var account_id = req.token.user.account_id;
        }

        User.findOrCreate({
            email: req.params.email
        },{
            id: UUID.v4().toString(),
            fname: req.params.fname,
            lname: req.params.lname,
            status: User.Status.ACTIVE,
            email: req.params.email,
            account_id: account_id,
            is_super_admin: false,
            type: req.params.type,
            password: Util.md5( req.params.password ) // TODO: Proper
        })
            .success(function(user, created) {
                if (!created) {
                    return Client.IllegalConflict(req, res, 'Duplicate Entity');
                }
                if (user) {
                    // TODO: Send Email to New User

                    // Filter fields for output
                    delete user.dataValues.password;

                    return Client.Success(req, res, user);
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('UsersController.Create :: Error creating user.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a user
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
UsersController.Update = function (req, res, body) {
    // Security
    if (!req.token.user || ((req.token.user.id != req.params.user_id) && (req.token.user.type != User.Type.ADMIN))) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    if (typeof req.params.fname != "undefined") {
        validate.AssertFalse('isNull',  [req.params.fname], 'Body parameter :fname may not be null.');
    }
    if (typeof req.params.lname != "undefined") {
        validate.AssertFalse('isNull',  [req.params.lname], 'Body parameter :lname may not be null.');
    }
    if (typeof req.params.email != "undefined") {
        validate.AssertFalse('isNull',  [req.params.email], 'Body parameter :email may not be null.');
        validate.AssertTrue('isEmail',  [req.params.email], 'Body parameter :email must be a valid email.');
    }
    if (typeof req.params.onboarding != "undefined") {
        req.params.onboarding = JSON.stringify(req.params.onboarding);
        validate.AssertTrue('isLength', [req.params.onboarding, 1, 512],  'Body parameter :onboarding must not exceed 512 characters.');
        validate.AssertFalse('isNull',  [req.params.onboarding],          'Body parameter :onboarding may not be null.');
    }
    if (typeof req.params.password != "undefined") {
        validate.AssertFalse('isNull',  [req.params.password], 'Body parameter :password may not be null.');
    }
    if (typeof req.params.type != "undefined") {
        validate.AssertFalse('isNull',  [req.params.type], 'Body parameter :type may not be null.');
        validate.AssertTrue('isIn',  [req.params.type, [User.Type.ADMIN, User.Type.EMPLOYEE, User.Type.CONTRACTOR]], 'Body parameter :type is not valid.');
    }
    if (typeof req.params.status != "undefined") {
        validate.AssertFalse('isNull',  [req.params.status], 'Body parameter :status may not be null.');
        validate.AssertTrue('isIn',  [req.params.status, [User.Status.ACTIVE, User.Status.PENDING]], 'Body parameter :status is not valid.');
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        User.find({ where: { id: req.params.user_id, account_id: req.token.user.account_id, status: [User.Status.ACTIVE, User.Status.PENDING] }})
            .success(function(user) {
                // Find entries
                if (user) {
                    if (typeof req.params.fname != "undefined") {
                        user.fname = req.params.fname;
                    }
                    if (typeof req.params.lname != "undefined") {
                        user.lname = req.params.lname;
                    }
                    if (typeof req.params.email != "undefined" && req.token.user.type == User.Type.ADMIN) {
                        user.email = req.params.email;
                    }
                    if (typeof req.params.password != "undefined") {
                        user.password = Util.md5(req.params.password);
                    }
                    if (typeof req.params.onboarding != "undefined") {
                        user.onboarding = req.params.onboarding;
                    }
                    if (typeof req.params.type != "undefined" && req.token.user.type == User.Type.ADMIN) {
                        user.type = req.params.type;
                    }
                    if (typeof req.params.status != "undefined" && req.token.user.type == User.Type.ADMIN) {
                        user.status = req.params.status;
                    }

                    user.save()
                        .success(function() {
                            // Filter fields for output
                            delete user.dataValues.password;

                            return Client.Success(req, res, user);
                        })
                        .error(function(error) {
                            console.log('UsersController.Update :: Error deleting user.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('UsersController.Update :: Error looking up user.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return All User Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
UsersController.List = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Fetch and return
    User.findAll({ where: { account_id: req.token.user.account_id, status: [User.Status.ACTIVE, User.Status.PENDING] } })
        .success(function(users) {
            if (users) {
                for (var i in users) {
                    // Filter fields for output
                    delete users[i].dataValues.password;
                }

                return Client.Success(req, res, users);
            } else {
                return Client.NotFound(req, res);
            }
        })
        .error(function(e, e2) {
            console.log('UsersController.Get :: Error looking up user.');
            console.log(e, e2);
            return Client.ServerError(req, res);
        })

}

/**
 * Mark a User as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
UsersController.Delete = function (req, res, body) {
     // Security
     if (req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
     }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.user_id, 4],  'Path parameter :user_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.user_id],     'Path parameter :user_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        User.find({ where: { id: req.params.user_id, account_id: req.token.user.account_id, status: [User.Status.ACTIVE, User.Status.PENDING] }})
            .success(function(user) {
                // Find entries
                if (user) {
                    user.status = User.Status.DELETED;
                    user.save()
                        .success(function() {
                            return Client.Success(req, res, { message: 'User deleted.' });
                        })
                        .error(function(error) {
                            console.log('UsersController.Delete :: Error deleting user.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e, e2) {
                console.log('UsersController.Delete :: Error looking up user.');
                console.log(e, e2);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.UsersController = UsersController;