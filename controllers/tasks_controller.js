// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Redis       = require("redis");

// Config
var Config      = require('../config.json');

// Redis Clients
var Publisher   = Redis.createClient(Config.redis_port, Config.redis_host);

// Models
var Models      = require('../models');
var User        = Models.User;
var Task        = Models.Task;

// -------------------------
//   Controller Definition
// -------------------------

var TasksController = function() {}

/**
 * Return Task Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TasksController.Get = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.task_id, 4],  'Path parameter :task_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.task_id],     'Path parameter :task_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        Task.find({ where: { id: req.params.task_id, status: [Task.Status.ACTIVE, Task.Status.IN_PROGRESS, Task.Status.COMPLETED] }})
            .success(function(task) {
                // Find entries
                if (task) {
                    return Client.Success(req, res, task);
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('TasksController.Get :: Error looking up task.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Return All Tasks
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TasksController.List = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    Task.FindAll_fromActiveCampaigns(req.token.user.account_id, function(e, tasks) {
        if (e) {
            console.log('TasksController.List :: Error looking up tasks.');
            console.log(e);
            return Client.ServerError(req, res);
        }

        if (tasks && tasks.length && tasks.length > 0) {
            return Client.Success(req, res, tasks);
        } else {
            return Client.NoContent(req, res);
        }
    });
}

/**
 * Return User's Assigned Tasks
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TasksController.Assigned = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    Task.findAll({ where: { assigned_to: req.token.user.id, status: [Task.Status.ACTIVE, Task.Status.IN_PROGRESS] }})
        .success(function(tasks) {
            if (tasks && tasks.length && tasks.length >= 0) {
                return Client.Success(req, res, tasks);
            } else {
                return Client.NoContent(req, res);
            }
        })
        .error(function(e, e2) {
            console.log('TasksController.Assigned :: Error looking up tasks.');
            console.log(e, e2);
            return Client.ServerError(req, res);
        })

}

/**
 * Return Latest Task Activity for the Account
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TasksController.Activity = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    try {
        Task.GetActivity(req.token.user.account_id, function(err, tasks) {
            if (err) {
                console.log('TasksController.Activity :: Error looking up tasks.');
                console.log(err);
                return Client.ServerError(req, res);
            }

            return Client.Success(req, res, tasks);
        })
    }
    catch(e) {
        console.log(e);
        return Client.ServerError(req, res);
    }
}

/**
 * Create a task
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TasksController.Create = function (req, res, body) {
    // Security
    if (!req.token.user || req.token.user.type != User.Type.ADMIN) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isLength', [req.params.title, 1, 128],         'Body parameter :title must be between 1-128 characters.')
        .AssertFalse('isNull',  [req.params.title],                 'Body parameter :title may not be null.')
        .AssertTrue('isLength', [req.params.description, 0, 5120],  'Body parameter :description must be between 1-128 characters.')
        .AssertTrue('isUUID',   [req.params.campaign_id, 4],        'Body parameter :campaign_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.campaign_id],           'Body parameter :campaign_id may not be null.')
        .AssertTrue('isUUID',   [req.params.company_id, 4],         'Body parameter :company_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.company_id],            'Body parameter :company_id may not be null.')
        .AssertTrue('isInt',    [req.params.due_on],                'Body parameter :due_on must be a valid Integer Timestamp.')
        .AssertFalse('isNull',  [req.params.due_on],                'Body parameter :due_on may not be null.');

    if (req.params.assigned_to) {
        errors.AssertTrue('isUUID',   [req.params.assigned_to, 4],        'Body parameter :assigned_to must be a valid UUID.')
        errors.AssertFalse('isNull',  [req.params.assigned_to],           'Body parameter :assigned_to may not be null.')
    } else {
        req.params.assigned_to = null;
    }
    errors = errors.GetErrors();

    if (!errors.length) {
        // TODO: Verify campaign_id & company_id exists
        Task.create({
            id: UUID.v4().toString(),
            title: req.params.title,
            description: req.params.description,
            campaign_id: req.params.campaign_id,
            company_id: req.params.company_id,
            assigned_to: req.params.assigned_to,
            account_id: req.token.user.account_id,
            due_on: req.params.due_on,
            status: Task.Status.ACTIVE
        })
            .success(function(task) {
                if (task) {
                    Publisher.publish('task_activity_' + req.token.user.account_id, JSON.stringify(task));
                    return Client.Success(req, res, task);
                } else {
                    return Client.InvalidRequest(req, res);
                }
            })
            .error(function(e) {
                console.log('TasksController.Create :: Error creating task.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
}

/**
 * Update a task
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TasksController.Update = function (req, res, body) {
    try {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var validate = new Validation();
    if (typeof req.params.title != "undefined") {
        validate.AssertTrue('isLength', [req.params.title, 1, 5120],    'Body parameter :description must be between 1-128 characters.');
        validate.AssertFalse('isNull',  [req.params.title],             'Body parameter :description may not be null.');
    }
    if (typeof req.params.description != "undefined") {
        validate.AssertTrue('isLength', [req.params.description, 0, 5120],  'Body parameter :description must be between 0-5120 characters.');
    }
    if (typeof req.params.campaign_id != "undefined") {
        validate.AssertTrue('isUUID',   [req.params.campaign_id, 4],    'Body parameter :campaign_id must be a valid UUID.');
        validate.AssertFalse('isNull',  [req.params.campaign_id],       'Body parameter :campaign_id may not be null.');
    }
    if (typeof req.params.company_id != "undefined") {
        validate.AssertTrue('isUUID',   [req.params.company_id, 4],     'Body parameter :company_id must be a valid UUID.')
        validate.AssertFalse('isNull',  [req.params.company_id],        'Body parameter :company_id may not be null.')
    }
    if (typeof req.params.assigned_to != "undefined" && req.params.assigned_to) {
        validate.AssertTrue('isUUID',   [req.params.assigned_to, 4],    'Body parameter :assigned_to must be a valid UUID.');
        validate.AssertFalse('isNull',  [req.params.assigned_to],       'Body parameter :assigned_to may not be null.');
    }
    if (typeof req.params.completion_msg != "undefined") {
        validate.AssertTrue('isLength', [req.params.completion_msg, 0, 5120], 'Body parameter :completion_msg must be between 1-5120 characters.');
    }
    if (typeof req.params.status != "undefined") {
        validate.AssertFalse('isNull',  [req.params.status],            'Body parameter :status may not be null.');
        validate.AssertTrue('isIn',     [req.params.status, [Task.Status.ACTIVE, Task.Status.COMPLETED]], 'Body parameter :status is not valid.');
    }
    if (typeof req.params.due_on != "undefined") {
        validate.AssertFalse('isNull',  [req.params.due_on],            'Body parameter :due_on may not be null.');
        validate.AssertTrue('isInt',    [req.params.due_on],            'Body parameter :due_on must be a valid integer timestamp.');
    }
    var errors = validate.GetErrors();

    if (!errors.length) {
        Task.find({ where: { id: req.params.task_id, status: [Task.Status.ACTIVE, Task.Status.IN_PROGRESS, Task.Status.COMPLETED] }})
            .success(function(task) {
                var trigger_event = false;

                // Find entries
                if (task) {
                    if (typeof req.params.title != "undefined" && req.token.user.type == User.Type.ADMIN) {
                        task.title = req.params.title;
                    }
                    if (typeof req.params.description != "undefined" && req.token.user.type == User.Type.ADMIN) {
                        task.description = req.params.description;
                    }
                    if (typeof req.params.assigned_to != "undefined" && req.token.user.type == User.Type.ADMIN) {
                        task.assigned_to = req.params.assigned_to;
                    }
                    if (typeof req.params.completion_msg != "undefined") {
                        task.completion_msg = req.params.completion_msg;
                    }
                    if (typeof req.params.due_on != "undefined") {
                        task.due_on = req.params.due_on;
                    }
                    if (typeof req.params.status != "undefined" /*&& req.token.user.type == User.Type.ADMIN*/) {
                        task.status = req.params.status;
                        if (task.status == Task.Status.COMPLETED) {
                            task.completed_by = req.token.user.id;
                            task.completed_on = parseInt(new Date().getTime() / 1000);
                        } else {
                            task.completed_on = '';
                        }
                    }

                    task.save()
                        .success(function() {
                            Client.Success(req, res, task);
                            Publisher.publish('task_activity_' + req.token.user.account_id, JSON.stringify(task));
                            return;
                        })
                        .error(function(error) {
                            console.log('TasksController.Update :: Error updating task.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('TasksController.Update :: Error looking up task.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        return Client.InvalidRequest(req, res, errors);
    }
    }
    catch(e) {
        console.log(e);
    }
}

/**
 * Mark a Task as deleted
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
TasksController.Delete = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    // Validate
    var errors = new Validation()
        .AssertTrue('isUUID',   [req.params.task_id, 4],  'Path parameter :task_id must be a valid UUID.')
        .AssertFalse('isNull',  [req.params.task_id],     'Path parameter :task_id may not be null.')
        .GetErrors();

    if (!errors.length) {
        Task.find({ where: { id: req.params.task_id, account_id: req.token.user.account_id, status: [Task.Status.ACTIVE, Task.Status.IN_PROGRESS, Task.Status.COMPLETED] }})
            .success(function(task) {
                // Find entries
                if (task) {
                    task.status = Task.Status.DELETED;
                    task.save()
                        .success(function() {
                            return Client.Success(req, res, { message: 'Task deleted.' });
                        })
                        .error(function(error) {
                            console.log('TasksController.Delete :: Error deleting task.');
                            console.log(error);
                            return Client.ServerError(req, res);
                        });
                } else {
                    return Client.NotFound(req, res);
                }
            })
            .error(function(e) {
                console.log('TasksController.Delete :: Error looking up task.');
                console.log(e);
                return Client.ServerError(req, res);
            })
    } else {
        // Validation of Parameters has Failed
        return Client.InvalidRequest(req, res, errors);
    }
}

exports.TasksController = TasksController;