// ----------------
//   Dependancies
// ----------------

// Utils
var Client      = require('../utils/client').Client;
var Time        = require('time');
var Async       = require('async');
var UUID        = require('node-uuid');
var Validation  = require('../utils/validator').Validation;
var Tokens      = require('../utils/tokens').Tokens;
var Util        = require('../utils/util').Util;

// Config
var Config      = require('../config.json');

// Stripe
var Stripe      = require('stripe')(Config.stripe.public_key);

// Models
var Models      = require('../models');
var Account     = Models.Account;
var User        = Models.User;

// -------------------------
//   Controller Definition
// -------------------------

var BillingController = function() {}

/**
 * Create Billing Agreement
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
BillingController.Create = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    try {
    // Validate
    var errors = new Validation()
        .AssertTrue('isLength',     [req.params.cardholder_name, 1, 128], 'Body parameter :cardholder_name must be between 1-128 characters.')
        .AssertFalse('isNull',      [req.params.cardholder_name],         'Body parameter :cardholder_name may not be null.')
        .AssertTrue('isCreditCard', [req.params.card_number],             'Body parameter :card_number must be a valid credit card number.')
        .AssertFalse('isNull',      [req.params.card_number],             'Body parameter :card_number may not be null.')
        .AssertTrue('inRange',      [req.params.card_exp_month, [1, 12]], 'Body parameter :card_exp_month must be a valid month [1-12].')
        .AssertFalse('isNull',      [req.params.card_exp_month],          'Body parameter :card_exp_month may not be null.')
        .AssertTrue('inRange',      [req.params.card_exp_year, [2014, 2025]], 'Body parameter :card_exp_year must be a valid month [1-12].')
        .AssertFalse('isNull',      [req.params.card_exp_year],               'Body parameter :card_exp_year may not be null.')
        .AssertTrue('isInt',        [req.params.card_cvv],                'Body parameter :card_cvv must be a valid month [1-12].')
        .AssertFalse('isNull',      [req.params.card_cvv],                'Body parameter :card_cvv may not be null.')
        .GetErrors();
    }
    catch(e) {
        console.log(e);
        return Client.ServerError(req, res, JSON.stringify(e));
    }

    User.find(req.token.user.id)
        .success(function(user) {
            if (user) {
                Account.find({ where: { id: user.account_id, status: [Account.Status.ACTIVE] }})
                    .success(function(account) {

                        // User & Account Fetched, Let's create a Stripe customer profile
                        Stripe.customers.create({
                            description: 'Customer: ' + account.id + ' (' + account.billing_email + ')',
                            card: {
                                number: req.params.card_number,
                                exp_month: req.params.card_exp_month,
                                exp_year: req.params.card_exp_year,
                                cvc: req.params.card_cvv,
                                name: req.params.cardholder_name
                            },
                            email: account.billing_email,
                            plan: Config.stripe.subscription_id
                        }, function(err, customer) {
                            if (err) {
                                console.log('BillingController.Create :: Error creating billing subscription.');
                                console.log(err);

                                switch (err.type) {
                                    case 'StripeCardError':
                                    // A declined card error
                                    case 'StripeInvalidRequestError':
                                    // Invalid parameters were supplied to Stripe's API
                                    case 'StripeAPIError':
                                    // An error occurred internally with Stripe's API
                                    case 'StripeConnectionError':
                                    // Some kind of error occurred during the HTTPS communication
                                    case 'StripeAuthenticationError':
                                    // You probably used an incorrect API key
                                }

                                return Client.InvalidRequest(req, res, err);
                            }

                            console.log('-----CUSTOMER-----');
                            console.log(customer.subscriptions.data[0]);

                            account.stripe_subscription_id = customer.subscriptions.data[0].id;
                            account.stripe_customer_id = customer.id;
                            account.stripe_card_id = customer.default_card;
                            account.save()
                                .success(function() {
                                    // Success
                                    Client.Success(req, res, account);
                                })
                                .error(function(err) {
                                    console.log('BillingController.Create :: Error saving billing information to account.');
                                    console.log(err);
                                    return Client.ServerError(req, res);
                                })
                        });
                    })
                    .error(function(err) {
                        console.log('BillingController.Create :: Error looking up account information.');
                        console.log(err);
                        return Client.ServerError(req, res);
                    })

                return;
            } else {
                return Client.NotFound(req, res);
            }
        })
        .error(function(e) {
            console.log('BillingController.Create :: Error looking up user.');
            console.log(e);
            return Client.ServerError(req, res);
        })
}

/**
 * Update Billing Details
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
BillingController.Update = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    User.find(req.token.user.id)
        .success(function(user) {
            if (user) {
                Account.find({ where: { id: user.account_id, status: [Account.Status.ACTIVE] }})
                    .success(function(account) {
                        if (account) {
                            Stripe.customers.deleteCard(
                                account.stripe_customer_id,
                                account.stripe_card_id,
                                function(err, confirmation) {
                                    if (err) {
                                        console.log('BillingController.Update :: Error removing old card.');
                                        console.log(err);
                                        return Client.ServerError(req, res);
                                    } else {
                                        // User Fetched, old card removed, Let's add the new card
                                        Stripe.customers.createCard(
                                            account.stripe_customer_id,
                                            { card: {
                                                number: req.params.card_number,
                                                exp_month: req.params.card_exp_month,
                                                exp_year: req.params.card_exp_year,
                                                cvc: req.params.card_cvv,
                                                name: req.params.cardholder_name
                                            }},
                                            function(err, card) {
                                                if (err) {
                                                    console.log('BillingController.Update :: Error updating card.');
                                                    console.log(err);
                                                    return Client.ServerError(req, res);
                                                } else {

                                                    account.stripe_card_id = card.id;
                                                    account.save()
                                                        .success(function() {
                                                            Stripe.customers.update(account.stripe_customer_id, {
                                                                default_card: card.id
                                                            }, function(err, customer) {
                                                                if (err) {
                                                                    console.log('BillingController.Update :: Error updating card.');
                                                                    console.log(err);
                                                                    return Client.ServerError(req, res);
                                                                } else {
                                                                    // Success
                                                                    Client.Success(req, res, account);
                                                                }
                                                            });
                                                        })
                                                        .error(function(e) {
                                                            console.log('BillingController.Update :: Error saving new card back to account.');
                                                            console.log(e);
                                                            return Client.ServerError(req, res);
                                                        })
                                                }
                                            }
                                        );
                                    }
                                }
                            );

                        } else {
                            console.log('BillingController.Update :: Error finding account..');
                            return Client.ServerError(req, res);
                        }
                    })
                    .error(function(e) {
                        console.log('BillingController.Update :: Error looking up account.');
                        console.log(e);
                        return Client.ServerError(req, res);
                    });
            } else {
                return Client.NotFound(req, res);
            }
        })
        .error(function(e) {
            console.log('BillingController.Update :: Error looking up user.');
            console.log(e);
            return Client.ServerError(req, res);
        })
}

/**
 * Cancel Billing Agreement
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
BillingController.Delete = function (req, res, body) {
    // Security
    if (!req.token.user) {
        return Client.NotAuthorized(req, res);
    }

    User.find(req.token.user.id)
        .success(function(user) {
            if (user) {
                Account.find({ where: { id: user.account_id, status: [Account.Status.ACTIVE] }})
                    .success(function(account) {
                        if (account) {
                            // User Fetched, Let's cancel the users subscription
                            Stripe.customers.cancelSubscription(
                                account.stripe_customer_id,
                                account.stripe_subscription_id, {
                                    at_period_end: true
                                },
                                function(err, confirmation) {
                                    if (err) {
                                        console.log('BillingController.Delete :: Error cancelling subscription.');
                                        console.log(err);
                                        return Client.ServerError(req, res);
                                    }

                                    // Success
                                    Client.Success(req, res, { message: true });
                                }
                            );
                        } else {
                            return Client.NotFound(req, res);
                        }
                    })
                    .error(function(e) {
                        console.log('BillingController.Delete :: Error looking up user.');
                        console.log(e);
                        return Client.ServerError(req, res);
                    })

            } else {
                return Client.NotFound(req, res);
            }
        })
        .error(function(e) {
            console.log('BillingController.Delete :: Error looking up user.');
            console.log(e);
            return Client.ServerError(req, res);
        })
}

/**
 * Receive Billing Event
 *
 * @param   object      req     Request object
 * @param   object      res     Response object
 * @param   object      body    Response body object.
 */
BillingController.Notify = function (req, res, body) {

    console.log('BILLING EVENT:', req.method);
    console.log(req.params);
    console.log(body);

    switch (req.params.type) {
        case 'invoice.created':
            if (req.params.data && req.params.data.object && req.params.data.object.customer) {
                // Stripe just notified us of a new Invoice, find
                // the associated account.
                Account.find({ where: { stripe_customer_id: req.params.data.object.customer } })
                    .success(function(account) {
                        console.log(account.dataValues);

                        // Count the number of Users associated with the account
                        User.count({ where: { account_id: account.id, status: [1,2,3] } })
                            .success(function(users) {
                                console.log('Users:', users);

                                if (users && users > 2) {
                                    console.log({
                                        customer: req.params.data.object.customer,
                                        amount: Config.billing.per_user_extra * (users - 2),
                                        currency: "usd",
                                        description: "Additional user accounts (" + (users - 2) + " x $" + parseFloat(Config.billing.per_user_extra / 100).toFixed(2) + ")",
                                        invoice: req.params.data.object.id
                                    });

                                    // Adjust Invoice for every extra user.
                                    Stripe.invoiceItems.create({
                                        customer: req.params.data.object.customer,
                                        amount: Config.billing.per_user_extra * (users - 2),
                                        currency: "usd",
                                        description: "Additional user accounts (" + (users - 2) + " x $" + parseFloat(Config.billing.per_user_extra / 100).toFixed(2) + ")",
                                        invoice: req.params.data.object.id
                                    }, function(errors, invoiceItem) {
                                        // asynchronously called
                                        if (errors) {
                                            console.log('BillingController.Notify :: Error applying usage fees.');
                                            console.log(errors);
                                            return Client.ServerError(req, res);
                                        } else {
                                            Client.Success(req, res, { message: "Complete." });
                                        }
                                    });
                                }
                            })
                            .error(function(e) {
                                console.log('BillingController.Notify :: Error looking up customer for billing event.');
                                console.log(err);
                                return Client.ServerError(req, res);
                            })
                    })
                    .error(function(err) {
                        console.log('BillingController.Notify :: Error looking up customer for billing event.');
                        console.log(err);
                        return Client.ServerError(req, res);
                    })
            } else {
                console.log('BillingController.Notify :: Error looking up customer for billing event.');
                console.log(err);
                return Client.InvalidRequest(req, res);
            }
            break;

        default:
            return Client.InvalidRequest(req, res);

            console.log('BillingController.Notify :: Not interested in this event.');
            console.log(req.params.type);

            break;
    }
}

exports.BillingController = BillingController;